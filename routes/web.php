<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {


    $api->group(['namespace'=> 'App\Http\Controllers'], function ($api) {
        // banner
        $api->get('/banner', ['uses' => 'BannerController@listing']);
        $api->get('/banner/{id}', ['uses' => 'BannerController@listById']);
        $api->post('/banner', ['middleware' => 'auth', 'uses' => 'BannerController@create']);
        $api->put('/banner/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@update']);
        $api->delete('/banner/{id}', ['middleware' => 'auth', 'uses' => 'BannerController@delete']);

        // gallery
        $api->get('/gallery', ['uses' => 'GalleryController@listing']);
        $api->get('/gallery/{id}', ['uses' => 'GalleryController@listById']);
        $api->get('/gallery/news/{news_id}', ['uses' => 'GalleryController@listByNewsId']);
        $api->post('/gallery/{news_id}', ['middleware' => 'auth', 'uses' => 'GalleryController@create']);
        $api->put('/gallery/{id}', ['middleware' => 'auth', 'uses' => 'GalleryController@update']);
        $api->delete('/gallery/{id}/news/{news_id}', ['middleware' => 'auth', 'uses' => 'GalleryController@delete']);

        // news
        $api->get('/product', ['uses' => 'ProductController@listing']);
        $api->get('/product/{id}', ['uses' => 'ProductController@listById']);
        $api->post('/product', ['middleware' => 'auth', 'uses' => 'ProductController@create']);
        $api->put('/product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@update']);
        $api->delete('/product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@delete']);

        // academy
        $api->get('/academy', ['uses' => 'AcademyController@listing']);
        $api->get('/academy/{id}', ['uses' => 'AcademyController@listById']);
        $api->post('/academy', ['uses' => 'AcademyController@create']);
        $api->put('/academy/{id}', ['middleware' => 'auth', 'uses' => 'AcademyController@update']);
        $api->delete('/academy/{id}', ['middleware' => 'auth', 'uses' => 'AcademyController@delete']);

        // news
        $api->get('/news', ['uses' => 'NewsController@listing']);
        $api->get('/news/{id}', ['uses' => 'NewsController@listById']);
        $api->post('/news', ['middleware' => 'auth', 'uses' => 'NewsController@create']);
        $api->put('/news/{id}', ['middleware' => 'auth', 'uses' => 'NewsController@update']);
        $api->delete('/news/{id}', ['middleware' => 'auth', 'uses' => 'NewsController@delete']);

        // news
        $api->get('/certificate', ['uses' => 'CertificateController@listing']);
        $api->get('/certificate/{id}', ['uses' => 'CertificateController@listById']);
        $api->get('/certificate/partnership/{partnership_id}', ['uses' => 'CertificateController@listByNewsId']);
        $api->post('/certificate/{partnership_id}', ['middleware' => 'auth', 'uses' => 'CertificateController@create']);
        $api->put('/certificate/{id}', ['middleware' => 'auth', 'uses' => 'CertificateController@update']);
        $api->delete('/certificate/{id}/partnership/{partnership_id}', ['middleware' => 'auth', 'uses' => 'CertificateController@delete']);

        // news
        $api->get('/education', ['uses' => 'KnowledgeController@listing']);
        $api->get('/education/{id}', ['uses' => 'KnowledgeController@listById']);
        $api->post('/education', ['middleware' => 'auth', 'uses' => 'KnowledgeController@create']);
        $api->put('/education/{id}', ['middleware' => 'auth', 'uses' => 'KnowledgeController@update']);
        $api->delete('/education/{id}', ['middleware' => 'auth', 'uses' => 'KnowledgeController@delete']);

        // partnership
        $api->get('/partnership', ['uses' => 'PartnershipController@listing']);
        $api->get('/partnership/{id}', ['uses' => 'PartnershipController@listById']);
        $api->post('/partnership', ['middleware' => 'auth', 'uses' => 'PartnershipController@create']);
        $api->put('/partnership/{id}', ['middleware' => 'auth', 'uses' => 'PartnershipController@update']);
        $api->delete('/partnership/{id}', ['middleware' => 'auth', 'uses' => 'PartnershipController@delete']);

        // system setting
        $api->get('/systemSetting', ['uses' => 'SystemSettingController@listing']);
        $api->put('/systemSetting', ['middleware' => 'auth', 'uses' => 'SystemSettingController@update']);

        // fileupload
        $api->post('/asset', ['middleware' => 'auth', 'uses' => 'AssetController@create']);
    });

    $api->group(['prefix' => 'user', 'namespace'=> 'App\Http\Controllers'], function ($api)
    {
        // account management
        $api->get('/me', ['middleware' => 'auth', 'uses' => 'UserController@show']);
        $api->post('/create', ['uses' => 'UserController@create']);
        $api->post('/update', ['middleware' => 'auth' , 'uses' => 'UserController@update']);
        $api->post('/login', ['uses' => 'UserController@login']);
        $api->post('/upload-profile-image', ['uses' => 'UserController@uploadProfileImage']);

        // password management
        $api->post('/password/request-reset', ['uses' => 'UserController@requestPasswordReset']);
        $api->post('/password/reset', ['uses' => 'UserController@passwordReset']);

    });
});

