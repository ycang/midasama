<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SystemSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        DB::table('system_settings')->insert([ //,
            'code' => 'about_us_video_url',
            'value' => 1,
        ]);
    }
}
