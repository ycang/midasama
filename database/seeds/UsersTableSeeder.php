<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        $limit = 1;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([ //,
                'firstname' => 'admin',
                'lastname' => 'admin',
                'password' => Hash::make('admin.1234!'),
                'api_token' => $faker->md5,
                'email' => 'admin@midasama.com'
            ]);
            DB::table('role_users')->insert([ //,
                'role_id' => 1,
                'user_id' => 1,
            ]);
        }
    }
}
