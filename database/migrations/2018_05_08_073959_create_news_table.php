<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en')->nullable();
            $table->string('content_en')->nullable();
            $table->text('title_cn')->nullable();
            $table->text('content_cn')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('video_link')->nullable();
            $table->string('news_type')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('news_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
