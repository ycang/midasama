<?php
namespace App\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract {

    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'title_en' => $product->title_en,
            'product_name_en' => $product->product_name_en,
            'content_en' => $product->content_en,
            'title_cn' => $product->title_cn,
            'product_name_cn' => $product->product_name_cn,
            'content_cn' => $product->content_cn,
            'cover_image' =>  $product->coverImage ? [ 'id' => $product->coverImage->id , 'name' => $product->coverImage->name , 'filename' => env('API_DOMAIN').'/uploaded-assets/'.$product->coverImage->filename ] : [],
            'video_link' => $product->video_link,
            'position' => $product->position,
            'status' => $product->status,
            'created_at'    => $product->created_at
        ];
    }

}
