<?php
namespace App\Transformers;

use App\Models\Partnership;
use League\Fractal\TransformerAbstract;

class PartnershipTransformer extends TransformerAbstract {

    protected $defaultIncludes = [
        'certificates'
    ];

    public function transform(Partnership $partnership)
    {
        return [
            'id' => $partnership->id,
            'title_en' => $partnership->title_en,
            'content_en' => $partnership->content_en,
            'title_cn' => $partnership->title_cn,
            'content_cn' => $partnership->content_cn,
            'cover_image' =>  $partnership->coverImage ? [ 'id' => $partnership->coverImage->id , 'name' => $partnership->coverImage->name , 'filename' => env('API_DOMAIN').'/uploaded-assets/'.$partnership->coverImage->filename ] : [],
            'video_link' => $partnership->video_link,
            'website' => $partnership->website,
            'status' => $partnership->status,
            'created_at'    => $partnership->created_at
        ];
    }

    public function includeCertificates(Partnership $partnership)
    {
        $certificates = $partnership->certificates;
        return ($certificates)? $this->collection($certificates, new CertificateTransformer) : $this->null();
    }

}
