<?php
namespace App\Transformers;

use App\Models\News;
use League\Fractal\TransformerAbstract;

class NewsTransformer extends TransformerAbstract {

    public function transform(News $news)
    {
        return [
            'id' => $news->id,
            'news_date' => $news->news_date,
            'news_type' => $news->news_type,
            'title_en' => $news->title_en,
            'content_en' => $news->content_en,
            'title_cn' => $news->title_cn,
            'content_cn' => $news->content_cn,
            'cover_image' =>  $news->coverImage ? [ 'id' => $news->coverImage->id , 'name' => $news->coverImage->name , 'filename' => env('API_DOMAIN').'/uploaded-assets/'.$news->coverImage->filename ] : [],
            'video_link' => $news->video_link,
            'position' => $news->position,
            'status' => $news->status,
            'created_at'    => $news->created_at
        ];
    }

}
