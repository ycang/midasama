<?php 
namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

    public function transform(User $user)
    {
        return [
            'id'            => $user->id,
            'role_id'       => $user->role->role_id,
            'firstname'     => $user->firstname,
            'lastname'      => $user->lastname,
            'profile_image' => ($user->profile_image) ? env('API_DOMAIN').'/profile-image/'.$user->profile_image : '',
            'email'         => $user->email,
            'token'         => $user->api_token
        ];
    }

}