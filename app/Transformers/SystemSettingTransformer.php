<?php
namespace App\Transformers;

use App\Models\SystemSetting;
use League\Fractal\TransformerAbstract;

class SystemSettingTransformer extends TransformerAbstract {

    public function transform(SystemSetting $systemSetting)
    {
        return [
            'id' => $systemSetting->id,
            'code' => $systemSetting->code,
            'value' => $systemSetting->value
        ];
    }

}
