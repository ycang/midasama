<?php
namespace App\Transformers;

use App\Models\Banner;
use League\Fractal\TransformerAbstract;

class BannerTransformer extends TransformerAbstract {

    public function transform(Banner $banner)
    {
        return [
            'id' => $banner->id,
            'title_en' => $banner->title_en,
            'subtitle_en' => $banner->subtitle_en,
            'title_cn' => $banner->title_cn,
            'subtitle_cn' => $banner->subtitle_cn,
            'banner_image_mobile' => $banner->bannerImageMobile ? [ 'id' => $banner->bannerImageMobile->id , 'name' => $banner->bannerImageMobile->name , 'filename' => env('API_DOMAIN').'/uploaded-assets/'.$banner->bannerImageMobile->filename ] : [],
            'banner_image_desktop' => $banner->bannerImageDesktop ? [ 'id' => $banner->bannerImageDesktop->id , 'name' => $banner->bannerImageDesktop->name, 'filename' => env('API_DOMAIN').'/uploaded-assets/'.$banner->bannerImageDesktop->filename ] : [],
            'position' => $banner->position,
            'status' => $banner->status,
            'created_at'    => $banner->created_at
        ];
    }

}
