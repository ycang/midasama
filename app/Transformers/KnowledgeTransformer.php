<?php
namespace App\Transformers;

use App\Models\Knowledge;
use League\Fractal\TransformerAbstract;

class KnowledgeTransformer extends TransformerAbstract {

    public function transform(Knowledge $knowledge)
    {
        return [
            'id' => $knowledge->id,
            'title_en' => $knowledge->title_en,
            'title_cn' => $knowledge->title_cn,
            'video_link' => $knowledge->video_link,
            'status' => $knowledge->status,
            'created_at'    => $knowledge->created_at
        ];
    }

}
