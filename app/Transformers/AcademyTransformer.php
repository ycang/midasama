<?php
namespace App\Transformers;

use App\Models\Academy;
use League\Fractal\TransformerAbstract;

class AcademyTransformer extends TransformerAbstract {

    public function transform(Academy $academy)
    {
        return [
            'id' => $academy->id,
            'first_name' => $academy->first_name,
            'last_name' => $academy->last_name,
            'contact_no' => $academy->contact_no,
            'email' => $academy->email,
            'username' => $academy->username,
            'nationality' => $academy->nationality,
            'remarks' => $academy->remarks,
            'reference' => $academy->reference,
            'status' => $academy->status,
            'audits' => $academy->audits,
            'paid_date' => $academy->paid_date,
            'created_at'    => $academy->created_at
        ];
    }

}
