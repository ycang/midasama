<?php
namespace App\Transformers;

use App\Models\Gallery;
use League\Fractal\TransformerAbstract;

class GalleryTransformer extends TransformerAbstract {

    public function transform(Gallery $gallery)
    {
        return [
            'id' => $gallery->id,
            'news_id' => $gallery->news_id,
            'title_en' => $gallery->title_en,
            'content_en' => $gallery->content_en,
            'title_cn' => $gallery->title_cn,
            'content_cn' => $gallery->content_cn,
            'gallery_image' => $gallery->image ? [ 'id' => $gallery->image->id , 'name' => $gallery->image->name , 'filename' => env('API_DOMAIN').'/uploaded-assets/'.$gallery->image->filename ] : [],
            'position' => $gallery->position,
            'status' => $gallery->status,
            'created_at'    => $gallery->created_at
        ];
    }

}
