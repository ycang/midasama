<?php
namespace App\Transformers;

use App\Models\Asset;
use League\Fractal\TransformerAbstract;

class AssetTransformer extends TransformerAbstract {

    public function transform(Asset $asset)
    {
        return [
            'id' => $asset->id,
            'name' => $asset->name,
            'type' => $asset->type,
            'created_at'    => $asset->created_at
        ];
    }

}
