<?php
namespace App\Transformers;

use App\Models\Certificate;
use League\Fractal\TransformerAbstract;

class CertificateTransformer extends TransformerAbstract {

    public function transform(Certificate $certificate)
    {
        return [
            'id' => $certificate->id,
            'partnership_id' => $certificate->partnership_id,
            'title_en' => $certificate->title_en,
            'content_en' => $certificate->content_en,
            'title_cn' => $certificate->title_cn,
            'content_cn' => $certificate->content_cn,
            'cover_image' => $certificate->coverImage ? [ 'id' => $certificate->coverImage->id , 'name' => $certificate->coverImage->name , 'filename' => env('API_DOMAIN').'/uploaded-assets/'.$certificate->coverImage->filename ] : [],
            'status' => $certificate->status,
            'created_at'    => $certificate->created_at
        ];
    }

}
