<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;

    protected $table = 'galleries';

    const STATUS_INACTIVE= 'inactive';
    const STATUS_ACTIVE = 'active';

    protected $fillable = [
        'gallery_image',
        'news_id',
        'title_en',
        'content_en',
        'title_cn',
        'content_cn',
        'position',
        'status',
    ];


    public function image()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'gallery_image');
    }
}
