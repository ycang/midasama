<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Academy extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'academy';

    const STATUS_OTHERS = 'others';
    const STATUS_PENDING_FOR_PAYMENT = 'pending-for-payment';
    const STATUS_PAYMENT_DONE = 'payment-done';
    const STATUS_COMPLETED = 'completed';

    protected $fillable = [
        'first_name',
        'last_name',
        'contact_no',
        'email',
        'username',
        'nationality',
        'paid_date',
        'remarks',
        'reference',
        'status',
    ];
}
