<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Certificate extends Model
{
    use SoftDeletes;

    protected $table = 'certificates';

    const STATUS_INACTIVE= 'inactive';
    const STATUS_ACTIVE = 'active';

    protected $fillable = [
        'partnership_id',
        'title_en',
        'content_en',
        'title_cn',
        'content_cn',
        'cover_image',
        'status',
    ];


    public function coverImage()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'cover_image');
    }
}
