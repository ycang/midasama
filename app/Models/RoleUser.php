<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
