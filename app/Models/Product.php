<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    const STATUS_INACTIVE= 'inactive';
    const STATUS_ACTIVE = 'active';

    protected $fillable = [
        'product_name_en',
        'title_en',
        'content_en',
        'product_name_cn',
        'title_cn',
        'content_cn',
        'cover_image',
        'status',
    ];

    public function coverImage()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'cover_image');
    }
}
