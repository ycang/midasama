<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemSetting extends Model
{
    protected $table = 'system_settings';

    protected $fillable = [
        'code',
        'value',
    ];
}
