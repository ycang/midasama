<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partnership extends Model
{
    use SoftDeletes;

    protected $table = 'partnerships';

    const STATUS_INACTIVE= 'inactive';
    const STATUS_ACTIVE = 'active';

    protected $fillable = [
        'title_en',
        'content_en',
        'title_cn',
        'content_cn',
        'cover_image',
        'status',
        'website'
    ];

    public function coverImage()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'cover_image');
    }

    public function certificates()
    {
        return $this->hasMany('App\Models\Certificate', 'partnership_id', 'id');
    }
}
