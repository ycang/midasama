<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use SoftDeletes;

    protected $table = 'banners';

    const STATUS_INACTIVE= 'inactive';
    const STATUS_ACTIVE = 'active';

    protected $fillable = [
        'title_en',
        'subtitle_en',
        'title_cn',
        'subtitle_cn',
        'banner_image_mobile',
        'banner_image_desktop',
        'position',
        'status',
    ];

    public function bannerImageMobile()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'banner_image_mobile');
    }

    public function bannerImageDesktop()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'banner_image_desktop');
    }
}
