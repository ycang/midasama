<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';

    const STATUS_INACTIVE= 'inactive';
    const STATUS_ACTIVE = 'active';
    const NEWS_TYPE_TEXT_ONLY = 'text-only';
    const NEWS_TYPE_GALLERY = 'gallery';
    const NEWS_TYPE_VIDEO = 'video';

    protected $fillable = [
        'news_type',
        'news_date',
        'title_en',
        'content_en',
        'title_cn',
        'content_cn',
        'cover_image',
        'video_link',
        'status',
    ];

    public function coverImage()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'cover_image');
    }
}
