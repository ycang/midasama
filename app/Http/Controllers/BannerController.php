<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Transformers\BannerTransformer;
use Illuminate\Http\Request;

/**
 * Banner resource representation.
 *
 * @Resource("Banner", uri="/banner")
 */
class BannerController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show banner listing
     *
     * Get a JSON representation listing of banner listing
     */
    public function listing(Request $request)
    {
        $isPublished = $request->get('isPublished');
        if($isPublished){
            $bannerModel = Banner::with('bannerImageMobile','bannerImageDesktop')->where('status', 'active')->orderBy('position','desc')->get();
        }else{
            $bannerModel = Banner::with('bannerImageMobile','bannerImageDesktop')->get();
        }
        return $this->response->collection($bannerModel, new BannerTransformer);
    }

    /**
     * Show banner listing
     *
     * Get a JSON representation listing of banner listing
     */
    public function listById($id)
    {
        $bannerModel = Banner::with('bannerImageMobile','bannerImageDesktop')->where('id', $id)->first();

        return $this->response->item($bannerModel, new BannerTransformer);
    }

    /**
     * Create banner
     *
     * Create a new banner
     *
     * @Post("/create")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Banner::STATUS_ACTIVE : Banner::STATUS_INACTIVE;

        if (Banner::create($data)) {
            $bannerModel = Banner::with('bannerImageMobile','bannerImageDesktop')->get();
            return $this->response->collection($bannerModel, new BannerTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update banner
     *
     * Update a new banner
     *
     * @Post("/update")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Banner::STATUS_ACTIVE : Banner::STATUS_INACTIVE;

        if (Banner::where('id', $id)->update($data)) {
            $bannerModel = Banner::with('bannerImageMobile','bannerImageDesktop')->get();
            return $this->response->collection($bannerModel, new BannerTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete news
     *
     * Delete a new news
     *
     * @Delete("/{id}")
     */
    public function delete($id)
    {
        $user = \Auth::user();

        if (Banner::where('id', $id)->delete()) {
            $bannerModel = Banner::with('bannerImageMobile','bannerImageDesktop')->get();
            return $this->response->collection($bannerModel, new BannerTransformer);
        }

        return $this->response->errorInternal();
    }

}
