<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Transformers\AssetTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

/**
 * Asset resource representation.
 *
 * @Resource("Asset", uri="/asset")
 */
class AssetController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Create asset
     *
     * Create a new asset
     *
     * @Post("/create")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request)
    {
        $file = $request->file('file');

        if(!File::exists(base_path() . '/public/uploaded-assets/')) {
            File::makeDirectory(base_path() . '/public/uploaded-assets/', $mode = 0755, true, true);
        }

        if ($file) {
            $filename = $file->hashName();
            $data['name'] = $file->getClientOriginalName();
            $data['filename'] = $filename;
            $data['type'] = $file->getClientOriginalExtension();
            try{
                move_uploaded_file($file->getPathname(), base_path() . '/public/uploaded-assets/' . $filename);
            }catch (Exception $e){
                return $this->response->error($e->getMessage(), 400);
            }
        }

        if ($assetModel = Asset::create($data)) {
            return $this->response->item($assetModel, new AssetTransformer);
        }

        return $this->response->errorInternal();
    }
}
