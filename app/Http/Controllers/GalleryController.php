<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Transformers\GalleryTransformer;
use Illuminate\Http\Request;

/**
 * Gallery resource representation.
 *
 * @Resource("Gallery", uri="/gallery")
 */
class GalleryController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show gallery listing
     *
     * @Get("/")
     * Get a JSON representation listing of gallery listing
     */
    public function listing(Request $request)
    {
        $isPublished = $request->get('isPublished');
        if($isPublished){
            $galleryModel = Gallery::with('image')->where('status', 'active')->orderBy('position','desc')->get();
        }else{
            $galleryModel = Gallery::with('image')->get();
        }
        return $this->response->collection($galleryModel, new GalleryTransformer);
    }

    /**
     * Show gallery by id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of gallery listing
     */
    public function listById($id)
    {
        $galleryModel = Gallery::with('image')->where('id', $id)->first();

        return $this->response->item($galleryModel, new GalleryTransformer);
    }

    /**
     * Show gallery by news id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of gallery listing
     */
    public function listByNewsId($news_id)
    {
        $galleryModel = Gallery::with('image')->where('news_id', $news_id)->orderBy('id','desc')->get();

        return $this->response->collection($galleryModel, new GalleryTransformer);
    }

    /**
     * Create gallery
     *
     * Create a new gallery
     *
     * @Post("/")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request, $news_id)
    {
        $user = \Auth::user();

        // check this user has credit
        $params = $request->all();

        if($params['gallery_images']){
           foreach ($params['gallery_images'] as $k => $v){
               $data['position'] = 0;
               $data['news_id'] = $news_id;
               $data['gallery_image'] = $v;
               $data['status'] = Gallery::STATUS_INACTIVE;
               Gallery::create($data);
           }
        }

        $galleryModel = Gallery::with('image')->where('news_id', $news_id)->orderBy('id','desc')->get();

        if($galleryModel){
            return $this->response->collection($galleryModel, new GalleryTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update gallery
     *
     * Update a new gallery
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['gallery_image'] = ($params['gallery_image']) ? $params['gallery_image'] : '' ;
        $data['status'] = ($params['status']) ? Gallery::STATUS_ACTIVE : Gallery::STATUS_INACTIVE;

        if (Gallery::where('id', $id)->update($data)) {
            $galleryModel = Gallery::with('image')->get();
            return $this->response->collection($galleryModel, new GalleryTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete gallery
     *
     * Delete a new gallery
     *
     * @Delete("/{id}")
     */
    public function delete($id,$news_id)
    {
        $user = \Auth::user();

        if (Gallery::where('id', $id)->delete()) {
            $galleryModel = Gallery::with('image')->where('news_id', $news_id)->orderBy('id','desc')->get();
            return $this->response->collection($galleryModel, new GalleryTransformer);
        }

        return $this->response->errorInternal();
    }

}
