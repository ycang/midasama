<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Transformers\ProductTransformer;
use Illuminate\Http\Request;

/**
 * Product resource representation.
 *
 * @Resource("Product", uri="/product")
 */
class ProductController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show product listing
     *
     * @Get("/")
     * Get a JSON representation listing of product listing
     */
    public function listing(Request $request)
    {
        $isPublished = $request->get('isPublished');
        if($isPublished){
            $productModel = Product::with('coverImage')->where('status', 'active')->get();
        }else{
            $productModel = Product::with('coverImage')->get();
        }
        return $this->response->collection($productModel, new ProductTransformer);
    }

    /**
     * Show product by id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of product listing
     */
    public function listById($id)
    {
        $productModel = Product::with('coverImage')->where('id', $id)->first();

        return $this->response->item($productModel, new ProductTransformer);
    }

    /**
     * Create product
     *
     * Create a new product
     *
     * @Post("/")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'product_name_en' => 'required',
            'product_name_cn' => 'required',
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Product::STATUS_ACTIVE : Product::STATUS_INACTIVE;

        if (Product::create($data)) {
            $productModel = Product::with('coverImage')->get();
            return $this->response->collection($productModel, new ProductTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update product
     *
     * Update a new product
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'product_name_en' => 'required',
            'product_name_cn' => 'required',
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Product::STATUS_ACTIVE : Product::STATUS_INACTIVE;

        if (Product::where('id', $id)->update($data)) {
            $productModel = Product::with('coverImage')->get();
            return $this->response->collection($productModel, new ProductTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete product
     *
     * Delete a new product
     *
     * @Delete("/{id}")
     */
    public function delete($id)
    {
        $user = \Auth::user();

        if (Product::where('id', $id)->delete()) {
            $productModel = Product::with('coverImage')->get();
            return $this->response->collection($productModel, new ProductTransformer);
        }

        return $this->response->errorInternal();
    }

}
