<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Transformers\NewsTransformer;
use Illuminate\Http\Request;

/**
 * News resource representation.
 *
 * @Resource("News", uri="/news")
 */
class NewsController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show news listing
     *
     * @Get("/")
     * Get a JSON representation listing of news listing
     */
    public function listing(Request $request)
    {
        $isPublished = $request->get('isPublished');
        if($isPublished){
            $newsModel = News::with('coverImage')->where('status', 'active')->orderBy('news_date','desc')->get();
        }else{
            $newsModel = News::with('coverImage')->get();
        }
        return $this->response->collection($newsModel, new NewsTransformer);
    }

    /**
     * Show news by id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of news listing
     */
    public function listById($id)
    {
        $newsModel = News::with('coverImage')->where('id', $id)->first();

        return $this->response->item($newsModel, new NewsTransformer);
    }

    /**
     * Create news
     *
     * Create a new news
     *
     * @Post("/")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'news_date' => 'required',
            'news_type' => 'required',
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? News::STATUS_ACTIVE : News::STATUS_INACTIVE;

        if (News::create($data)) {
            $newsModel = News::with('coverImage')->get();
            return $this->response->collection($newsModel, new NewsTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update news
     *
     * Update a new news
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'news_date' => 'required',
            'news_type' => 'required',
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? News::STATUS_ACTIVE : News::STATUS_INACTIVE;

        if (News::where('id', $id)->update($data)) {
            $newsModel = News::with('coverImage')->get();
            return $this->response->collection($newsModel, new NewsTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete news
     *
     * Delete a new news
     *
     * @Delete("/{id}")
     */
    public function delete($id)
    {
        $user = \Auth::user();

        if (News::where('id', $id)->delete()) {
            $newsModel = News::with('coverImage')->get();
            return $this->response->collection($newsModel, new NewsTransformer);
        }

        return $this->response->errorInternal();
    }

}
