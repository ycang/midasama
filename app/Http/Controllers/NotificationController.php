<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Transformers\NotificationTransformer;
use Illuminate\Http\Request;

/**
 * User resource representation.
 *
 * @Resource("Notification", uri="/notification")
 */
class NotificationController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show user's notification listing
     *
     * Get a JSON representation listing of user's notification listing
     */
    public function listing()
    {
        $user = \Auth::user();

        $notificationModel = Notification::where('user_id', $user->id)->get();

        return $this->response->collection($notificationModel, new NotificationTransformer);
    }

}
