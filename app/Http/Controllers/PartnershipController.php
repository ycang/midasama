<?php

namespace App\Http\Controllers;

use App\Models\Partnership;
use App\Transformers\PartnershipTransformer;
use Illuminate\Http\Request;

/**
 * Partnership resource representation.
 *
 * @Resource("Partnership", uri="/partnership")
 */
class PartnershipController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show partnership listing
     *
     * @Get("/")
     * Get a JSON representation listing of partnership listing
     */
    public function listing(Request $request)
    {
        $isPublished = $request->get('isPublished');
        if ($isPublished) {
            $partnershipModel = Partnership::with('coverImage', 'certificates')->where('status', 'active')->get();
        } else {
            $partnershipModel = Partnership::with('coverImage', 'certificates')->get();
        }
        return $this->response->collection($partnershipModel, new PartnershipTransformer);
    }

    /**
     * Show partnership by id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of partnership listing
     */
    public function listById($id)
    {
        $partnershipModel = Partnership::with('coverImage', 'certificates', 'certificates.coverImage')->where('id', $id)->first();

        return $this->response->item($partnershipModel, new PartnershipTransformer);
    }

    /**
     * Create partnership
     *
     * Create a new partnership
     *
     * @Post("/")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Partnership::STATUS_ACTIVE : Partnership::STATUS_INACTIVE;

        if (Partnership::create($data)) {
            $partnershipModel = Partnership::with('coverImage', 'certificates')->get();
            return $this->response->collection($partnershipModel, new PartnershipTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update partnership
     *
     * Update a new partnership
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Partnership::STATUS_ACTIVE : Partnership::STATUS_INACTIVE;

        if (Partnership::where('id', $id)->update($data)) {
            $partnershipModel = Partnership::with('coverImage', 'certificates')->get();
            return $this->response->collection($partnershipModel, new PartnershipTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete partnership
     *
     * Delete a new partnership
     *
     * @Delete("/{id}")
     */
    public function delete($id)
    {
        $user = \Auth::user();

        if (Partnership::where('id', $id)->delete()) {
            $partnershipModel = Partnership::with('coverImage', 'certificates')->get();
            return $this->response->collection($partnershipModel, new PartnershipTransformer);
        }

        return $this->response->errorInternal();
    }

}
