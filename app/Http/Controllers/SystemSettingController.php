<?php

namespace App\Http\Controllers;

use App\Models\SystemSetting;
use App\Transformers\SystemSettingTransformer;
use Illuminate\Http\Request;

/**
 * SystemSetting resource representation.
 *
 * @Resource("SystemSetting", uri="/systemSetting")
 */
class SystemSettingController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show systemSetting listing
     *
     * @Get("/")
     * Get a JSON representation listing of systemSetting listing
     */
    public function listing(Request $request)
    {
        $systemSettingModel = SystemSetting::all();
        return $this->response->collection($systemSettingModel, new SystemSettingTransformer);
    }

    /**
     * Update systemSetting
     *
     * Update a new systemSetting
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update(Request $request)
    {
        $user = \Auth::user();

        // check this user has credit
        $params = $request->all();

        $data = $params;
        if($params['about_us_video_url']){
            SystemSetting::where('code', 'about_us_video_url')->update(array('value' => $params['about_us_video_url']));
        }

        $systemSettingModel = SystemSetting::all();
        return $this->response->collection($systemSettingModel, new SystemSettingTransformer);
    }

}
