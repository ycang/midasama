<?php

namespace App\Http\Controllers;

use App\Models\Academy;
use App\Transformers\AcademyTransformer;
use Illuminate\Http\Request;
use OwenIt\Auditing\Facades\Auditor;

/**
 * Academy resource representation.
 *
 * @Resource("Academy", uri="/academy")
 */
class AcademyController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show academy listing
     *
     * @Get("/")
     * Get a JSON representation listing of academy listing
     */
    public function listing()
    {
        $academyModel = Academy::all();
        return $this->response->collection($academyModel, new AcademyTransformer);
    }

    /**
     * Show academy by id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of academy listing
     */
    public function listById($id)
    {
        $academyModel = Academy::where('id', $id)->first();

        return $this->response->item($academyModel, new AcademyTransformer);
    }

    /**
     * Create academy
     *
     * Create a new academy
     *
     * @Post("/")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'contact_no' => 'required',
            'nationality' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = Academy::STATUS_PENDING_FOR_PAYMENT;

        if (Academy::create($data)) {
            return $this->response->accepted('Form submitted', 200);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update academy
     *
     * Update a new academy
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;

        if ($academyModel = Academy::find($id)->update($data)) {
            $academyModel = Academy::all();
            return $this->response->collection($academyModel, new AcademyTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete academy
     *
     * Delete a new academy
     *
     * @Delete("/{id}")
     */
    public function delete($id)
    {
        $user = \Auth::user();

        if (Academy::where('id', $id)->delete()) {
            $academyModel = Academy::all();
            return $this->response->collection($academyModel, new AcademyTransformer);
        }

        return $this->response->errorInternal();
    }

}
