<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Transformers\CertificateTransformer;
use Illuminate\Http\Request;

/**
 * Certificate resource representation.
 *
 * @Resource("Certificate", uri="/certificate")
 */
class CertificateController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show certificate listing
     *
     * @Get("/")
     * Get a JSON representation listing of certificate listing
     */
    public function listing(Request $request)
    {
        $isPublished = $request->get('isPublished');
        if($isPublished){
            $certificateModel = Certificate::with('coverImage')->where('status', 'active')->orderBy('position','desc')->get();
        }else{
            $certificateModel = Certificate::with('coverImage')->get();
        }
        return $this->response->collection($certificateModel, new CertificateTransformer);
    }

    /**
     * Show certificate by id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of certificate listing
     */
    public function listById($id)
    {
        $certificateModel = Certificate::with('coverImage')->where('id', $id)->first();

        return $this->response->item($certificateModel, new CertificateTransformer);
    }

    /**
     * Show certificate by partnership id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of certificate listing
     */
    public function listByNewsId($partnership_id)
    {
        $certificateModel = Certificate::with('coverImage')->where('partnership_id', $partnership_id)->orderBy('id','desc')->get();

        return $this->response->collection($certificateModel, new CertificateTransformer);
    }

    /**
     * Create certificate
     *
     * Create a new certificate
     *
     * @Post("/")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request, $partnership_id)
    {
        $user = \Auth::user();

        // check this user has credit
        $params = $request->all();

        if($params['certificate_images']){
            foreach ($params['certificate_images'] as $k => $v){
                $data['position'] = 0;
                $data['partnership_id'] = $partnership_id;
                $data['cover_image'] = $v;
                $data['status'] = Certificate::STATUS_INACTIVE;
                Certificate::create($data);
            }
        }

        $certificateModel = Certificate::with('coverImage')->where('partnership_id', $partnership_id)->orderBy('id','desc')->get();

        if($certificateModel){
            return $this->response->collection($certificateModel, new CertificateTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update certificate
     *
     * Update a new certificate
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'title_en' => 'required',
            'title_cn' => 'required',
            'content_en' => 'required',
            'content_cn' => 'required',
            'cover_image' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['cover_image'] = ($params['cover_image']) ? $params['cover_image'] : '' ;
        $data['status'] = ($params['status']) ? Certificate::STATUS_ACTIVE : Certificate::STATUS_INACTIVE;

        if (Certificate::where('id', $id)->update($data)) {
            $certificateModel = Certificate::with('coverImage')->get();
            return $this->response->collection($certificateModel, new CertificateTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete certificate
     *
     * Delete a new certificate
     *
     * @Delete("/{id}")
     */
    public function delete($id,$partnership_id)
    {
        $user = \Auth::user();

        if (Certificate::where('id', $id)->delete()) {
            $certificateModel = Certificate::with('coverImage')->where('partnership_id', $partnership_id)->orderBy('id','desc')->get();
            return $this->response->collection($certificateModel, new CertificateTransformer);
        }

        return $this->response->errorInternal();
    }

}
