<?php

namespace App\Http\Controllers;

use App\Models\Knowledge;
use App\Transformers\KnowledgeTransformer;
use Illuminate\Http\Request;

/**
 * Knowledge resource representation.
 *
 * @Resource("Knowledge", uri="/knowledge")
 */
class KnowledgeController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show knowledge listing
     *
     * @Get("/")
     * Get a JSON representation listing of knowledge listing
     */
    public function listing(Request $request)
    {
        $isPublished = $request->get('isPublished');
        if($isPublished){
            $knowledgeModel = Knowledge::where('status', 'active')->get();
        }else{
            $knowledgeModel = Knowledge::all();
        }
        return $this->response->collection($knowledgeModel, new KnowledgeTransformer);
    }

    /**
     * Show knowledge by id
     *
     * @Get("/{id}")
     * Get a JSON representation listing of knowledge listing
     */
    public function listById($id)
    {
        $knowledgeModel = Knowledge::where('id', $id)->first();

        return $this->response->item($knowledgeModel, new KnowledgeTransformer);
    }

    /**
     * Create knowledge
     *
     * Create a new knowledge
     *
     * @Post("/")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'title_en' => 'required',
            'title_cn' => 'required',
            'video_link' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Knowledge::STATUS_ACTIVE : Knowledge::STATUS_INACTIVE;

        if (Knowledge::create($data)) {
            $knowledgeModel = Knowledge::all();
            return $this->response->collection($knowledgeModel, new KnowledgeTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update knowledge
     *
     * Update a new knowledge
     *
     * @Put("/{id}")
     * @Request({"title_en": "foo", "title_cn": "foo", "subtitle_en": "foo", "subtitle_cn": "foo"})
     */
    public function update($id, Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'title_en' => 'required',
            'title_cn' => 'required',
            'video_link' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        // check this user has credit
        $params = $request->all();

        $data = $params;
        $data['status'] = ($params['status']) ? Knowledge::STATUS_ACTIVE : Knowledge::STATUS_INACTIVE;

        if (Knowledge::where('id', $id)->update($data)) {
            $knowledgeModel = Knowledge::all();
            return $this->response->collection($knowledgeModel, new KnowledgeTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Delete knowledge
     *
     * Delete a new knowledge
     *
     * @Delete("/{id}")
     */
    public function delete($id)
    {
        $user = \Auth::user();

        if (Knowledge::where('id', $id)->delete()) {
            $knowledgeModel = Knowledge::all();
            return $this->response->collection($knowledgeModel, new KnowledgeTransformer);
        }

        return $this->response->errorInternal();
    }

}
