<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Property;
use App\Transformers\PropertyTransformer;
use Illuminate\Support\Facades\File;

/**
 * User resource representation.
 *
 * @Resource("User", uri="/user")
 */
class UserController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show specific user
     *
     * Get a JSON representation specific user
     *
     * @Get("/me")
     */
    public function show()
    {
        $user = \Auth::user();

        $userModel = User::with('role')->find($user->id);

        return $this->response->item($userModel, new UserTransformer);
    }

    /**
     * Create user
     *
     * Create specific user with a `firstname` , `lastname`, `email` , `password`.
     *
     * @Post("/create")
     * @Request({"firstname": "foo", "lastname": "foo" , "email" : "foo@gmail.com" , "password" : "foo.1234!"})
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'password' => 'required',
            'user_type' => 'required',
            'email' => 'required|unique:users'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['api_token'] = str_random(30);

        if ($result = User::create($data)) {
            $role_id = ($data['user_type'] == 'agent') ? 1 : 2;
            RoleUser::create(['user_id' => $result->id, 'role_id' => $role_id]);
            $userData = User::with('role')->find($result->id);
            return $this->response->item($userData, new UserTransformer);
        }

        return $this->response->errorInternal();
    }

    /**
     * Update specific user
     *
     * Update specific user with a `firstname` , `password`, `lastname`.
     *
     * @Post("/update")
     * @Request({"firstname": "foo", "lastname": "foo" , "email" : "foo@gmail.com" , "password" : "foo.1234!"})
     */
    public function update(Request $request)
    {
        $user = \Auth::user();

        $validator = \Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        $data = [];
        if ($request['password']) {
            $data['password'] = Hash::make($request['password']);
        }
        $data['firstname'] = $request['firstname'];
        $data['lastname'] = $request['lastname'];

        $userModel = User::with('role')->find($user->id);

        if ($userModel->update($data)) {
            return $this->response->item($userModel, new UserTransformer);
        }

        return $this->response->errorInternal();
    }

    public function uploadProfileImage(Request $request){

        $user = \Auth::user();

        $userModel = User::with('role')->find($user->id);
        $uploadedProfileImage = $request->file('uploadedProfileImage');

        if(!File::exists(base_path() . '/public/profile-image/')) {
            File::makeDirectory(base_path() . '/public/profile-image/', $mode = 0755, true, true);
        }

        if ($uploadedProfileImage) {

            $filename = md5($uploadedProfileImage->getFilename(). time()).'.'.$uploadedProfileImage->getClientOriginalExtension();
            try{
                move_uploaded_file($uploadedProfileImage->getPathname(), base_path() . '/public/profile-image/' . $filename);
            }catch (Exception $e){
                return $this->response->error($e->getMessage(), 400);
            }
            $userModel->profile_image = $filename;
        }

        if ($userModel->save()) {
            return $this->response->item($userModel, new UserTransformer);
        }
    }

    /**
     * Reset Password
     *
     * reset password with a `reset_token` and `password`.
     *
     * @Post("/password/reset")
     * @Request({"reset_token": "a0dj0232knsdsad" , "password" : "foo.12314!"})
     */
    public function passwordReset(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'reset_token' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        $userModel = User::where('reset_token', $request['reset_token']);
        $userData = $userModel->get();
        if ($userData->count() == 0) {
            return $this->response->error('Invalid reset token or reset token expired.', 400);
        }

        $data = [];
        $data['password'] = Hash::make($request['password']);
        $data['reset_token'] = null;

        if ($userModel->update($data)) {
            return response()->json(['status_code' => 200, 'message' => 'Your password has been reset. Please login again.']);
        }

        return $this->response->errorInternal();
    }

    /**
     * Request Reset Password
     *
     * request reset password with a `email`.
     *
     * @Post("/password/request-reset")
     * @Request({"email": "foo@gmail.com"})
     */
    public function requestPasswordReset(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        $userModel = User::where('email', $request['email']);
        $userData = $userModel->get();
        if ($userData->count() == 0) {
            return $this->response->error('Email doesn\'t exist in our system', 400);
        }

        $data = [];
        $data['reset_token'] = str_random(30);

        if ($userModel->update($data)) {
            return response()->json(['status_code' => 200, 'message' => 'A reset password email has been sent to your email.']);
        }

        return $this->response->errorInternal();
    }

    /**
     * Login user
     *
     * login user with a `email` and `password`.
     *
     * @Post("/login")
     * @Request({"email": "foo@gmail.com" , "password": "foo.1234!"})
     */
    public function login(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $responseMessage = '';
            foreach ($errors as $key => $error_message) {
                $responseMessage .= $error_message;
            }
            return $this->response->error($responseMessage, 400);
        }

        $roleModel = Role::where('name',$request['user_type'])->first();

        if(!$roleModel){
            return $this->response->error('Login invalid', 400);
        }

        if(!$roleModel->id){
            return $this->response->error('User Type invalid', 400);
        }

        $role_id = $roleModel->id;
        $userModel = User::with('role')->where('email', $request['email']);
        $userData = $userModel->first();
        if ($userData && Hash::check($request['password'], $userData->password) && $userData->role->role_id == $role_id) {
            return response()->json([
                'status_code' => 200,
                'message' => 'Login successful.',
                'data' => [
                    'id' => $userData->id,
                    'role_id' => $userData->role->role_id,
                    'token' => $userData->api_token,
                    'lastname' => $userData->lastname,
                    'firstname' => $userData->firstname,
                    'profile_image' => ($userData->profile_image) ? env('API_DOMAIN').'/profile-image/'.$userData->profile_image : '',
                    'email' => $userData->email
                ]]);
        }
        return $this->response->error('Login invalid', 400);
    }
}
