FORMAT: 1A

# Property Networks API Docs

# Properties [/property]
Property resource representation.

## Show all properties [GET /property]
Get a JSON representation of all the properties.

## Show specific property [GET /property/{slug}]
Get a JSON representation specific property

## Create property [POST /property/create]
Create a new property with a `title` and `description` and `no_of_bedroom`.

+ Request (application/json)
    + Body

            {
                "title": "foo",
                "description": "foo",
                "no_of_bedroom": "2"
            }

## Update specific property [POST /property/update]
Update specific property with a `title` and `description` and `no_of_bedroom`.

+ Request (application/json)
    + Body

            {
                "title": "foo",
                "description": "foo",
                "no_of_bedroom": "2"
            }

# User [/user]
User resource representation.

## Show specific user [GET /user/me]
Get a JSON representation specific user

## Create user [POST /user/create]
Create specific user with a `firstname` , `lastname`, `email` , `password` , `mobile_no`.

+ Request (application/json)
    + Body

            {
                "firstname": "foo",
                "lastname": "foo",
                "email": "foo@gmail.com",
                "password": "foo.1234!",
                "mobile_no": "0199929292"
            }

## Update specific user [POST /user/update]
Update specific user with a `firstname` ,`mobile_no`, `password`, `lastname`.

+ Request (application/json)
    + Body

            {
                "firstname": "foo",
                "lastname": "foo",
                "email": "foo@gmail.com",
                "password": "foo.1234!",
                "mobile_no": "0199929292"
            }

## Reset Password [POST /user/password/reset]
reset password with a `reset_token` and `password`.

+ Request (application/json)
    + Body

            {
                "reset_token": "a0dj0232knsdsad",
                "password": "foo.12314!"
            }

## Request Reset Password [POST /user/password/request-reset]
request reset password with a `email`.

+ Request (application/json)
    + Body

            {
                "email": "foo@gmail.com"
            }

## Login user [POST /user/login]
login user with a `email` and `password`.

+ Request (application/json)
    + Body

            {
                "email": "foo@gmail.com",
                "password": "foo.1234!"
            }