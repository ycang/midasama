/* eslint no-console:0 */
/* eslint consistent-return:0 */
const path          = require('path');
const webpack       = require('webpack');
const express       = require('express');
const devMiddleware = require('webpack-dev-middleware');
const hotMiddleware = require('webpack-hot-middleware');
const config        = require('./webpack.hot.reload.config');
const chalk         = require('chalk');
const bodyParser    = require("body-parser");

const app       = express();
const compiler  = webpack(config);


app.use(devMiddleware(compiler, {
  publicPath: config.output.publicPath,
  historyApiFallback: true
}));

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(hotMiddleware(compiler));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/payment/response', (req, res) => {
  const {amount,orderid,skey, tranID, status, appcode, channel, error_code ,currency, error_desc} = req.body;
  console.log(req.body);
  res.redirect(`/payment/response?currency=${currency}&amount=${amount}&orderid=${orderid}&skey=${skey}&tranID=${tranID}&status=${status}&appcode=${appcode}&channel=${channel}&error_code=${error_code}&error_desc=${error_desc}`);
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(3001, (err) => {
  if (err) {
    return console.error(err);
  }
  console.log(
    `
      =====================================================
      -> Server (${chalk.bgBlue('Hot reload')}) 🏃 (running) on ${chalk.green('localhost')}:${chalk.green('3001')}
      =====================================================
    `
  );
});
