// @flow
const express   = require('express');
const path      = require('path');
const bodyParser    = require("body-parser");

const app       = express();
const DOCS_PATH = '../../docs/';
const PORT      = 8082;
const IP_ADRESS = 'localhost';
require('newrelic');

app.set('port', PORT);
app.set('ipAdress', IP_ADRESS);

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.get('*.js', function(req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  res.set('Content-Type', 'text/javascript');
  next();
});

app.get('*.css', function(req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  res.set('Content-Type', 'text/css');
  next();
});

app.use(express.static(path.join(__dirname, DOCS_PATH)));

app.get('/*', (req, res) => res.sendFile(path.join(__dirname, DOCS_PATH, 'index.html')));

app.post('/payment/response', (req, res) => {
  const {amount,orderid,skey, tranID, status, appcode, channel, error_code ,currency, error_desc} = req.body;
  res.redirect(`/payment/response?currency=${currency}&amount=${amount}&orderid=${orderid}&skey=${skey}&tranID=${tranID}&status=${status}&appcode=${appcode}&channel=${channel}&error_code=${error_code}&error_desc=${error_desc}`);
  res.sendFile(path.join(__dirname, 'index.html'));
});

/* eslint-disable no-console */
app.listen(
  PORT,
  IP_ADRESS,
  () => console.log(`
    ==============================================
    -> Server 🏃 (running) on ${IP_ADRESS}:${PORT}
    ==============================================
  `)
);
/* eslint-enable no-console */
