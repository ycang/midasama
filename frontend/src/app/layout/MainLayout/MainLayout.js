import React, { Component } from "react";
import PropTypes from "prop-types";
import navigationModel from "../../config/navigation.json";
import { NavigationBar } from "../../components/index";
import { GlobalVars } from "../../config/global_vars";
import auth from "../../services/auth/index";
import intl from "react-intl-universal";
import { Link } from "react-router-dom";
import { Row, Col, Layout, Icon } from "antd";
import logo from "../../images/Slicing-02.png";
import { withCookies, Cookies } from "react-cookie";
import cx from "classnames";
import { isEnUsLocale } from "../../services/utils";
const { Header, Content, Footer } = Layout;

class MainLayout extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    removeGlobalMessage: PropTypes.func.isRequired,

    // views:
    currentView: PropTypes.string
  };

  constructor(props) {
    super(props);
    var dateObj = new Date();
    this.state = {
      collapsed: false,
      navModel: navigationModel,
      currentYear: dateObj.getUTCFullYear()
    };
  }

  componentDidMount() {
    const { cookies } = this.props;
    const lang = cookies.get("locale") || "zh-CN";
    $("html , body")
      .addClass("main-layout")
      .addClass(lang);
    this.fixedHeader();
  }

  fixedHeader() {
    $(window).scroll(function() {
      var header = $(document).scrollTop();
      var headerHeight = $(".js-header-navigation").outerHeight();
      if (header > headerHeight) {
        $(".js-header-navigation").addClass("fixed");
      } else {
        $(".js-header-navigation").removeClass("fixed");
      }
    });
  }

  redirectToUrl(pathname) {
    const { history } = this.props;
    history.push({ pathname });
  }

  getRegisterLink = () => {
    const { history } = this.props;
    let redirectToUrl = "";
    if (history.location.pathname == "/academy") {
      redirectToUrl = isEnUsLocale()
        ? "https://investor.econnez.com/x/en/register-3-2/"
        : "https://investor.econnez.com/x/cn/register-3-2/";
    } else {
      redirectToUrl = "https://member.midasama.com/Account/Register";
    }
    return redirectToUrl;
  };

  render() {
    const { navModel } = this.state;
    const { userAuth, history } = this.props;
    return (
      <Layout className="layout main-layout">
        <Header className="layout-header layout-header--blue js-header-navigation">
          <Row type="flex" justify="start">
            <Col xs={12} md={6} lg={4} xl={3}>
              <div
                className="logo is-link"
                onClick={() => this.redirectToUrl("/")}
              >
                <img src={logo} alt="logo" width="100%" className="logo" />
              </div>
            </Col>
            <Col xs={12} md={18} lg={20} xl={21}>
              <NavigationBar
                brand={navModel.brand}
                history={history}
                navModel={navModel}
                handleRegisterClick={this.handleRegisterClick}
                handleLoginClick={this.handleLoginClick}
                userAuth={userAuth}
                accountNavigationList={auth.getAccountMNavigationMenu()}
              />
            </Col>
          </Row>
        </Header>
        <Content>{this.props.children}</Content>
        <Row className="text-center bg-madison">
          <Col md={12} className="misc-footer-register-link p-t-20 p-b-20">
            <Link
              target="_blank"
              className="h3 text-primary cursor-pointer"
              to={this.getRegisterLink()}
            >
              {intl.getHTML("footer.register.heading")}
              <Icon type="right" className="p-l-15" />
            </Link>
            <h5 className="text-white m-t-5">
              {history.location.pathname == "/academy"
                ? intl.getHTML("footer.register.subHeading_academy")
                : intl.getHTML("footer.register.subHeading")}
            </h5>
          </Col>
          <Col md={12} className="p-t-20 p-b-20">
            <p target="_blank" className="h3 text-primary m-0">
              {history.location.pathname == "/academy"
                ? intl.getHTML("footer.login.heading_academy")
                : intl.getHTML("footer.login.heading")}
              <Icon type="right" className="p-l-15" />
            </p>
            <h5 className="text-white m-t-5">
              {history.location.pathname == "/academy"
                ? intl.getHTML("footer.login.subHeading_academy")
                : intl.getHTML("footer.login.subHeading")}
            </h5>
          </Col>
        </Row>
        <Footer className="bg-midnight">
          <Row className="m-t-60">
            <Col md={12}>
              <Row>
                <Col md={18}>
                  <img
                    src={logo}
                    className="m-b-30"
                    style={{ maxWidth: 246 }}
                  />
                </Col>
              </Row>
            </Col>
            <Col md={12}>
              <Row>
                <Col xs={12} xl={6}>
                  <ul className="ant-menu-footer">
                    <li className="ant-menu-footer__item-wrapper">
                      <Link
                        to={"/about"}
                        className={cx("ant-menu-footer__item", {
                          active: this.props.match.path == "/about"
                        })}
                      >
                        {intl.getHTML("menu.item1")}
                      </Link>
                    </li>
                    <li className="ant-menu-footer__item-wrapper">
                      <Link
                        to={"/products"}
                        className={cx("ant-menu-footer__item", {
                          active: this.props.match.path == "/products"
                        })}
                      >
                        {intl.getHTML("menu.item2")}
                      </Link>
                    </li>
                    <li className="ant-menu-footer__item-wrapper">
                      <Link
                        to={"/platform"}
                        className={cx("ant-menu-footer__item", {
                          active: this.props.match.path == "/platform"
                        })}
                      >
                        {intl.getHTML("menu.item3")}
                      </Link>
                    </li>
                    <li className="ant-menu-footer__item-wrapper">
                      <Link
                        to={"/latest-news"}
                        className={cx("ant-menu-footer__item", {
                          active: this.props.match.path == "/latest-news"
                        })}
                      >
                        {intl.getHTML("menu.item4")}
                      </Link>
                    </li>
                  </ul>
                </Col>
                <Col xs={12} xl={6}>
                  <ul className="ant-menu-footer">
                    <li className="ant-menu-footer__item-wrapper">
                      <Link
                        to={"/partnerships"}
                        className={cx("ant-menu-footer__item", {
                          active: this.props.match.path == "/partnerships"
                        })}
                      >
                        {intl.getHTML("menu.item5")}
                      </Link>
                    </li>
                    <li className="ant-menu-footer__item-wrapper">
                      <Link
                        to={"/education"}
                        className={cx("ant-menu-footer__item", {
                          active: this.props.match.path == "/knowledge"
                        })}
                      >
                        {intl.getHTML("menu.item6")}
                      </Link>
                    </li>
                  </ul>
                </Col>
              </Row>
            </Col>
            <Col xs={24} className="m-t-60">
              <small className="text-gray">
                {intl.getHTML("footer.disclaimer.heading")}
              </small>
              <small className="text-gray">
                {intl.getHTML("footer.disclaimer.subHeading")}
              </small>
              <small className="text-gray">
                {intl.getHTML("footer.disclaimer.subHeading2")}
              </small>
            </Col>
            <Col xs={24}>
              <p className="text-right text-white">
                <small>
                  {intl.getHTML("footer.copyright")}{" "}
                  <span className="text-letterspacing-0">
                    &copy; 2012 - {this.state.currentYear}{" "}
                    {GlobalVars.brandName}
                  </span>
                </small>
              </p>
            </Col>
          </Row>
        </Footer>
      </Layout>
    );
  }

  handleOnClickCloseGlobalMessage = () => {
    this.props.removeGlobalMessage();
  };
}

export default withCookies(MainLayout);
