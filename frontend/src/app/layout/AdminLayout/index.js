// @flow weak

import {connect} from 'react-redux';
import {
  bindActionCreators,
  compose
} from 'redux';
import AdminLayout from './AdminLayout';
import {withRouter} from 'react-router';
import * as viewsActions from '../../redux/modules/reducers';
import * as userAuthActions from '../../redux/modules/userAuth';
import * as systemActions from '../../redux/modules/system';

const mapStateToProps = (state) => {
  return {
    // views
    currentView: state.views.currentView,

    // useAuth:
    isAuthenticated: state.userAuth.isAuthenticated,
    isFetching: state.userAuth.isFetching,
    isLogging: state.userAuth.isLogging,
    globalMessages: state.system.globalMessages,
    userAuth: state.userAuth,
    notification: state.notification,
    pitching: state.pitching,
    properties: state.properties,
    propertyQuestion: state.propertyQuestion,
    system: state.system,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...systemActions
    },
    dispatch
  );
};

// we use here compose (from redux) just for conveniance (since compose(f,h, g)(args) looks better than f(g(h(args))))
export default compose(
  withRouter, // IMPORTANT: witRouter is "needed here" to avoid blocking routing:
  connect(mapStateToProps, mapDispatchToProps)
)(AdminLayout);
