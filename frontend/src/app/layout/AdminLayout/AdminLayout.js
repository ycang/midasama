import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import navigationModel from '../../config/navigation.json';
import {
  LeftNavigation
} from '../../components/index';
import Drawer from 'react-motion-drawer';
import {Layout, Breadcrumb, Icon} from 'antd';
import logo from '../../images/Slicing-02.png';

const {Header, Content, Footer, Sider} = Layout;
const BreadcrumbItem = Breadcrumb.Item;

class AdminLayout extends Component {

  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    removeGlobalMessage: PropTypes.func.isRequired,

    // views:
    currentView: PropTypes.string

  };

  constructor(props) {
    super(props);
    this.state = {
      navModel: navigationModel,
      routes: [
        {icon: 'picture', label: 'Banner', path: '/admin/banner'},
        {icon: 'notification', label: 'News', path: '/admin/news'},
        {icon: 'table', label: 'Product', path: '/admin/product'},
        {icon: 'global', label: 'Partnership', path: '/admin/partnership'},
        {icon: 'video-camera', label: 'Education', path: '/admin/education'},
        {icon: 'solution', label: 'Academy', path: '/admin/academy'},
        {icon: 'setting', label: 'System Setting', path: '/admin/systemSetting'},
        {icon: 'logout', label: 'Logout', path: '/logout'}
      ],
      currentBreadcrumbLabel: '',
    }
  }

  componentDidMount() {
    const {checkUserIsConnected} = this.props;
    checkUserIsConnected();
    $('html , body').addClass('admin-layout');
    this.setCurrentBreadcrumbFromRouting();
  }

  render() {
    const {navModel} = this.state;
    const {message, userAuth, history, globalMessages} = this.props;
    return (
      <Layout style={{minHeight: '100vh'}} className="admin-layout">
        <Drawer open={ ($('html').hasClass('tablet') || $('html').hasClass('mobile')) && this.state.collapsed} onChange={this.onChangeDrawer}>
          <div className="logo__wrapper">
            <img src={logo} alt="logo" className="logo"/>
          </div>
          <LeftNavigation match={this.props.match}
                          history={this.props.history}
                          onClickCollapse={this.onCollapse}
                          handleChangeBreadcrumb={ (label) => this.handleChangeBreadcrumb(label)}/>
        </Drawer>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
        >
          <div className="logo__wrapper">
            <img src={logo} alt="logo" className="logo"/>
          </div>
          <LeftNavigation match={this.props.match} history={this.props.history} handleChangeBreadcrumb={ (label) => this.handleChangeBreadcrumb(label)}/>
        </Sider>
        <Layout>
          <Header style={{background: '#fff', padding: 0}}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.onCollapse}
            />
          </Header>
          <Content style={{margin: '0 16px'}}>
            <Breadcrumb style={{margin: '16px 0'}}>
              <BreadcrumbItem>Home</BreadcrumbItem>
              <BreadcrumbItem>{this.state.currentBreadcrumbLabel}</BreadcrumbItem>
            </Breadcrumb>
            <div style={{padding: 24, background: '#fff', minHeight: 360}}>
              {this.props.children}
            </div>
          </Content>
          <Footer style={{textAlign: 'center'}}>
            Admin Panel
          </Footer>
        </Layout>
      </Layout>
    )
  }

  handleChangeBreadcrumb(label){
    this.setState({currentBreadcrumbLabel: label});
  }

  setCurrentBreadcrumbFromRouting(){
    this.state.routes.map((route, i) => {
      if (this.props.match.path.indexOf(route.path) > -1) {
        this.setState({currentBreadcrumbLabel: route.label});
      }
    });
  }

  onChangeDrawer = (e) =>{
    this.setState({
      collapsed: e,
    });
  };

  onCollapse = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
}

export default AdminLayout;
