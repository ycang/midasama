import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import navigationModel from '../../config/navigation.json';
import {GlobalVars} from '../../config/global_vars';
import {Row, Col, Modal, Layout} from 'antd';

const {Header, Content, Footer} = Layout;
class BlankLayout extends Component {

  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    removeGlobalMessage: PropTypes.func.isRequired,

    // views:
    currentView: PropTypes.string

  };

  state = {
    navModel: navigationModel
  };

  componentDidMount() {
    const {checkUserIsConnected} = this.props;
    checkUserIsConnected();
    $('html , body').addClass('blank-layout');
  }

  redirectToUrl(pathname) {
    const {
      history
    } = this.props;

    history.push({pathname});
  }

  render() {
    const {navModel} = this.state;
    const {message, userAuth, history, globalMessages} = this.props;
    return (
      <Layout style={{minHeight: '100vh'}} className="blank-layout">
        <Layout>
          <Content>
            {this.props.children}
          </Content>
          <Footer style={{textAlign: 'center'}}>
            {GlobalVars.brandName} Admin Panel
          </Footer>
        </Layout>
      </Layout>
    )
  }
}

export default BlankLayout;
