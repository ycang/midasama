export const AccountNavigationMenu = {
  "normalUser": [
    {"path": "/user/my-profile", "label": "My Profile" , "icon": "idcard"},
    {"path": "/user/my-properties", "label": "My Properties" , "icon": "shop"},
    {"path": "/user/my-favourite-properties", "label": "My Favourite Properties" , "icon": "heart-o"},
    {"path": "/user/my-enquiry", "label": "My Enquiry" , "icon": "bulb"},
    {"path": "/user/my-messages", "label": "My Messages" , "icon": "inbox"}
  ],
  "agent": [
    {"path": "/user/my-profile", "label": "My Profile" , "icon": "idcard"},
    {"path": "/user/my-credits", "label": "My Credits" , "icon": "copyright"},
    {"path": "/user/my-messages", "label": "My Messages" , "icon": "message"},
    {"path": "/user/public-property-listing", "label": "Public Property Listing" , "icon": "shop"},
    {"path": "/user/my-pitching-properties", "label": "My Pitching Properties" , "icon": "solution"}
  ]
}
