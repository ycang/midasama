import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const CREATE_ASSET_REQUEST = 'CREATE_ASSET_REQUEST';
const CREATE_ASSET_FAIL = 'CREATE_ASSET_FAIL';
const CREATE_ASSET_SUCCESS = 'CREATE_ASSET_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
  list: [],
  current: {}
};

export default function (state = initialState, action) {
  const currentTime = moment().format();

  switch (action.type) {

    case CREATE_ASSET_FAIL:
    case CREATE_ASSET_REQUEST:
      return {
        ...state,
        actionTime: currentTime
      };
      break;

    case CREATE_ASSET_SUCCESS:
      const list = action.payload.data.data;
      return Object.assign({}, state, {
        ...state,
        actionTime: currentTime,
        list
      });
      break;

    default:
      return state;
  }
}


// /////////////////////
// action creators
// /////////////////////

export function createAsset(file) {
  const token = auth.getToken();
  const url = `${GlobalVars.apiServer}/asset`;
  const config = {
    headers: {
      authorization: `Bearer ${token}`,
      'content-type': 'multipart/form-data'
    }
  };
  const formData = new FormData();
  formData.append('file', file);

  return async (dispatch) => {
    return post(url, formData, config).then((data) => {
      return dispatch({type: CREATE_ASSET_SUCCESS, payload: data})
    }).catch((data) => {
      return dispatch({type: CREATE_ASSET_FAIL, payload: data})
    })
  };
}
