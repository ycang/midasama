import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_CERTIFICATE_LIST_REQUEST = 'FETCH_CERTIFICATE_LIST_REQUEST';
const FETCH_CERTIFICATE_LIST_FAIL = 'FETCH_CERTIFICATE_LIST_FAIL';
const FETCH_CERTIFICATE_LIST_SUCCESS = 'FETCH_CERTIFICATE_LIST_SUCCESS';

const FETCH_CERTIFICATE_LIST_BY_NEWS_ID_REQUEST = 'FETCH_CERTIFICATE_LIST_BY_NEWS_ID_REQUEST';
const FETCH_CERTIFICATE_LIST_BY_NEWS_ID_FAIL = 'FETCH_CERTIFICATE_LIST_BY_NEWS_ID_FAIL';
const FETCH_CERTIFICATE_LIST_BY_NEWS_ID_SUCCESS = 'FETCH_CERTIFICATE_LIST_BY_NEWS_ID_SUCCESS';

const FETCH_CERTIFICATE_BY_ID_REQUEST = 'FETCH_CERTIFICATE_BY_ID_REQUEST';
const FETCH_CERTIFICATE_BY_ID_FAIL = 'FETCH_CERTIFICATE_BY_ID_FAIL';
const FETCH_CERTIFICATE_BY_ID_SUCCESS = 'FETCH_CERTIFICATE_BY_ID_SUCCESS';

const CREATE_CERTIFICATE_REQUEST = 'CREATE_CERTIFICATE_REQUEST';
const CREATE_CERTIFICATE_FAIL = 'CREATE_CERTIFICATE_FAIL';
const CREATE_CERTIFICATE_SUCCESS = 'CREATE_CERTIFICATE_SUCCESS';

const EDIT_CERTIFICATE_REQUEST = 'EDIT_CERTIFICATE_REQUEST';
const EDIT_CERTIFICATE_FAIL = 'EDIT_CERTIFICATE_FAIL';
const EDIT_CERTIFICATE_SUCCESS = 'EDIT_CERTIFICATE_SUCCESS';

const DELETE_CERTIFICATE_REQUEST = 'DELETE_CERTIFICATE_REQUEST';
const DELETE_CERTIFICATE_FAIL = 'DELETE_CERTIFICATE_FAIL';
const DELETE_CERTIFICATE_SUCCESS = 'DELETE_CERTIFICATE_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
  list: [],
  current: {}
};

export default function (state = initialState, action) {
  const currentTime = moment().format();

  switch (action.type) {

    case DELETE_CERTIFICATE_FAIL:
    case DELETE_CERTIFICATE_REQUEST:
    case EDIT_CERTIFICATE_FAIL:
    case EDIT_CERTIFICATE_REQUEST:
    case CREATE_CERTIFICATE_FAIL:
    case CREATE_CERTIFICATE_REQUEST:
    case FETCH_CERTIFICATE_LIST_FAIL:
    case FETCH_CERTIFICATE_LIST_REQUEST:
    case FETCH_CERTIFICATE_LIST_BY_NEWS_ID_FAIL:
    case FETCH_CERTIFICATE_LIST_BY_NEWS_ID_REQUEST:
    case FETCH_CERTIFICATE_BY_ID_FAIL:
    case FETCH_CERTIFICATE_BY_ID_REQUEST:
      return {
        ...state,
        actionTime: currentTime
      };
      break;

    case DELETE_CERTIFICATE_SUCCESS:
    case EDIT_CERTIFICATE_SUCCESS:
    case CREATE_CERTIFICATE_SUCCESS:
    case FETCH_CERTIFICATE_LIST_BY_NEWS_ID_SUCCESS:
    case FETCH_CERTIFICATE_LIST_SUCCESS:
      const list = action.payload.data.data;
      return Object.assign({}, state, {
        ...state,
        actionTime: currentTime,
        list
      });
      break;

    case FETCH_CERTIFICATE_BY_ID_SUCCESS:
      const current = action.payload.data.data;
      return Object.assign({}, state, {
        ...state,
        actionTime: currentTime,
        current
      });
      break;

    default:
      return state;
  }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchCertificateList(params) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'get';
    let url = `${GlobalVars.apiServer}/certificate`;
    const isPublished = (params && params.isPublished) ? params.isPublished : '';
    if (isPublished) {
      url += '?isPublished=' + isPublished;
    }
    const headers = {};
    const options = {};

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: FETCH_CERTIFICATE_LIST_REQUEST,
          success: FETCH_CERTIFICATE_LIST_SUCCESS,
          fail: FETCH_CERTIFICATE_LIST_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function fetchCertificateListByPartnershipId(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'get';
    const url = `${GlobalVars.apiServer}/certificate/partnership/${id}`;
    const headers = {};
    const options = {};

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: FETCH_CERTIFICATE_LIST_BY_NEWS_ID_REQUEST,
          success: FETCH_CERTIFICATE_LIST_BY_NEWS_ID_SUCCESS,
          fail: FETCH_CERTIFICATE_LIST_BY_NEWS_ID_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function createCertificate({id, certificate_images}) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'post';
    const url = `${GlobalVars.apiServer}/certificate/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = {
      data: {
        certificate_images
      }
    };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: CREATE_CERTIFICATE_REQUEST,
          success: CREATE_CERTIFICATE_SUCCESS,
          fail: CREATE_CERTIFICATE_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function editCertificate({id, title_en, title_cn, content_en, content_cn, status, cover_image}) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'put';
    const url = `${GlobalVars.apiServer}/certificate/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = {
      data: {
        title_en,
        title_cn,
        cover_image,
        content_en,
        content_cn,
        status
      }
    };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: EDIT_CERTIFICATE_REQUEST,
          success: EDIT_CERTIFICATE_SUCCESS,
          fail: EDIT_CERTIFICATE_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function fetchCertificateById(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'get';
    const url = `${GlobalVars.apiServer}/certificate/${id}`;
    const headers = {};
    const options = {};

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: FETCH_CERTIFICATE_BY_ID_REQUEST,
          success: FETCH_CERTIFICATE_BY_ID_SUCCESS,
          fail: FETCH_CERTIFICATE_BY_ID_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function deleteCertificate(id, partnership_id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'delete';
    const url = `${GlobalVars.apiServer}/certificate/${id}/partnership/${partnership_id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = {};

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: DELETE_CERTIFICATE_REQUEST,
          success: DELETE_CERTIFICATE_SUCCESS,
          fail: DELETE_CERTIFICATE_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}
