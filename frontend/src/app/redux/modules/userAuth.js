// @flow weak
import moment from 'moment';
import {appConfig} from '../../config';
import {GlobalVars} from '../../config/global_vars';
import userInfosMockData from '../../mock/userInfosMock.json';
import auth from '../../services/auth';
import axios, {post} from 'axios';
// --------------------------------
// CONSTANTS
// --------------------------------
const REQUEST_USER_INFOS_DATA: string = 'REQUEST_USER_INFOS_DATA';
const RECEIVED_USER_INFOS_DATA: string = 'RECEIVED_USER_INFOS_DATA';
const ERROR_USER_INFOS_DATA: string = 'ERROR_USER_INFOS_DATA';

const REQUEST_CREATE_USER: string = 'REQUEST_CREATE_USER';
const RECEIVED_CREATE_USER: string = 'RECEIVED_CREATE_USER';
const ERROR_CREATE_USER: string = 'ERROR_CREATE_USER';

const REQUEST_LOG_USER: string = 'REQUEST_LOG_USER';
const RECEIVED_LOG_USER: string = 'RECEIVED_LOG_USER';
const ERROR_LOG_USER: string = 'ERROR_LOG_USER';

const REQUEST_REQUEST_RESET_PASSWORD: string = 'REQUEST_REQUEST_RESET_PASSWORD';
const RECEIVED_REQUEST_RESET_PASSWORD: string = 'RECEIVED_REQUEST_RESET_PASSWORD';
const ERROR_REQUEST_RESET_PASSWORD: string = 'ERROR_REQUEST_RESET_PASSWORD';

const REQUEST_RESET_PASSWORD: string = 'REQUEST_RESET_PASSWORD';
const RECEIVED_RESET_PASSWORD: string = 'RECEIVED_RESET_PASSWORD';
const ERROR_RESET_PASSWORD: string = 'ERROR_RESET_PASSWORD';

const CHECK_IF_USER_IS_AUTHENTICATED = 'CHECK_IF_USER_IS_AUTHENTICATED';

const DISCONNECT_USER = 'DISCONNECT_USER';
const CLEAR_USER_MESSAGE = 'CLEAR_USER_MESSAGE';
const UPDATE_IMAGE_FAIL = 'UPDATE_IMAGE_FAIL';

// --------------------------------
// REDUCER
// --------------------------------
const initialState = {
  // actions details
  isFetching: false,
  isLogging: false,
  time: '',

  // userInfos
  id: '',
  email: '',
  role_id: '',
  mobile_no: '',
  credit_amount: 0,
  firstname: '',
  lastname: '',
  profile_image: '',
  properties: [],
  favourite: {
    properties: [],
    agents: []
  },
  token: null,
  isAuthenticated: false   // authentication status (token based auth)
};

export default function (state = initialState, action) {
  const currentTime = moment().format();

  switch (action.type) {

    case CHECK_IF_USER_IS_AUTHENTICATED:
      return {
        ...state,
        actionTime: currentTime,
        isAuthenticated: action.isAuthenticated,
        token: action.token || initialState.token,
        id: action.id ? action.id : initialState.id,
        email: action.email ? action.email : initialState.email,
        credit_amount: action.credit_amount ? action.credit_amount : initialState.credit_amount,
        role_id: action.role_id ? action.role_id : initialState.role_id,
        mobile_no: action.mobile_no ? action.mobile_no : initialState.mobile_no,
        profile_image: action.profile_image ? action.profile_image : initialState.profile_image,
        firstname: action.firstname ? action.firstname : initialState.firstname,
        lastname: action.lastname ? action.lastname : initialState.firstname
      };
      break;

    case CLEAR_USER_MESSAGE:
      return {
        ...state,
        actionTime: currentTime,
        message: ''
      };
      break;

    case DISCONNECT_USER:
      return {
        ...state,
        actionTime: currentTime,
        isAuthenticated: false,
        token: initialState.token,
        id: initialState.id,
        email: initialState.email,
        credit_amount: initialState.credit_amount,
        profile_image: initialState.profile_image,
        firstname: initialState.firstname,
        lastname: initialState.lastname
      };
      break;

    case REQUEST_RESET_PASSWORD:
    case REQUEST_REQUEST_RESET_PASSWORD:
      return {
        ...state,
        actionTime: currentTime
      };
      break;

    case REQUEST_CREATE_USER:
      return {
        ...state,
        actionTime: currentTime,
        isLogging: true
      };
      break;

    case RECEIVED_CREATE_USER:
      const userData = action.payload.data.data;
      return {
        ...state,
        actionTime: currentTime,
        isAuthenticated: true,
        token: userData.token,
        id: userData.id,
        role_id: userData.role_id,
        email: userData.email,
        mobile_no: userData.mobile_no,
        credit_amount: userData.credit_amount,
        firstname: userData.firstname,
        lastname: userData.lastname,
        isLogging: false
      };
      break;

    case REQUEST_LOG_USER:
      return {
        ...state,
        actionTime: currentTime,
        isLogging: true
      };
      break;

    case UPDATE_IMAGE_FAIL:
      return {
        ...state,
        actionTime: currentTime
      };
      break;

    case RECEIVED_LOG_USER:
      const userLogged = action.payload.data.data;
      return {
        ...state,
        actionTime: currentTime,
        isAuthenticated: true,
        token: userLogged.token,
        id: userLogged.id,
        role_id: userLogged.role_id,
        email: userLogged.email,
        credit_amount: userLogged.credit_amount,
        mobile_no: userLogged.mobile_no,
        profile_image: userLogged.profile_image,
        firstname: userLogged.firstname,
        lastname: userLogged.lastname,
        isLogging: false
      };
      break;

    case ERROR_REQUEST_RESET_PASSWORD:
    case ERROR_RESET_PASSWORD:
    case ERROR_CREATE_USER:
    case ERROR_LOG_USER:
      return {
        ...state,
        actionTime: currentTime,
        isAuthenticated: false,
        isLogging: false
      };
      break;


    case RECEIVED_REQUEST_RESET_PASSWORD:
    case RECEIVED_RESET_PASSWORD:
      return {
        ...state,
        actionTime: currentTime
      };
      break;

    // not used right now:
    case REQUEST_USER_INFOS_DATA:
      return {
        ...state,
        actionTime: currentTime,
        isFetching: true
      };
      break;

    case RECEIVED_USER_INFOS_DATA:
      const userInfos = action.payload.data.data;

      return {
        ...state,
        actionTime: currentTime,
        isFetching: false,
        id: userInfos.id,
        email: userInfos.email,
        credit_amount: userInfos.credit_amount,
        firstname: userInfos.firstname,
        profile_image: userInfos.profile_image,
        mobile_no: userInfos.mobile_no,
        lastname: userInfos.lastname
      };
      break;

    case ERROR_USER_INFOS_DATA:
      return {
        ...state,
        actionTime: currentTime,
        isFetching: false
      };
      break;

    default:
      return state;
  }
}

// --------------------------------
// ACTIONS CREATORS
// --------------------------------
//

/**
 *
 * set user isAuthenticated to false and clear all app localstorage:
 *
 * @export
 * @returns {action} action
 */
export function disconnectUser() {
  auth.clearAllAppStorage();
  return {type: DISCONNECT_USER};
}

/**
 *
 * check if user is connected by looking at locally stored
 * - token
 * - user fonrmation
 *
 * @export
 * @returns {action} action
 */
export function checkUserIsConnected() {
  const token = auth.getToken();
  const user = auth.getUserInfo();
  // const checkUserHasId = obj => obj && obj._id;
  const isAuthenticated = (token) ? true : false;

  return {
    type: CHECK_IF_USER_IS_AUTHENTICATED,
    token,
    ...user,
    isAuthenticated
  };
}

/**
 *
 *  user login
 *
 * @param {string} login user login
 * @param {string} password usepasswordr
 * @returns {Promise<any>} promised action
 */
function logUser({email, password, userType}) {
  return async (dispatch) => {
    const FETCH_TYPE = appConfig.DEV_MODE ? 'FETCH_MOCK' : 'FETCH';

    // const mockResult  = userInfosMockData; // will be fetch_mock data returned (in case FETCH_TYPE = 'FETCH_MOCK', otherwise cata come from server)
    const url = `${GlobalVars.apiServer}/user/login`;
    const method = 'post';
    const headers = {};
    const options = {
      data: {
        email,
        password,
        user_type : userType
      }
    };

    // fetchMiddleware (does: fetch mock, real fetch, dispatch 3 actions... for a minimum code on action creator!)
    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: REQUEST_LOG_USER,
          success: RECEIVED_LOG_USER,
          fail: ERROR_LOG_USER
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  };
}

export function logUserIfNeeded(email: string, password: string): (...any) => Promise<any> {
  return (dispatch: (any) => any,
          getState: () => boolean): any => {
    if (shouldLogUser(getState())) {
      return dispatch(logUser(email, password));
    }
    return Promise.resolve();
  };
}

function shouldLogUser(state: any): boolean {
  const isLogging = state.userAuth.isLogging;
  if (isLogging) {
    return false;
  }
  return true;
}

/**
 * fetch user info
 *
 * NOTE: this shows a use-case of fetchMiddleware
 *@param {string} [id=''] user id
 * @returns {Promise<any>} returns fetch promise
 */
function fetchUserInfosData() {
  return dispatch => {
    const token = auth.getToken();
    const FETCH_TYPE = appConfig.DEV_MODE ? 'FETCH_MOCK' : 'FETCH';

    const mockResult = userInfosMockData; // will be fetch_mock data returned (in case FETCH_TYPE = 'FETCH_MOCK', otherwise cata come from server)
    const url = `${GlobalVars.apiServer}/user/me`;
    const method = 'get';
    const headers = {authorization: `Bearer ${token}`};
    const options = {
      credentials: 'same-origin'
    };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: REQUEST_USER_INFOS_DATA,
          success: RECEIVED_USER_INFOS_DATA,
          fail: ERROR_USER_INFOS_DATA
        },
        // mock fetch props:
        mockResult,
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  };
}

export function fetchUserInfoDataIfNeeded(id: string = '') {
  return (dispatch,
          getState) => {
    if (shouldFetchUserInfoData(getState())) {
      return dispatch(fetchUserInfosData(id));
    }
    return Promise.resolve();
  };
}

/**
 *
 * determine whether fetching should occur
 *
 * rules:
 * - should not fetch twice when already fetching
 * - ...more rules can be added
 *
 * @param {Immutable.Map} state all redux state (immutable state)
 * @returns {boolean} flag
 */
function shouldFetchUserInfoData(state): boolean {
  const userInfos = state.userAuth;
  if (userInfos.isFetching) {
    return false;
  }
  return true;
}

export function clearUserMessage() {
  return {
    type: 'CLEAR_USER_MESSAGE'
  };
}

export function createUser({userType, firstname, lastname, mobile_no, email, password}) {
  return dispatch => {
    const token = auth.getToken();
    const FETCH_TYPE = appConfig.DEV_MODE ? 'FETCH_MOCK' : 'FETCH';

    const url = `${GlobalVars.apiServer}/user/create`;
    const method = 'post';
    const headers = {};
    const options = {
      credentials: 'same-origin',
      data: {
        user_type: userType, // normal-user , agent
        firstname,
        lastname,
        mobile_no,
        email,
        password
      }
    }; // put options here (see axios options)

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        globalSuccessMessage: `Your account is now created.`,
        actionTypes: {
          request: REQUEST_CREATE_USER,
          success: RECEIVED_CREATE_USER,
          fail: ERROR_CREATE_USER
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  };
}

export function requestResetPassword({email}) {
  return dispatch => {
    const token = auth.getToken();
    const FETCH_TYPE = appConfig.DEV_MODE ? 'FETCH_MOCK' : 'FETCH';

    const url = `${GlobalVars.apiServer}/user/password/request-reset`;
    const method = 'post';
    const headers = {};
    const options = {
      credentials: 'same-origin',
      data: {
        email
      }
    }; // put options here (see axios options)

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        globalSuccessMessage: `An request reset password email has been sent to ${email}.`,
        actionTypes: {
          request: REQUEST_REQUEST_RESET_PASSWORD,
          success: RECEIVED_REQUEST_RESET_PASSWORD,
          fail: ERROR_REQUEST_RESET_PASSWORD
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  };
}

export function resetPassword({reset_token, password}) {
  return dispatch => {
    const token = auth.getToken();
    const FETCH_TYPE = appConfig.DEV_MODE ? 'FETCH_MOCK' : 'FETCH';

    const mockResult = userInfosMockData; // will be fetch_mock data returned (in case FETCH_TYPE = 'FETCH_MOCK', otherwise cata come from server)
    const url = `${GlobalVars.apiServer}/user/password/reset`;
    const method = 'post';
    const headers = {authorization: `Bearer ${token}`};
    const options = {
      credentials: 'same-origin',
      data: {
        reset_token,
        password
      }
    }; // put options here (see axios options)

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        globalSuccessMessage: 'Your account password have been reset. Please login again.',
        actionTypes: {
          request: REQUEST_RESET_PASSWORD,
          success: RECEIVED_RESET_PASSWORD,
          fail: ERROR_RESET_PASSWORD
        },
        // mock fetch props:
        mockResult,
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  };
}

export function updateUserInfo({firstname, lastname, mobile_no, email, password, uploadedProfileImage}) {
  return dispatch => {
    const token = auth.getToken();
    const FETCH_TYPE = appConfig.DEV_MODE ? 'FETCH_MOCK' : 'FETCH';
    const mockResult = userInfosMockData; // will be fetch_mock data returned (in case FETCH_TYPE = 'FETCH_MOCK', otherwise cata come from server)
    const url = `${GlobalVars.apiServer}/user/update`;
    const method = 'post';
    const headers = {
      authorization: `Bearer ${token}`
    };
    const options = {
      credentials: 'same-origin',
      data : {
        firstname,
        lastname,
        mobile_no,
        email,
        password
      }
    };

    if(uploadedProfileImage){
      const url = `${GlobalVars.apiServer}/user/upload-profile-image/`;
      const formData = new FormData();
      formData.append('uploadedProfileImage', uploadedProfileImage);
      const config = {
        headers: {
          authorization: `Bearer ${token}`,
          'content-type': 'multipart/form-data'
        }
      };
      return post(url, formData, config).then(() => {
        return dispatch({
          type: 'FETCH_MIDDLEWARE',
          fetch: {
            // common props:
            type: FETCH_TYPE,
            globalSuccessMessage: 'Profile has been updated.',
            actionTypes: {
              request: REQUEST_USER_INFOS_DATA,
              success: RECEIVED_USER_INFOS_DATA,
              fail: ERROR_USER_INFOS_DATA
            },
            // mock fetch props:
            mockResult,
            // real fetch props:
            url,
            method,
            headers,
            options
          }
        });
      }).catch((data) => {
        dispatch({type: UPDATE_IMAGE_FAIL, payload: data})
      });
    }else{
      return dispatch({
        type: 'FETCH_MIDDLEWARE',
        fetch: {
          // common props:
          type: FETCH_TYPE,
          globalSuccessMessage: 'Profile has been updated.',
          actionTypes: {
            request: REQUEST_USER_INFOS_DATA,
            success: RECEIVED_USER_INFOS_DATA,
            fail: ERROR_USER_INFOS_DATA
          },
          // mock fetch props:
          mockResult,
          // real fetch props:
          url,
          method,
          headers,
          options
        }
      });
    }

  };
}
