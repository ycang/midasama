// @flow weak

import {routerReducer} from 'react-router-redux';
import {combineReducers} from 'redux';
import views from './views';
import fakeModuleWithFetch from './fakeModuleWithFetch';
import userAuth from './userAuth';
import system from './system';
import gallery from './gallery';
import banner from './banner';
import asset from './asset';
import news from './news';
import academy from './academy';
import education from './education';
import certificate from './certificate';
import partnership from './partnership';
import product from './product';
import systemSetting from './systemSetting';

export const reducers = {
  academy,
  views,
  fakeModuleWithFetch,
  banner,
  gallery,
  asset,
  news,
  partnership,
  certificate,
  product,
  userAuth,
  education,
  system,
  systemSetting
};

export default combineReducers({
  ...reducers,
  routing: routerReducer
});
