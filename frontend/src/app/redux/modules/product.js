import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_PRODUCT_LIST_REQUEST = 'FETCH_PRODUCT_LIST_REQUEST';
const FETCH_PRODUCT_LIST_FAIL = 'FETCH_PRODUCT_LIST_FAIL';
const FETCH_PRODUCT_LIST_SUCCESS = 'FETCH_PRODUCT_LIST_SUCCESS';

const FETCH_PRODUCT_BY_ID_REQUEST = 'FETCH_PRODUCT_BY_ID_REQUEST';
const FETCH_PRODUCT_BY_ID_FAIL = 'FETCH_PRODUCT_BY_ID_FAIL';
const FETCH_PRODUCT_BY_ID_SUCCESS = 'FETCH_PRODUCT_BY_ID_SUCCESS';

const CREATE_PRODUCT_REQUEST = 'CREATE_PRODUCT_REQUEST';
const CREATE_PRODUCT_FAIL = 'CREATE_PRODUCT_FAIL';
const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';

const EDIT_PRODUCT_REQUEST = 'EDIT_PRODUCT_REQUEST';
const EDIT_PRODUCT_FAIL = 'EDIT_PRODUCT_FAIL';
const EDIT_PRODUCT_SUCCESS = 'EDIT_PRODUCT_SUCCESS';

const DELETE_PRODUCT_REQUEST = 'DELETE_PRODUCT_REQUEST';
const DELETE_PRODUCT_FAIL = 'DELETE_PRODUCT_FAIL';
const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
    list: [],
    current: {}
};

export default function (state = initialState, action) {
    const currentTime = moment().format();

    switch (action.type) {

        case DELETE_PRODUCT_FAIL:
        case EDIT_PRODUCT_FAIL:
        case EDIT_PRODUCT_REQUEST:
        case CREATE_PRODUCT_FAIL:
        case CREATE_PRODUCT_REQUEST:
        case FETCH_PRODUCT_LIST_FAIL:
        case FETCH_PRODUCT_LIST_REQUEST:
        case FETCH_PRODUCT_BY_ID_FAIL:
        case FETCH_PRODUCT_BY_ID_REQUEST:
            return {
                ...state,
                actionTime: currentTime
            };
            break;

        case DELETE_PRODUCT_SUCCESS:
        case EDIT_PRODUCT_SUCCESS:
        case CREATE_PRODUCT_SUCCESS:
        case FETCH_PRODUCT_LIST_SUCCESS:
            const list = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                list
            });
            break;

        case FETCH_PRODUCT_BY_ID_SUCCESS:
            const current = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                current
            });
            break;

        default:
            return state;
    }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchProductList(params) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        let url = `${GlobalVars.apiServer}/product`;
        const isPublished = (params && params.isPublished) ? params.isPublished : '';
        if(isPublished){
            url += '?isPublished=' + isPublished;
        }
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_PRODUCT_LIST_REQUEST,
                    success: FETCH_PRODUCT_LIST_SUCCESS,
                    fail: FETCH_PRODUCT_LIST_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function createProduct({product_name_en, product_name_cn, title_en, title_cn, content_en, content_cn, cover_image, status}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'post';
        const url = `${GlobalVars.apiServer}/product`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
              product_name_en, product_name_cn, title_en, title_cn, content_en, content_cn, cover_image, status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: CREATE_PRODUCT_REQUEST,
                    success: CREATE_PRODUCT_SUCCESS,
                    fail: CREATE_PRODUCT_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function editProduct({id, product_name_en, product_name_cn, title_en, title_cn, content_en, content_cn, cover_image, status}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'put';
        const url = `${GlobalVars.apiServer}/product/${id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
                product_name_en, product_name_cn, title_en, title_cn, content_en, content_cn, cover_image, status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: EDIT_PRODUCT_REQUEST,
                    success: EDIT_PRODUCT_SUCCESS,
                    fail: EDIT_PRODUCT_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function fetchProductById(id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        const url = `${GlobalVars.apiServer}/product/${id}`;
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_PRODUCT_BY_ID_REQUEST,
                    success: FETCH_PRODUCT_BY_ID_SUCCESS,
                    fail: FETCH_PRODUCT_BY_ID_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function deleteProduct(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'delete';
    const url = `${GlobalVars.apiServer}/product/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = { };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: DELETE_PRODUCT_REQUEST,
          success: DELETE_PRODUCT_SUCCESS,
          fail: DELETE_PRODUCT_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}
