import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_ACADEMY_LIST_REQUEST = 'FETCH_ACADEMY_LIST_REQUEST';
const FETCH_ACADEMY_LIST_FAIL = 'FETCH_ACADEMY_LIST_FAIL';
const FETCH_ACADEMY_LIST_SUCCESS = 'FETCH_ACADEMY_LIST_SUCCESS';

const FETCH_ACADEMY_BY_ID_REQUEST = 'FETCH_ACADEMY_BY_ID_REQUEST';
const FETCH_ACADEMY_BY_ID_FAIL = 'FETCH_ACADEMY_BY_ID_FAIL';
const FETCH_ACADEMY_BY_ID_SUCCESS = 'FETCH_ACADEMY_BY_ID_SUCCESS';

const CREATE_ACADEMY_REQUEST = 'CREATE_ACADEMY_REQUEST';
const CREATE_ACADEMY_FAIL = 'CREATE_ACADEMY_FAIL';
const CREATE_ACADEMY_SUCCESS = 'CREATE_ACADEMY_SUCCESS';

const EDIT_ACADEMY_REQUEST = 'EDIT_ACADEMY_REQUEST';
const EDIT_ACADEMY_FAIL = 'EDIT_ACADEMY_FAIL';
const EDIT_ACADEMY_SUCCESS = 'EDIT_ACADEMY_SUCCESS';

const DELETE_ACADEMY_REQUEST = 'DELETE_ACADEMY_REQUEST';
const DELETE_ACADEMY_FAIL = 'DELETE_ACADEMY_FAIL';
const DELETE_ACADEMY_SUCCESS = 'DELETE_ACADEMY_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
    list: [],
    current: {}
};

export default function (state = initialState, action) {
    const currentTime = moment().format();

    switch (action.type) {

        case DELETE_ACADEMY_FAIL:
        case EDIT_ACADEMY_FAIL:
        case EDIT_ACADEMY_REQUEST:
        case CREATE_ACADEMY_FAIL:
        case CREATE_ACADEMY_REQUEST:
        case FETCH_ACADEMY_LIST_FAIL:
        case FETCH_ACADEMY_LIST_REQUEST:
        case FETCH_ACADEMY_BY_ID_FAIL:
        case FETCH_ACADEMY_BY_ID_REQUEST:
            return {
                ...state,
                actionTime: currentTime
            };
            break;

        case DELETE_ACADEMY_SUCCESS:
        case EDIT_ACADEMY_SUCCESS:
        case CREATE_ACADEMY_SUCCESS:
        case FETCH_ACADEMY_LIST_SUCCESS:
            const list = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                list
            });
            break;

        case FETCH_ACADEMY_BY_ID_SUCCESS:
            const current = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                current
            });
            break;

        default:
            return state;
    }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchAcademyList(params) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        let url = `${GlobalVars.apiServer}/academy`;
        const isPublished = (params && params.isPublished) ? params.isPublished : '';
        if(isPublished){
            url += '?isPublished=' + isPublished;
        }
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_ACADEMY_LIST_REQUEST,
                    success: FETCH_ACADEMY_LIST_SUCCESS,
                    fail: FETCH_ACADEMY_LIST_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function createAcademy({username, last_name, first_name, email, contact_no, nationality}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'post';
        const url = `${GlobalVars.apiServer}/academy`;
        const headers = {};
        const options = {
            data: {
              username, last_name, first_name, email, contact_no, nationality
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: CREATE_ACADEMY_REQUEST,
                    success: CREATE_ACADEMY_SUCCESS,
                    fail: CREATE_ACADEMY_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function editAcademy({id, username, remarks, paid_date, reference, status}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'put';
        const url = `${GlobalVars.apiServer}/academy/${id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
              username, remarks, paid_date, reference, status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: EDIT_ACADEMY_REQUEST,
                    success: EDIT_ACADEMY_SUCCESS,
                    fail: EDIT_ACADEMY_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function fetchAcademyById(id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        const url = `${GlobalVars.apiServer}/academy/${id}`;
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_ACADEMY_BY_ID_REQUEST,
                    success: FETCH_ACADEMY_BY_ID_SUCCESS,
                    fail: FETCH_ACADEMY_BY_ID_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function deleteAcademy(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'delete';
    const url = `${GlobalVars.apiServer}/academy/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = { };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: DELETE_ACADEMY_REQUEST,
          success: DELETE_ACADEMY_SUCCESS,
          fail: DELETE_ACADEMY_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}
