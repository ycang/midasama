import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_BANNER_LIST_REQUEST = 'FETCH_BANNER_LIST_REQUEST';
const FETCH_BANNER_LIST_FAIL = 'FETCH_BANNER_LIST_FAIL';
const FETCH_BANNER_LIST_SUCCESS = 'FETCH_BANNER_LIST_SUCCESS';

const FETCH_BANNER_BY_ID_REQUEST = 'FETCH_BANNER_BY_ID_REQUEST';
const FETCH_BANNER_BY_ID_FAIL = 'FETCH_BANNER_BY_ID_FAIL';
const FETCH_BANNER_BY_ID_SUCCESS = 'FETCH_BANNER_BY_ID_SUCCESS';

const CREATE_BANNER_REQUEST = 'CREATE_BANNER_REQUEST';
const CREATE_BANNER_FAIL = 'CREATE_BANNER_FAIL';
const CREATE_BANNER_SUCCESS = 'CREATE_BANNER_SUCCESS';

const EDIT_BANNER_REQUEST = 'EDIT_BANNER_REQUEST';
const EDIT_BANNER_FAIL = 'EDIT_BANNER_FAIL';
const EDIT_BANNER_SUCCESS = 'EDIT_BANNER_SUCCESS';

const DELETE_BANNER_REQUEST = 'DELETE_BANNER_REQUEST';
const DELETE_BANNER_FAIL = 'DELETE_BANNER_FAIL';
const DELETE_BANNER_SUCCESS = 'DELETE_BANNER_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
  list: [],
  current: {}
};

export default function (state = initialState, action) {
  const currentTime = moment().format();

  switch (action.type) {

    case DELETE_BANNER_FAIL:
    case DELETE_BANNER_REQUEST:
    case EDIT_BANNER_FAIL:
    case EDIT_BANNER_REQUEST:
    case CREATE_BANNER_FAIL:
    case CREATE_BANNER_REQUEST:
    case FETCH_BANNER_LIST_FAIL:
    case FETCH_BANNER_LIST_REQUEST:
    case FETCH_BANNER_BY_ID_FAIL:
    case FETCH_BANNER_BY_ID_REQUEST:
      return {
        ...state,
        actionTime: currentTime
      };
      break;

    case DELETE_BANNER_SUCCESS:
    case EDIT_BANNER_SUCCESS:
    case CREATE_BANNER_SUCCESS:
    case FETCH_BANNER_LIST_SUCCESS:
      const list = action.payload.data.data;
      return Object.assign({}, state, {
        ...state,
        actionTime: currentTime,
        list
      });
      break;

    case FETCH_BANNER_BY_ID_SUCCESS:
      const current = action.payload.data.data;
      return Object.assign({}, state, {
        ...state,
        actionTime: currentTime,
        current
      });
      break;

    default:
      return state;
  }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchBannerList(params) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'get';
    let url = `${GlobalVars.apiServer}/banner`;
    const isPublished = (params && params.isPublished) ? params.isPublished : '';
    if(isPublished){
        url += '?isPublished=' + isPublished;
    }
    const headers = {};
    const options = {};

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: FETCH_BANNER_LIST_REQUEST,
          success: FETCH_BANNER_LIST_SUCCESS,
          fail: FETCH_BANNER_LIST_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function createBanner({title_en, title_cn, subtitle_en, subtitle_cn, position, banner_image_desktop, banner_image_mobile, status}) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'post';
    const url = `${GlobalVars.apiServer}/banner`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = {
      data: {
        title_en,
        title_cn,
        subtitle_en,
        subtitle_cn,
        position,
        banner_image_desktop,
        banner_image_mobile,
        status
      }
    };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: CREATE_BANNER_REQUEST,
          success: CREATE_BANNER_SUCCESS,
          fail: CREATE_BANNER_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function editBanner({id, title_en, title_cn, subtitle_en, subtitle_cn, position, banner_image_desktop, banner_image_mobile, status}) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'put';
    const url = `${GlobalVars.apiServer}/banner/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = {
      data: {
        title_en,
        title_cn,
        subtitle_en,
        subtitle_cn,
        position,
        banner_image_desktop,
        banner_image_mobile,
        status
      }
    };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: EDIT_BANNER_REQUEST,
          success: EDIT_BANNER_SUCCESS,
          fail: EDIT_BANNER_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function fetchBannerById(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'get';
    const url = `${GlobalVars.apiServer}/banner/${id}`;
    const headers = {};
    const options = {};

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: FETCH_BANNER_BY_ID_REQUEST,
          success: FETCH_BANNER_BY_ID_SUCCESS,
          fail: FETCH_BANNER_BY_ID_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}

export function deleteBanner(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'delete';
    const url = `${GlobalVars.apiServer}/banner/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = { };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: DELETE_BANNER_REQUEST,
          success: DELETE_BANNER_SUCCESS,
          fail: DELETE_BANNER_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}
