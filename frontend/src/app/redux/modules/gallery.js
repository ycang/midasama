import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_GALLERY_LIST_REQUEST = 'FETCH_GALLERY_LIST_REQUEST';
const FETCH_GALLERY_LIST_FAIL = 'FETCH_GALLERY_LIST_FAIL';
const FETCH_GALLERY_LIST_SUCCESS = 'FETCH_GALLERY_LIST_SUCCESS';

const FETCH_GALLERY_LIST_BY_NEWS_ID_REQUEST = 'FETCH_GALLERY_LIST_BY_NEWS_ID_REQUEST';
const FETCH_GALLERY_LIST_BY_NEWS_ID_FAIL = 'FETCH_GALLERY_LIST_BY_NEWS_ID_FAIL';
const FETCH_GALLERY_LIST_BY_NEWS_ID_SUCCESS = 'FETCH_GALLERY_LIST_BY_NEWS_ID_SUCCESS';

const FETCH_GALLERY_BY_ID_REQUEST = 'FETCH_GALLERY_BY_ID_REQUEST';
const FETCH_GALLERY_BY_ID_FAIL = 'FETCH_GALLERY_BY_ID_FAIL';
const FETCH_GALLERY_BY_ID_SUCCESS = 'FETCH_GALLERY_BY_ID_SUCCESS';

const CREATE_GALLERY_REQUEST = 'CREATE_GALLERY_REQUEST';
const CREATE_GALLERY_FAIL = 'CREATE_GALLERY_FAIL';
const CREATE_GALLERY_SUCCESS = 'CREATE_GALLERY_SUCCESS';

const EDIT_GALLERY_REQUEST = 'EDIT_GALLERY_REQUEST';
const EDIT_GALLERY_FAIL = 'EDIT_GALLERY_FAIL';
const EDIT_GALLERY_SUCCESS = 'EDIT_GALLERY_SUCCESS';

const DELETE_GALLERY_REQUEST = 'DELETE_GALLERY_REQUEST';
const DELETE_GALLERY_FAIL = 'DELETE_GALLERY_FAIL';
const DELETE_GALLERY_SUCCESS = 'DELETE_GALLERY_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
    list: [],
    current: {}
};

export default function (state = initialState, action) {
    const currentTime = moment().format();

    switch (action.type) {

        case DELETE_GALLERY_FAIL:
        case DELETE_GALLERY_REQUEST:
        case EDIT_GALLERY_FAIL:
        case EDIT_GALLERY_REQUEST:
        case CREATE_GALLERY_FAIL:
        case CREATE_GALLERY_REQUEST:
        case FETCH_GALLERY_LIST_FAIL:
        case FETCH_GALLERY_LIST_REQUEST:
        case FETCH_GALLERY_LIST_BY_NEWS_ID_FAIL:
        case FETCH_GALLERY_LIST_BY_NEWS_ID_REQUEST:
        case FETCH_GALLERY_BY_ID_FAIL:
        case FETCH_GALLERY_BY_ID_REQUEST:
            return {
                ...state,
                actionTime: currentTime
            };
            break;

        case DELETE_GALLERY_SUCCESS:
        case EDIT_GALLERY_SUCCESS:
        case CREATE_GALLERY_SUCCESS:
        case FETCH_GALLERY_LIST_BY_NEWS_ID_SUCCESS:
        case FETCH_GALLERY_LIST_SUCCESS:
            const list = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                list
            });
            break;

        case FETCH_GALLERY_BY_ID_SUCCESS:
            const current = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                current
            });
            break;

        default:
            return state;
    }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchGalleryList(params) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        let url = `${GlobalVars.apiServer}/gallery`;
        const isPublished = (params && params.isPublished) ? params.isPublished : '';
        if (isPublished) {
            url += '?isPublished=' + isPublished;
        }
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_GALLERY_LIST_REQUEST,
                    success: FETCH_GALLERY_LIST_SUCCESS,
                    fail: FETCH_GALLERY_LIST_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function fetchGalleryListByNewsId(id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        const url = `${GlobalVars.apiServer}/gallery/news/${id}`;
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_GALLERY_LIST_BY_NEWS_ID_REQUEST,
                    success: FETCH_GALLERY_LIST_BY_NEWS_ID_SUCCESS,
                    fail: FETCH_GALLERY_LIST_BY_NEWS_ID_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function createGallery({id, gallery_images}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'post';
        const url = `${GlobalVars.apiServer}/gallery/${id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
                gallery_images
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: CREATE_GALLERY_REQUEST,
                    success: CREATE_GALLERY_SUCCESS,
                    fail: CREATE_GALLERY_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function editGallery({id, title_en, title_cn, content_en, content_cn, position, status, gallery_image}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'put';
        const url = `${GlobalVars.apiServer}/gallery/${id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
                title_en,
                title_cn,
                gallery_image,
                content_en,
                content_cn,
                position,
                status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: EDIT_GALLERY_REQUEST,
                    success: EDIT_GALLERY_SUCCESS,
                    fail: EDIT_GALLERY_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function fetchGalleryById(id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        const url = `${GlobalVars.apiServer}/gallery/${id}`;
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_GALLERY_BY_ID_REQUEST,
                    success: FETCH_GALLERY_BY_ID_SUCCESS,
                    fail: FETCH_GALLERY_BY_ID_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function deleteGallery(id, news_id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'delete';
        const url = `${GlobalVars.apiServer}/gallery/${id}/news/${news_id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: DELETE_GALLERY_REQUEST,
                    success: DELETE_GALLERY_SUCCESS,
                    fail: DELETE_GALLERY_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}
