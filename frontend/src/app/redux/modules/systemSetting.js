import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_SYSTEM_SETTING_LIST_REQUEST = 'FETCH_SYSTEM_SETTING_LIST_REQUEST';
const FETCH_SYSTEM_SETTING_LIST_FAIL = 'FETCH_SYSTEM_SETTING_LIST_FAIL';
const FETCH_SYSTEM_SETTING_LIST_SUCCESS = 'FETCH_SYSTEM_SETTING_LIST_SUCCESS';

const EDIT_SYSTEM_SETTING_REQUEST = 'EDIT_SYSTEM_SETTING_REQUEST';
const EDIT_SYSTEM_SETTING_FAIL = 'EDIT_SYSTEM_SETTING_FAIL';
const EDIT_SYSTEM_SETTING_SUCCESS = 'EDIT_SYSTEM_SETTING_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
    list: [],
    current: {}
};

export default function (state = initialState, action) {
    const currentTime = moment().format();

    switch (action.type) {

        case EDIT_SYSTEM_SETTING_FAIL:
        case EDIT_SYSTEM_SETTING_REQUEST:
        case FETCH_SYSTEM_SETTING_LIST_FAIL:
        case FETCH_SYSTEM_SETTING_LIST_REQUEST:
            return {
                ...state,
                actionTime: currentTime
            };
            break;

        case EDIT_SYSTEM_SETTING_SUCCESS:
        case FETCH_SYSTEM_SETTING_LIST_SUCCESS:
            const list = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                list
            });
            break;

        default:
            return state;
    }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchSystemSettingList(params) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        let url = `${GlobalVars.apiServer}/systemSetting`;
        const isPublished = (params && params.isPublished) ? params.isPublished : '';
        if(isPublished){
            url += '?isPublished=' + isPublished;
        }
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_SYSTEM_SETTING_LIST_REQUEST,
                    success: FETCH_SYSTEM_SETTING_LIST_SUCCESS,
                    fail: FETCH_SYSTEM_SETTING_LIST_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function editSystemSetting({ about_us_video_url}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'put';
        const url = `${GlobalVars.apiServer}/systemSetting`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
              about_us_video_url
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: EDIT_SYSTEM_SETTING_REQUEST,
                    success: EDIT_SYSTEM_SETTING_SUCCESS,
                    fail: EDIT_SYSTEM_SETTING_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}
