import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_NEWS_LIST_REQUEST = 'FETCH_NEWS_LIST_REQUEST';
const FETCH_NEWS_LIST_FAIL = 'FETCH_NEWS_LIST_FAIL';
const FETCH_NEWS_LIST_SUCCESS = 'FETCH_NEWS_LIST_SUCCESS';

const FETCH_NEWS_BY_ID_REQUEST = 'FETCH_NEWS_BY_ID_REQUEST';
const FETCH_NEWS_BY_ID_FAIL = 'FETCH_NEWS_BY_ID_FAIL';
const FETCH_NEWS_BY_ID_SUCCESS = 'FETCH_NEWS_BY_ID_SUCCESS';

const CREATE_NEWS_REQUEST = 'CREATE_NEWS_REQUEST';
const CREATE_NEWS_FAIL = 'CREATE_NEWS_FAIL';
const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';

const EDIT_NEWS_REQUEST = 'EDIT_NEWS_REQUEST';
const EDIT_NEWS_FAIL = 'EDIT_NEWS_FAIL';
const EDIT_NEWS_SUCCESS = 'EDIT_NEWS_SUCCESS';

const DELETE_NEWS_REQUEST = 'DELETE_NEWS_REQUEST';
const DELETE_NEWS_FAIL = 'DELETE_NEWS_FAIL';
const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
    list: [],
    current: {}
};

export default function (state = initialState, action) {
    const currentTime = moment().format();

    switch (action.type) {

        case DELETE_NEWS_FAIL:
        case EDIT_NEWS_FAIL:
        case EDIT_NEWS_REQUEST:
        case CREATE_NEWS_FAIL:
        case CREATE_NEWS_REQUEST:
        case FETCH_NEWS_LIST_FAIL:
        case FETCH_NEWS_LIST_REQUEST:
        case FETCH_NEWS_BY_ID_FAIL:
        case FETCH_NEWS_BY_ID_REQUEST:
            return {
                ...state,
                actionTime: currentTime
            };
            break;

        case DELETE_NEWS_SUCCESS:
        case EDIT_NEWS_SUCCESS:
        case CREATE_NEWS_SUCCESS:
        case FETCH_NEWS_LIST_SUCCESS:
            const list = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                list
            });
            break;

        case FETCH_NEWS_BY_ID_SUCCESS:
            const current = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                current
            });
            break;

        default:
            return state;
    }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchNewsList(params) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        let url = `${GlobalVars.apiServer}/news`;
        const isPublished = (params && params.isPublished) ? params.isPublished : '';
        if(isPublished){
          url += '?isPublished=' + isPublished;
        }
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_NEWS_LIST_REQUEST,
                    success: FETCH_NEWS_LIST_SUCCESS,
                    fail: FETCH_NEWS_LIST_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function createNews({news_type, news_date, title_en, title_cn, content_en, content_cn, cover_image, video_link, status}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'post';
        const url = `${GlobalVars.apiServer}/news`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
              news_type, news_date, title_en, title_cn, content_en, content_cn, cover_image, video_link, status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: CREATE_NEWS_REQUEST,
                    success: CREATE_NEWS_SUCCESS,
                    fail: CREATE_NEWS_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function editNews({id, news_type, news_date, title_en, title_cn, content_en, content_cn, cover_image, video_link, status}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'put';
        const url = `${GlobalVars.apiServer}/news/${id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
              news_type, news_date, title_en, title_cn, content_en, content_cn, cover_image, video_link, status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: EDIT_NEWS_REQUEST,
                    success: EDIT_NEWS_SUCCESS,
                    fail: EDIT_NEWS_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function fetchNewsById(id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        const url = `${GlobalVars.apiServer}/news/${id}`;
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_NEWS_BY_ID_REQUEST,
                    success: FETCH_NEWS_BY_ID_SUCCESS,
                    fail: FETCH_NEWS_BY_ID_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function deleteNews(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'delete';
    const url = `${GlobalVars.apiServer}/news/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = { };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: DELETE_NEWS_REQUEST,
          success: DELETE_NEWS_SUCCESS,
          fail: DELETE_NEWS_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}
