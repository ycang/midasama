import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';

// /////////////////////
// constants
// /////////////////////
const ADD_GLOBAL_MESSAGE = 'ADD_GLOBAL_MESSAGE';
const REMOVE_GLOBAL_MESSAGE = 'REMOVE_GLOBAL_MESSAGE';
const SHOW_REGISTER_MODAL = 'SHOW_REGISTER_MODAL';
const SHOW_LOGIN_MODAL = 'SHOW_LOGIN_MODAL';
const SHOW_REQUEST_RESET_PASSWORD_MODAL = 'SHOW_REQUEST_RESET_PASSWORD_MODAL';
const HIDE_MODAL =  'HIDE_MODAL';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
  globalMessages: [],
  modal: {
    isOpen: false,
    showLogin: false,
    showRegister: false,
    showRequestResetPassword: false
  }
};

export default function (state = initialState, action) {
  const currentTime = moment().format();
  let globalMessages = state.globalMessages;

  switch (action.type) {

    case SHOW_REGISTER_MODAL:
      return {
        ...state,
        modal: {
          isOpen: true,
          showLogin: false,
          showRegister: true,
          showRequestResetPassword: false
        }
      };
      break;

    case SHOW_REQUEST_RESET_PASSWORD_MODAL:
      return {
        ...state,
        modal: {
          isOpen: true,
          showLogin: false,
          showRegister: false,
          showRequestResetPassword: true
        }
      };
      break;

    case SHOW_LOGIN_MODAL:
      return {
        ...state,
        modal: {
          isOpen: true,
          showLogin: true,
          showRegister: false,
          showRequestResetPassword: false
        }
      };
      break;

    case HIDE_MODAL:
      return {
        ...state,
        modal: {
          isOpen: false,
          showLogin: false,
          showRegister: false,
          showRequestResetPassword: false
        }
      };
      break;

    case REMOVE_GLOBAL_MESSAGE:
      return {
        ...state,
        globalMessages: []
      };
      break;

    case ADD_GLOBAL_MESSAGE:
      action.actionTime = currentTime;
      globalMessages.push(action.message);
      return {
        ...state,
        globalMessages
      };
      break;

    default:
      return state;
  }
}

// /////////////////////
// action creators
// /////////////////////
export function addGlobalMessage(message) {
  return {
    type: ADD_GLOBAL_MESSAGE,
    message
  };
}

export function removeGlobalMessage(message) {
  return {
    type: REMOVE_GLOBAL_MESSAGE,
    message
  };
}

export function showRequestResetPasswordModal(message) {
  return {
    type: SHOW_REQUEST_RESET_PASSWORD_MODAL
  };
}

export function hideModal(message) {
  return {
    type: HIDE_MODAL
  };
}

export function showRegisterModal(message) {
  return {
    type: SHOW_REGISTER_MODAL
  };
}

export function showLoginModal(message) {
  return {
    type: SHOW_LOGIN_MODAL
  };
}
