import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_EDUCATION_LIST_REQUEST = 'FETCH_EDUCATION_LIST_REQUEST';
const FETCH_EDUCATION_LIST_FAIL = 'FETCH_EDUCATION_LIST_FAIL';
const FETCH_EDUCATION_LIST_SUCCESS = 'FETCH_EDUCATION_LIST_SUCCESS';

const FETCH_EDUCATION_BY_ID_REQUEST = 'FETCH_EDUCATION_BY_ID_REQUEST';
const FETCH_EDUCATION_BY_ID_FAIL = 'FETCH_EDUCATION_BY_ID_FAIL';
const FETCH_EDUCATION_BY_ID_SUCCESS = 'FETCH_EDUCATION_BY_ID_SUCCESS';

const CREATE_EDUCATION_REQUEST = 'CREATE_EDUCATION_REQUEST';
const CREATE_EDUCATION_FAIL = 'CREATE_EDUCATION_FAIL';
const CREATE_EDUCATION_SUCCESS = 'CREATE_EDUCATION_SUCCESS';

const EDIT_EDUCATION_REQUEST = 'EDIT_EDUCATION_REQUEST';
const EDIT_EDUCATION_FAIL = 'EDIT_EDUCATION_FAIL';
const EDIT_EDUCATION_SUCCESS = 'EDIT_EDUCATION_SUCCESS';

const DELETE_EDUCATION_REQUEST = 'DELETE_EDUCATION_REQUEST';
const DELETE_EDUCATION_FAIL = 'DELETE_EDUCATION_FAIL';
const DELETE_EDUCATION_SUCCESS = 'DELETE_EDUCATION_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
    list: [],
    current: {}
};

export default function (state = initialState, action) {
    const currentTime = moment().format();

    switch (action.type) {

        case DELETE_EDUCATION_FAIL:
        case EDIT_EDUCATION_FAIL:
        case EDIT_EDUCATION_REQUEST:
        case CREATE_EDUCATION_FAIL:
        case CREATE_EDUCATION_REQUEST:
        case FETCH_EDUCATION_LIST_FAIL:
        case FETCH_EDUCATION_LIST_REQUEST:
        case FETCH_EDUCATION_BY_ID_FAIL:
        case FETCH_EDUCATION_BY_ID_REQUEST:
            return {
                ...state,
                actionTime: currentTime
            };
            break;

        case DELETE_EDUCATION_SUCCESS:
        case EDIT_EDUCATION_SUCCESS:
        case CREATE_EDUCATION_SUCCESS:
        case FETCH_EDUCATION_LIST_SUCCESS:
            const list = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                list
            });
            break;

        case FETCH_EDUCATION_BY_ID_SUCCESS:
            const current = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                current
            });
            break;

        default:
            return state;
    }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchEducationList(params) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        let url = `${GlobalVars.apiServer}/education`;
        const isPublished = (params && params.isPublished) ? params.isPublished : '';
        if (isPublished) {
            url += '?isPublished=' + isPublished;
        }
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_EDUCATION_LIST_REQUEST,
                    success: FETCH_EDUCATION_LIST_SUCCESS,
                    fail: FETCH_EDUCATION_LIST_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function createEducation({title_en, title_cn, video_link, status}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'post';
        const url = `${GlobalVars.apiServer}/education`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
              title_en, title_cn, video_link, status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: CREATE_EDUCATION_REQUEST,
                    success: CREATE_EDUCATION_SUCCESS,
                    fail: CREATE_EDUCATION_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function editEducation({id, title_en, title_cn, video_link, status}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'put';
        const url = `${GlobalVars.apiServer}/education/${id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
              title_en, title_cn, video_link, status
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: EDIT_EDUCATION_REQUEST,
                    success: EDIT_EDUCATION_SUCCESS,
                    fail: EDIT_EDUCATION_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function fetchEducationById(id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        const url = `${GlobalVars.apiServer}/education/${id}`;
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_EDUCATION_BY_ID_REQUEST,
                    success: FETCH_EDUCATION_BY_ID_SUCCESS,
                    fail: FETCH_EDUCATION_BY_ID_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function deleteEducation(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'delete';
    const url = `${GlobalVars.apiServer}/education/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = { };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: DELETE_EDUCATION_REQUEST,
          success: DELETE_EDUCATION_SUCCESS,
          fail: DELETE_EDUCATION_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}
