import moment from 'moment';

const dateFormat = 'DD/MM/YYYY HH:mm';
import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';

// /////////////////////
// constants
// /////////////////////
const FETCH_PARTNERSHIP_LIST_REQUEST = 'FETCH_PARTNERSHIP_LIST_REQUEST';
const FETCH_PARTNERSHIP_LIST_FAIL = 'FETCH_PARTNERSHIP_LIST_FAIL';
const FETCH_PARTNERSHIP_LIST_SUCCESS = 'FETCH_PARTNERSHIP_LIST_SUCCESS';

const FETCH_PARTNERSHIP_BY_ID_REQUEST = 'FETCH_PARTNERSHIP_BY_ID_REQUEST';
const FETCH_PARTNERSHIP_BY_ID_FAIL = 'FETCH_PARTNERSHIP_BY_ID_FAIL';
const FETCH_PARTNERSHIP_BY_ID_SUCCESS = 'FETCH_PARTNERSHIP_BY_ID_SUCCESS';

const CREATE_PARTNERSHIP_REQUEST = 'CREATE_PARTNERSHIP_REQUEST';
const CREATE_PARTNERSHIP_FAIL = 'CREATE_PARTNERSHIP_FAIL';
const CREATE_PARTNERSHIP_SUCCESS = 'CREATE_PARTNERSHIP_SUCCESS';

const EDIT_PARTNERSHIP_REQUEST = 'EDIT_PARTNERSHIP_REQUEST';
const EDIT_PARTNERSHIP_FAIL = 'EDIT_PARTNERSHIP_FAIL';
const EDIT_PARTNERSHIP_SUCCESS = 'EDIT_PARTNERSHIP_SUCCESS';

const DELETE_PARTNERSHIP_REQUEST = 'DELETE_PARTNERSHIP_REQUEST';
const DELETE_PARTNERSHIP_FAIL = 'DELETE_PARTNERSHIP_FAIL';
const DELETE_PARTNERSHIP_SUCCESS = 'DELETE_PARTNERSHIP_SUCCESS';

// /////////////////////
// reducer
// /////////////////////
const initialState = {
    list: [],
    current: {}
};

export default function (state = initialState, action) {
    const currentTime = moment().format();

    switch (action.type) {

        case DELETE_PARTNERSHIP_FAIL:
        case EDIT_PARTNERSHIP_FAIL:
        case EDIT_PARTNERSHIP_REQUEST:
        case CREATE_PARTNERSHIP_FAIL:
        case CREATE_PARTNERSHIP_REQUEST:
        case FETCH_PARTNERSHIP_LIST_FAIL:
        case FETCH_PARTNERSHIP_LIST_REQUEST:
        case FETCH_PARTNERSHIP_BY_ID_FAIL:
        case FETCH_PARTNERSHIP_BY_ID_REQUEST:
            return {
                ...state,
                actionTime: currentTime
            };
            break;

        case DELETE_PARTNERSHIP_SUCCESS:
        case EDIT_PARTNERSHIP_SUCCESS:
        case CREATE_PARTNERSHIP_SUCCESS:
        case FETCH_PARTNERSHIP_LIST_SUCCESS:
            const list = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                list
            });
            break;

        case FETCH_PARTNERSHIP_BY_ID_SUCCESS:
            const current = action.payload.data.data;
            return Object.assign({}, state, {
                ...state,
                actionTime: currentTime,
                current
            });
            break;

        default:
            return state;
    }
}


// /////////////////////
// action creators
// /////////////////////

export function fetchPartnershipList(params) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        let url = `${GlobalVars.apiServer}/partnership`;
        const isPublished = (params && params.isPublished) ? params.isPublished : '';
        if(isPublished){
            url += '?isPublished=' + isPublished;
        }
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_PARTNERSHIP_LIST_REQUEST,
                    success: FETCH_PARTNERSHIP_LIST_SUCCESS,
                    fail: FETCH_PARTNERSHIP_LIST_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function createPartnership({title_en, title_cn, content_en, content_cn, cover_image, status, website}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'post';
        const url = `${GlobalVars.apiServer}/partnership`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
                title_en, title_cn, content_en, content_cn, cover_image, status, website
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: CREATE_PARTNERSHIP_REQUEST,
                    success: CREATE_PARTNERSHIP_SUCCESS,
                    fail: CREATE_PARTNERSHIP_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function editPartnership({id, title_en, title_cn, content_en, content_cn, cover_image, status, website}) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'put';
        const url = `${GlobalVars.apiServer}/partnership/${id}`;
        const token = auth.getToken();
        const headers = {authorization: `Bearer ${token}`};
        const options = {
            data: {
                title_en, title_cn, content_en, content_cn, cover_image, status, website
            }
        };

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: EDIT_PARTNERSHIP_REQUEST,
                    success: EDIT_PARTNERSHIP_SUCCESS,
                    fail: EDIT_PARTNERSHIP_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function fetchPartnershipById(id) {
    return dispatch => {
        const FETCH_TYPE = 'FETCH';
        const method = 'get';
        const url = `${GlobalVars.apiServer}/partnership/${id}`;
        const headers = {};
        const options = {};

        return dispatch({
            type: 'FETCH_MIDDLEWARE',
            fetch: {
                // common props:
                type: FETCH_TYPE,
                actionTypes: {
                    request: FETCH_PARTNERSHIP_BY_ID_REQUEST,
                    success: FETCH_PARTNERSHIP_BY_ID_SUCCESS,
                    fail: FETCH_PARTNERSHIP_BY_ID_FAIL
                },
                // real fetch props:
                url,
                method,
                headers,
                options
            }
        });
    }
}

export function deletePartnership(id) {
  return dispatch => {
    const FETCH_TYPE = 'FETCH';
    const method = 'delete';
    const url = `${GlobalVars.apiServer}/partnership/${id}`;
    const token = auth.getToken();
    const headers = {authorization: `Bearer ${token}`};
    const options = { };

    return dispatch({
      type: 'FETCH_MIDDLEWARE',
      fetch: {
        // common props:
        type: FETCH_TYPE,
        actionTypes: {
          request: DELETE_PARTNERSHIP_REQUEST,
          success: DELETE_PARTNERSHIP_SUCCESS,
          fail: DELETE_PARTNERSHIP_FAIL
        },
        // real fetch props:
        url,
        method,
        headers,
        options
      }
    });
  }
}
