// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
import LinesEllipsis from 'react-lines-ellipsis'
import {Row, Col, Card} from 'antd';
import Slicing10 from '../../images/Slicing-10.svg';
import moment from 'moment';
import {isEnUsLocale} from "../../services/utils";

class MultiAssetCard extends PureComponent {

  static propTypes = {
    children: PropTypes.node
  };

  componentDidMount() {
  }

  render() {
    const {link, title, imageUrl, description, actions, coverImage, onClickModal, created_at, isVideo} = this.props;
    return (
      <Card className="card--multi-asset" onClick={() => onClickModal()} bordered={false}>
        <div className="card__cover-image-container"
             style={{'backgroundImage': `url('${coverImage}')`}}>
          {
            isVideo ? <div className="card__play-button"><img src={Slicing10} width="50"/></div> : ''
          }
        </div>
        <div className="card__content-container">
          <Row className="card__content-header">
            <Col xs={8} xl={6} className="card__first-col">
              <div className="card__date-wrapper">
                {
                  created_at ?
                    <p className="card__date-content">
                      <span className="card__month">{(isEnUsLocale()) ? moment(created_at).format('MMM') : moment(created_at).locale('zh-cn').format('MMM')}</span>
                      <span className="card__date">{ moment(created_at).format('DD')}</span>
                      <span className="card__year">{moment(created_at).format('YYYY')}</span>
                    </p>
                    : null
                }
              </div>
            </Col>
            <Col xs={16} xl={18}>
              <div className="card__title-wrapper">
                <h5>
                  <LinesEllipsis
                    text={title}
                    maxLine='3'
                    ellipsis='...'
                    trimRight
                    basedOn='letters'
                  />
                </h5>
              </div>
            </Col>
          </Row>
          <Row>
            <Col className="card__description-wrapper">
              <small>
                <LinesEllipsis
                  text={description}
                  maxLine='5'
                  ellipsis='...'
                  trimRight
                  basedOn='letters'
                />
              </small>
            </Col>
          </Row>
        </div>
      </Card>
    );
  }

}

export default MultiAssetCard;
