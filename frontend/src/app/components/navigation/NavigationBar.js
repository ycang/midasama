// @flow weak
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Menu, Icon, Row, Col, Button, Badge } from "antd";
import intl from "react-intl-universal";
import { withCookies } from "react-cookie";
import Drawer from "react-motion-drawer";
const SubMenu = Menu.SubMenu;
const MenuItem = Menu.Item;
import cx from "classnames";

class NavigationBar extends Component {
  static propTypes = {
    brand: PropTypes.string,
    history: PropTypes.object.isRequired,
    navModel: PropTypes.shape({
      leftLinks: PropTypes.arrayOf(
        PropTypes.shape({
          label: PropTypes.string.isRequired,
          link: PropTypes.string.isRequired
        })
      ).isRequired,
      rightLinks: PropTypes.arrayOf(
        PropTypes.shape({
          label: PropTypes.string.isRequired,
          link: PropTypes.string.isRequired
        })
      ).isRequired
    })
  };

  static defaultProps = {
    brand: "brand"
  };

  state = {
    anchorEl: null,
    right: false
  };

  redirectToUrl(pathname) {
    const { history } = this.props;

    history.push({ pathname });
    this.setState({ anchorEl: null });
  }

  handleMenuItemClick(e) {
    e.item.props.onClickCallback();
  }

  render() {
    const {
      brand,
      navModel,
      handleLoginClick,
      handleRegisterClick,
      userAuth,
      history,
      accountNavigationList
    } = this.props;

    return (
      <Row type="flex" justify="start">
        <Col md={15} lg={14}>
          <Row>
            <Col xs={0} xl={24}>
              {
                <Menu
                  onClick={this.handleMenuItemClick}
                  mode="horizontal"
                  style={{ lineHeight: "96px" }}
                  className="bg-midnight border-b-0 ant-menu-primary-left ant-menu-dark"
                >
                  <MenuItem
                    onClickCallback={() => this.redirectToUrl("/about")}
                    className={cx({
                      active: history.location.pathname == "/about"
                    })}
                  >
                    {intl.getHTML("menu.item1")}
                  </MenuItem>
                  <MenuItem
                    onClickCallback={() => this.redirectToUrl("/products")}
                    className={cx({
                      active: history.location.pathname == "/products"
                    })}
                  >
                    {intl.getHTML("menu.item2")}
                  </MenuItem>
                  <MenuItem
                    onClickCallback={() => this.redirectToUrl("/academy")}
                    className={cx({
                      active: history.location.pathname == "/academy"
                    })}
                  >
                    {intl.getHTML("menu.item7")}
                  </MenuItem>
                  <MenuItem
                    onClickCallback={() => this.redirectToUrl("/platform")}
                    className={cx({
                      active: history.location.pathname == "/platform"
                    })}
                  >
                    {intl.getHTML("menu.item3")}
                  </MenuItem>
                  <MenuItem
                    onClickCallback={() => this.redirectToUrl("/latest-news")}
                    className={cx({
                      active: history.location.pathname == "/latest-news"
                    })}
                  >
                    {intl.getHTML("menu.item4")}
                  </MenuItem>
                  <MenuItem
                    onClickCallback={() => this.redirectToUrl("/partnerships")}
                    className={cx({
                      active: history.location.pathname == "/partnerships"
                    })}
                  >
                    {intl.getHTML("menu.item5")}
                  </MenuItem>
                  <MenuItem
                    onClickCallback={() => this.redirectToUrl("/education")}
                    className={cx({
                      active: history.location.pathname == "/knowledge"
                    })}
                  >
                    {intl.getHTML("menu.item6")}
                  </MenuItem>
                </Menu>
              }
            </Col>
          </Row>
        </Col>
        <Col xs={24} md={9} lg={10}>
          <Row>
            <Col xs={24} xl={0}>
              <a
                className="ant-dropdown-link pull-right text-white"
                href="#"
                onClick={() => this.onCollapse()}
              >
                <span style={{ position: "relative", top: 2 }}>
                  {intl.getHTML("menu.label")}
                </span>
                <Icon
                  type="bars"
                  style={{ fontSize: 40, position: "relative", top: 12 }}
                />
              </a>
            </Col>
            <Col xs={0} xl={24}>
              <Menu
                mode="horizontal"
                style={{ lineHeight: "96px" }}
                className="bg-midnight pull-right border-b-0 ant-menu-dark"
              >
                <MenuItem className="p-0">
                  <span>
                    <Button
                      type="primary"
                      className="ant-btn-small"
                      href="https://member.midasama.com/Account/Login"
                    >
                      {intl.getHTML("menu.login")}
                    </Button>
                  </span>
                </MenuItem>
                <MenuItem>
                  <span>
                    <Button
                      type="third"
                      className="ant-btn-small"
                      href="https://member.midasama.com/Account/Register"
                    >
                      {intl.getHTML("menu.register")}
                    </Button>
                  </span>
                </MenuItem>
                <MenuItem className="text-white">
                  <span onClick={() => this.setLocale("en-US")}>ENG</span> /{" "}
                  <span onClick={() => this.setLocale("zh-CN")}>中文</span>
                </MenuItem>
              </Menu>
            </Col>
          </Row>
        </Col>
        <Drawer
          className="bg-midnight"
          open={
            ($("html").hasClass("tablet") || $("html").hasClass("mobile")) &&
            this.state.collapsed
          }
          onChange={this.onChangeDrawer}
        >
          <Menu
            mode="vertical"
            onClick={this.handleMenuItemClick}
            className="bg-midnight border-0 ant-menu-mobile"
            style={{ marginTop: "3rem" }}
          >
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item0")}
            </MenuItem>
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/about");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item1")}
            </MenuItem>
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/products");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item2")}
            </MenuItem>
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/academy");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item7")}
            </MenuItem>
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/platform");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item3")}
            </MenuItem>
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/latest-news");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item4")}
            </MenuItem>
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/partnerships");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item5")}
            </MenuItem>
            <MenuItem
              onClickCallback={() => {
                this.redirectToUrl("/education");
                this.setState({ collapsed: false });
              }}
            >
              {intl.getHTML("menu.item6")}
            </MenuItem>
            <MenuItem>
              <span>
                <Button
                  type="primary"
                  className="ant-btn-small"
                  href="https://member.midasama.com/Account/Login"
                >
                  {intl.getHTML("menu.login")}
                </Button>
              </span>
              <span className="m-l-10">
                <Button
                  type="secondary"
                  className="ant-btn-small"
                  href="https://member.midasama.com/Account/Register"
                >
                  {intl.getHTML("menu.register")}
                </Button>
              </span>
            </MenuItem>
            <MenuItem className="text-white">
              <span onClick={() => this.setLocale("en-US")}>ENG</span> /{" "}
              <span onClick={() => this.setLocale("zh-CN")}>中文</span>
            </MenuItem>
          </Menu>
        </Drawer>
      </Row>
    );
  }

  setLocale(locale) {
    const { cookies } = this.props;
    $("html , body").addClass(locale);
    cookies.set("locale", locale, { path: "/" });
    window.location.reload();
  }

  handleMenuItemClick(e) {
    e.item.props.onClickCallback();
  }

  onChangeDrawer = e => {
    this.setState({
      collapsed: e
    });
  };

  onCollapse = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
}

export default withCookies(NavigationBar);
