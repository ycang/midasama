// @flow weak
import {connect} from 'react-redux';
import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Redirect,
  withRouter
} from 'react-router-dom';
import auth from '../../services/auth';
import {
  bindActionCreators,
  compose
} from 'redux';
import * as systemActions from '../../redux/modules/system';

class LogoutRoute extends PureComponent {
  componentDidMount() {
    const {addGlobalMessage} = this.props;
    addGlobalMessage({message: 'Your account is now being logged out.'});
    auth.clearAllAppStorage();
  }

  render() {
    return (
      <Route {...this.props}>
        <Redirect to={{pathname: '/'}}/>
      </Route>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // views
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    addGlobalMessage: PropTypes.func.isRequired,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      ...systemActions,
    },
    dispatch
  );
};

// we use here compose (from redux) just for conveniance (since compose(f,h, g)(args) looks better than f(g(h(args))))
export default compose(
  withRouter, // IMPORTANT: witRouter is "needed here" to avoid blocking routing:
  connect(mapStateToProps, mapDispatchToProps)
)(LogoutRoute);

