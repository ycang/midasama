import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Button, notification} from 'antd';

class SimpleSnackbar extends PureComponent {

  static propTypes = {
    message: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    const key = `open${Date.now()}`;
    const btn = (
      <Button type="primary" size="small" onClick={() => notification.close(key)}>
        Close
      </Button>
    );
    notification.config({
      placement: 'bottomRight',
      bottom: 50,
      duration: 3,
    });
    notification.open({
      message: 'System Notification',
      description: this.props.message,
      btn,
      key,
      onClose: this.props.open,
    })
  }

  render() {
    return <div></div>;
  }
}

export default SimpleSnackbar;
