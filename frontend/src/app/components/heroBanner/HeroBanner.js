// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
import {Col, Row} from 'antd';

class HeroBanner extends PureComponent {

  static propTypes = {
    children: PropTypes.node
  };

  redirectToUrl(pathname) {
    const {history} = this.props;
    history.push({pathname});
    this.setState({anchorEl: null});
  };

  render() {
    const {link, title, imageUrl, description, actions} = this.props;
    return (
      <Row type="flex" justify="center" align="middle"
           style={{'backgroundImage': `url('${imageUrl}')`}} className="text-center hero-banner">
        <Col>
          <div className="p-l-25 p-r-25">
            <h1 className="text-primary hero-banner__heading" data-aos='fade-left'>{title}</h1>
            <div className="anchor-text" data-aos='fade' data-aos-delay='600'></div>
            <h3 className="text-white hero-banner_subheading" data-aos='fade-right'>{description}</h3>
          </div>
        </Col>
      </Row>
    );
  }

}

export default HeroBanner;
