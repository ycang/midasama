// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Form, Input, Divider, Button, Icon, notification} from 'antd';
const FormItem = Form.Item;

class LoginForm extends Component {
  static propTypes = {
    children: PropTypes.node,
    handleOnFormSubmit: PropTypes.func.isRequired,
  };

  state = {
    email: '',
    password: '',
    showPassword: false,
  };

  render() {
    const {message} = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form autoComplete="off" onSubmit={this.handleFormSubmit}>
        <FormItem label="Email" required>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email!' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                   placeholder="Email"
                   type="email"
                   onChange={this.handleChange('email')}/>
          )}
        </FormItem>
        <FormItem label="Password" required>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                   placeholder="Password"
                   type={this.state.showPassword ? 'text' : 'password'}
                   onChange={this.handleChange('password')}
                   onKeyPress={this.handleKeyPress} />
          )}
        </FormItem>
        <FormItem>
          {
            this.props.handleOnClickShowForgotPasswordLink ?
              <a href="javascript:;" onClick={() => this.props.handleOnClickShowForgotPasswordLink()}>
                Forgot Your Password ? Reset now.
              </a> : ''
          }
          <Button type="primary" style={{width : '100%'}} onClick={this.handleFormSubmit}>
            Submit
          </Button>
          {
            this.props.handleOnClickShowRegisterLink ?
              <Divider/> : ''
          }
          {
            this.props.handleOnClickShowRegisterLink ? <Button type="primary" className="btn-fullwidth" onClick={() => this.props.handleOnClickShowRegisterLink()}>
              Don't have an account ? Sign up now.
            </Button> : ''
          }
        </FormItem>
      </Form>
    );
  }

  handleFormSubmit = () => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(`handleFormSubmit`);
        this.props.handleOnFormSubmit({
          email: this.state.email,
          password: this.state.password,
          userType: this.props.userType || 'normal-user'
        })
      }
    });
  }

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.handleFormSubmit();
    }
  };

  handleChange = prop => event => {
    this.setState({[prop]: event.target.value});
  };

}

export default Form.create()(LoginForm);
