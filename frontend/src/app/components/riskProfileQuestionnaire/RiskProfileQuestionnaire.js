import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import anime from 'animejs';
import intl from 'react-intl-universal';
import {Button, Radio, Card, Row, Col} from 'antd';

const RadioGroup = Radio.Group;

class RiskProfileQuestionnaire extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      result: 1,
      step1Answer: 'A',
      step2Answer: 'A',
      step3Answer: 'A',
      step4Answer: 'A',
      step5Answer: 'A',
      step6Answer: 'A',
      profile1score: 0,
      profile2score: 0,
      profile3score: 0,
      profile4score: 0,
      profile5score: 0,
      profile6score: 0,
      finalResultPoints: {
        1: '70 50.24192838101293 90.14800421288159 65.94300421288159 80.83095365830248 95.02093680677612 55.114957871184075 94.38907161898707 45.667928381012928 60.924974722710445',
        2: '70 41.24192838101293 129.14800421288159 56.94300421288159 90.83095365830248 100.02093680677612 55.114957871184075 100.38907161898707 40.667928381012928 56.924974722710445',
        3: '70 41.24192838101293 95.14800421288159 56.94300421288159 110.83095365830248 130.02093680677612 55.114957871184075 100.38907161898707 40.667928381012928 56.924974722710445',
        4: '70 41.24192838101293 95.14800421288159 56.94300421288159 90.83095365830248 100.02093680677612 55.114957871184075 94.38907161898707 10.667928381012928 56.924974722710445',
        5: '70 41.24192838101293 95.14800421288159 56.94300421288159 90.83095365830248 100.02093680677612 35.114957871184075 130.38907161898707 40.667928381012928 56.924974722710445',
        6: '70 11.24192838101293 95.14800421288159 56.94300421288159 90.83095365830248 100.02093680677612 55.114957871184075 100.38907161898707 40.667928381012928 56.924974722710445',
      }
    }
  }

  componentWillMount() {
    if (this.props.modalStatus) {
      this.resetForm();
    }
  }

  componentWillReceiveProps() {
    // when close , reset data
    if (this.props.modalStatus == false) {
      this.setState({
        step: 1,
        result: 1,
        step1Answer: 'A',
        step2Answer: 'A',
        step3Answer: 'A',
        step4Answer: 'A',
        step5Answer: 'A',
        step6Answer: 'A',
        profile1score: 0,
        profile2score: 0,
        profile3score: 0,
        profile4score: 0,
        profile5score: 0,
        profile6score: 0
      })
    }
  }

  render() {
    const radioStyle = {
      display: 'block',
    };
    return <div>
      <Row type="flex" justify="center" align="middle"
           className={cx('risk-profile-container', {'hide': this.state.step == 7})}>
        <Col xs={22} sm={20} md={20} lg={12}>
          <div className="text-center m-b-20">
            <h2 className="text-primary m-b-5">{intl.getHTML('riskprofile.question.heading')}</h2>
            <div className="anchor-text"></div>
            <br/>
            <br/>
            <br/>
          </div>
          <Card className="m-b-60">
            <div className="risk-profile__step-counter"> <span className="text-primary">0{this.state.step}</span> / 06</div>
            <div className={cx('risk-profile__step', {'active': this.state.step === 1})}>
              <h4>{intl.getHTML('riskprofile.question1')}</h4>
              <br/>
              <br/>
              <RadioGroup onChange={(e) => this.onChange('step1Answer', e)}
                          value={this.state.step1Answer}>
                <Radio style={radioStyle}
                       value={'A'}>{intl.getHTML('riskprofile.question1.option1')}</Radio>
                <Radio style={radioStyle}
                       value={'B'}>{intl.getHTML('riskprofile.question1.option2')}</Radio>
                <Radio style={radioStyle}
                       value={'C'}>{intl.getHTML('riskprofile.question1.option3')}</Radio>
              </RadioGroup>
            </div>
            <div className={cx('risk-profile__step', {'active': this.state.step === 2})}>
              <h4>{intl.getHTML('riskprofile.question2')}</h4>
              <RadioGroup onChange={(e) => this.onChange('step2Answer', e)}
                          value={this.state.step2Answer}>
                <Radio style={radioStyle}
                       value={'A'}>{intl.getHTML('riskprofile.question2.option1')}</Radio>
                <Radio style={radioStyle}
                       value={'B'}>{intl.getHTML('riskprofile.question2.option2')}</Radio>
                <Radio style={radioStyle}
                       value={'C'}>{intl.getHTML('riskprofile.question2.option3')}</Radio>
              </RadioGroup>
            </div>
            <div className={cx('risk-profile__step', {'active': this.state.step === 3})}>
              <h4>{intl.getHTML('riskprofile.question3')}</h4>
              <RadioGroup onChange={(e) => this.onChange('step3Answer', e)}
                          value={this.state.step3Answer}>
                <Radio style={radioStyle}
                       value={'A'}>{intl.getHTML('riskprofile.question3.option1')}</Radio>
                <Radio style={radioStyle}
                       value={'B'}>{intl.getHTML('riskprofile.question3.option2')}</Radio>
                <Radio style={radioStyle}
                       value={'C'}>{intl.getHTML('riskprofile.question3.option3')}</Radio>
                <Radio style={radioStyle}
                       value={'D'}>{intl.getHTML('riskprofile.question3.option4')}</Radio>
                <Radio style={radioStyle}
                       value={'E'}>{intl.getHTML('riskprofile.question3.option5')}</Radio>
              </RadioGroup>
            </div>
            <div className={cx('risk-profile__step', {'active': this.state.step === 4})}>
              <h4>{intl.getHTML('riskprofile.question4')}</h4>
              <RadioGroup onChange={(e) => this.onChange('step4Answer', e)}
                          value={this.state.step4Answer}>
                <Radio style={radioStyle}
                       value={'A'}>{intl.getHTML('riskprofile.question4.option1')}</Radio>
                <Radio style={radioStyle}
                       value={'B'}>{intl.getHTML('riskprofile.question4.option2')}</Radio>
                <Radio style={radioStyle}
                       value={'C'}>{intl.getHTML('riskprofile.question4.option3')}</Radio>
                <Radio style={radioStyle}
                       value={'D'}>{intl.getHTML('riskprofile.question4.option4')}</Radio>
                <Radio style={radioStyle}
                       value={'E'}>{intl.getHTML('riskprofile.question4.option5')}</Radio>
              </RadioGroup>
            </div>
            <div className={cx('risk-profile__step', {'active': this.state.step === 5})}>
              <h4>{intl.getHTML('riskprofile.question5')}</h4>
              <RadioGroup onChange={(e) => this.onChange('step5Answer', e)}
                          value={this.state.step5Answer}>
                <Radio style={radioStyle}
                       value={'A'}>{intl.getHTML('riskprofile.question5.option1')}</Radio>
                <Radio style={radioStyle}
                       value={'B'}>{intl.getHTML('riskprofile.question5.option2')}</Radio>
                <Radio style={radioStyle}
                       value={'C'}>{intl.getHTML('riskprofile.question5.option3')}</Radio>
                <Radio style={radioStyle}
                       value={'D'}>{intl.getHTML('riskprofile.question5.option4')}</Radio>
                <Radio style={radioStyle}
                       value={'E'}>{intl.getHTML('riskprofile.question5.option5')}</Radio>
              </RadioGroup>
            </div>
            <div className={cx('risk-profile__step', {'active': this.state.step === 6})}>
              <h4>{intl.getHTML('riskprofile.question6')}</h4>
              <RadioGroup onChange={(e) => this.onChange('step6Answer', e)}
                          value={this.state.step6Answer}>
                <Radio style={radioStyle}
                       value={'A'}>{intl.getHTML('riskprofile.question6.option1')}</Radio>
                <Radio style={radioStyle}
                       value={'B'}>{intl.getHTML('riskprofile.question6.option2')}</Radio>
                <Radio style={radioStyle}
                       value={'C'}>{intl.getHTML('riskprofile.question6.option3')}</Radio>
                <Radio style={radioStyle}
                       value={'D'}>{intl.getHTML('riskprofile.question6.option4')}</Radio>
                <Radio style={radioStyle}
                       value={'E'}>{intl.getHTML('riskprofile.question6.option5')}</Radio>
              </RadioGroup>
            </div>
            <div className="text-center risk-profile__next-button">
              <Row>
                <Col xs={this.state.step === 1 ? 0 :24} sm={this.state.step === 1 ? 0 :12} className="m-b-10">
                  <Button type="secondary"
                          onClick={() => this.prevStep()}>{intl.getHTML(`riskprofile.button.previous`)}</Button>
                </Col>
                <Col xs={24} sm={this.state.step === 1 ? 24 :12}>
                  <Button type="secondary"
                          onClick={() => this.nextStep()}>{intl.getHTML(`riskprofile.button.next`)}</Button>
                </Col>
              </Row>
            </div>
          </Card>
        </Col>
      </Row>
      <Row type="flex" justify="center" align="middle">
        <Col sm={18}>
          <div className={cx('risk-profile__step', {'active': this.state.step === 7})}>
            <Row className="risk-profile__result-heading-wrapper">
              <Col>
                <div className="m-b-20">
                  <h2 className="text-center text-primary m-b-5">{intl.getHTML('riskprofile.result.heading')}</h2>
                  <div className="anchor-text"></div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col sm={12}>
                <div id="morphing"
                     style={{position: 'relative', width: 140, margin: '0 auto'}} className="risk-profile__graph-container">
                  <div className="text-white risk-profile__graph-heading risk-profile__graph-heading-1">{intl.getHTML(`riskprofile.answer1.heading`)}</div>
                  <div className="text-white risk-profile__graph-heading risk-profile__graph-heading-2 text-center">{intl.getHTML(`riskprofile.answer2.heading`)}</div>
                  <div className="text-white risk-profile__graph-heading risk-profile__graph-heading-3">{intl.getHTML(`riskprofile.answer3.heading`)}</div>
                  <div className="text-white risk-profile__graph-heading risk-profile__graph-heading-4">{intl.getHTML(`riskprofile.answer4.heading`)}</div>
                  <div className="text-white risk-profile__graph-heading risk-profile__graph-heading-5">{intl.getHTML(`riskprofile.answer5.heading`)}</div>
                  <div className="text-white risk-profile__graph-heading risk-profile__graph-heading-6">{intl.getHTML(`riskprofile.answer6.heading`)}</div>
                  <svg width="140" height="140" viewBox="0 0 140 140" className="risk-profile__graph">
                    <g fill="#006cff" fillRule="evenodd">
                      <g fill="blue" fillOpacity=".30" transform="translate(0 6)">
                        <polygon
                          points="70 0 136.574 48.369 111.145 126.631 28.855 126.631 3.426 48.369"></polygon>
                        <polygon
                          points="70 18 119.455 53.931 100.565 112.069 39.435 112.069 20.545 53.931"></polygon>
                        <polygon
                          points="70 34.86 101.727 57.911 89.609 95.209 50.391 95.209 38.273 57.911"></polygon>
                        <polygon
                          points="70 50.898 84.864 61.697 79.186 79.171 60.814 79.171 55.136 61.697"></polygon>
                      </g>
                      <polygon className="polymorph" strokeWidth="1" stroke="006cff"
                               points="70 31.24192838101293 119.14800421288159 59.94300421288159 104.83095365830248 124.02093680677612 55.114957871184075 94.38907161898707 10.667928381012928 56.924974722710445"></polygon>
                    </g>
                  </svg>
                </div>
              </Col>
              <Col sm={12} className="risk-profile__result-wraper">
                {
                  [1, 2, 3, 4, 5, 6].map((key, i) => {
                    return (
                      <div
                        className={cx('risk-profile__result', {'active': this.state.result === key})}
                        key={i}>
                        <p
                          className="text-primary">{intl.getHTML(`riskprofile.answer${key}.heading`)}</p>
                        <ul className="custom-list-style">
                          <li
                            className="m-b-20 text-white">{intl.getHTML(`riskprofile.answer${key}.point1`)}</li>
                          <li
                            className="m-b-20 text-white">{intl.getHTML(`riskprofile.answer${key}.point2`)}</li>
                          <li className="text-white">{intl.getHTML(`riskprofile.answer${key}.point3`)}</li>
                        </ul>
                      </div>
                    )
                  })
                }
              </Col>
            </Row>

          </div>
        </Col>
      </Row>
    </div>;
  }

  onChange(key, e) {
    this.scoreCalculation(key, e.target.value);
    this.setState({[key]: e.target.value});
  }

  scoreCalculation(key, value) {
    if (key == 'step1Answer') {
      if (value == 'A') {
        this.setState({profile1score: this.state.profile1score + 1});
      }
      if (value == 'B') {
        this.setState({
          profile2score: this.state.profile2score + 1,
          profile3score: this.state.profile3score + 1,
          profile4score: this.state.profile4score + 1
        });
      }
      if (value == 'C') {
        this.setState({
          profile5score: this.state.profile5score + 1,
          profile6score: this.state.profile6score + 1
        });
      }
    }
    if (key == 'step2Answer') {
      if (value == 'A') {
        this.setState({profile2score: this.state.profile2score + 1});
      }
      if (value == 'B') {
        this.setState({
          profile3score: this.state.profile3score + 1,
          profile4score: this.state.profile4score + 1
        });
      }
      if (value == 'C') {
        this.setState({
          profile5score: this.state.profile5score + 1,
          profile6score: this.state.profile6score + 1
        });
      }
    }
    if (key == 'step3Answer' || key == 'step4Answer' || key == 'step5Answer') {
      if (value == 'A') {
        this.setState({profile2score: this.state.profile2score + 1});
      }
      if (value == 'B') {
        this.setState({profile3score: this.state.profile3score + 1});
      }
      if (value == 'C') {
        this.setState({profile4score: this.state.profile4score + 1});
      }
      if (value == 'D') {
        this.setState({profile5score: this.state.profile5score + 1});
      }
      if (value == 'E') {
        this.setState({profile6score: this.state.profile6score + 1});
      }
    }
  }

  prevStep() {
    const prevStep = this.state.step - 1;
    this.setState({step: prevStep});
  }

  nextStep() {
    const nextStep = this.state.step + 1;
    if (nextStep == 7) {
      let finalResult = 0;
      if (this.state.profile1score == 1) {
        finalResult = 1;
      } else if ((this.state.profile2score >= this.state.profile3score)
        && (this.state.profile2score >= this.state.profile4score)
        && (this.state.profile2score >= this.state.profile5score)
        && (this.state.profile2score >= this.state.profile6score)
      ) {
        finalResult = 2
      } else if ((this.state.profile3score > this.state.profile2score)
        && (this.state.profile3score >= this.state.profile4score)
        && (this.state.profile3score >= this.state.profile5score)
        && (this.state.profile3score >= this.state.profile6score)
      ) {
        finalResult = 3
      } else if ((this.state.profile4score > this.state.profile2score)
        && (this.state.profile4score > this.state.profile3score)
        && (this.state.profile4score > this.state.profile5score)
        && (this.state.profile4score >= this.state.profile6score)
      ) {
        finalResult = 4
      } else if ((this.state.profile5score > this.state.profile2score)
        && (this.state.profile5score > this.state.profile3score)
        && (this.state.profile5score > this.state.profile4score)
        && (this.state.profile5score >= this.state.profile6score)
      ) {
        finalResult = 5
      } else if ((this.state.profile6score > this.state.profile2score)
        && (this.state.profile6score > this.state.profile3score)
        && (this.state.profile6score > this.state.profile4score)
        && (this.state.profile6score > this.state.profile5score)
      ) {
        finalResult = 6
      }

      //console.log([this.state.profile1score,this.state.profile2score,this.state.profile3score,this.state.profile4score,this.state.profile5score,this.state.profile6score]);
      this.setState({result: finalResult});
      var morphing = anime({
        targets: '#morphing .polymorph',
        points: [
          {value: '70 41 118.574 59.369 111.145 132.631 60.855 84.631 20.426 60.369'},
          {value: '70 6 119.574 60.369 100.145 117.631 39.855 117.631 55.426 68.369'},
          {value: '70 57 136.574 54.369 89.145 100.631 28.855 132.631 38.426 64.369'},
          {value: this.state.finalResultPoints[finalResult]}
        ],
        easing: 'easeOutQuad',
        duration: 2000
      });
    }

    this.setState({step: nextStep});
  }

  resetForm() {
    this.setState({
      step: 1,
      result: 1,
      step1Answer: 'A',
      step2Answer: 'A',
      step3Answer: 'A',
      step4Answer: 'A',
      step5Answer: 'A',
      step6Answer: 'A',
      profile1score: 0,
      profile2score: 0,
      profile3score: 0,
      profile4score: 0,
      profile5score: 0,
      profile6score: 0,
    });
  }
}

export default RiskProfileQuestionnaire;
