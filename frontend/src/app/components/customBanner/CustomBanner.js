// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
import anime from 'animejs'
import homepageBanner02 from '../../images/homepage-banner-02.png';
import homepageBanner03 from '../../images/homepage-banner-03.png';
import homepageBanner04 from '../../images/homepage-banner-04.png';
import homepageBanner05 from '../../images/homepage-banner-05.png';
import homepageBanner06 from '../../images/homepage-banner-06.png';
class CustomBanner extends PureComponent {

  static propTypes = {
    children: PropTypes.node
  };

  componentDidMount() {

    var timelineParameters3 = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters3
      .add({
        targets: '.js-custom-banner-3',
        translateX: [{value: 80}, {value: 160}],
        translateY: [{value: 30}, {value: -30}],
        duration: 4000
      });

    var timelineParameters4 = anime.timeline({
      easing: 'easeInOutQuad'
    });
    timelineParameters4
      .add({
        targets: '.js-custom-banner-4',
        translateX: [{value: 80}, {value: 160}, {value: 120}, {value: 0}],
        translateY: [{value: 30}, {value: -30}, {value: -60}, {value: 0}],
        duration: 10000
      });

    var timelineParameters5 = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });

    timelineParameters5
      .add({
        targets: '.js-custom-banner-5',
        translateX: [{value: 80}, {value: 640}, {value: 320}],
        translateY: [{value: 30}, {value: -300}, {value: -600}],
        duration: 15000
      });

    // 6
    var timelineParameters6 = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad'
    });
    timelineParameters6
      .add({
        targets: '.js-custom-banner-6',
        translateX: [{value: 80}, {value: 160}],
        translateY: [{value: 30}, {value: -30}],
        duration: 2000
      });

    // 2
    var timelineParameters2 = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters2
      .add({
        targets: '.js-custom-banner-2',
        translateX: [{value: 0}, {value: 0}],
        translateY: [{value: 30}, {value: -30}],
        duration: 2000
      });
  }
  render() {
    const {link, title, imageUrl, description, actions} = this.props;
    return (
      <div>
        <div className="js-custom-banner-3 custom-banner__box--3">
          <img src={homepageBanner05}/>
        </div>
        <div className="js-custom-banner-4 custom-banner__box--4">
          <img src={homepageBanner04}/>
        </div>
        <div className="js-custom-banner-5 custom-banner__box--5">
          <img src={homepageBanner03}/>
        </div>
        <div className="js-custom-banner-6 custom-banner__box--6">
          <img src={homepageBanner06}/>
        </div>
        <div className="js-custom-banner-2 custom-banner__logo">
          <img src={homepageBanner02}/>
        </div>
      </div>
    );
  }

}

export default CustomBanner;
