// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Menu, Icon} from 'antd';
const MenuItem = Menu.Item;

class LeftNavigation extends Component {

  static propTypes = {
    children: PropTypes.node
  };

  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      routes: [
        {icon: 'picture', label: 'Banner', path: '/admin/banner'},
        {icon: 'notification', label: 'News', path: '/admin/news'},
        {icon: 'table', label: 'Product', path: '/admin/product'},
        {icon: 'global', label: 'Partnership', path: '/admin/partnership'},
        {icon: 'video-camera', label: 'Education', path: '/admin/education'},
        {icon: 'solution', label: 'Academy', path: '/admin/academy'},
        {icon: 'setting', label: 'System Setting', path: '/admin/systemSetting'},
        {icon: 'logout', label: 'Logout', path: '/logout'}
      ],
      currentBreadcrumbLabel: '',
      defaultSelectedKeys: ['1']
    }
  }

  componentDidMount() {
    this.state.routes.map((route, i) => {
      if (this.props.match.path.indexOf(route.path) > -1) {
        this.setState({selectedKeys: [i.toString()]});
      }
    });
  }

  redirectToUrl(pathname) {
    const {history} = this.props;
    history.push({pathname});
  }

  handleMenuItemClick(e) {
    e.item.props.onClickCallback();
  }

  onClickMenuItem({label, path}, i) {
    this.props.handleChangeBreadcrumb(label);
    this.setState({selectedKeys: [i.toString()]});
    this.redirectToUrl(path);
    const {onClickCollapse} = this.props;
    if(onClickCollapse){
      onClickCollapse();
    }
  }

  render() {
    const {} = this.props;
    return (
      <Menu onClick={this.handleMenuItemClick} theme="dark"
            defaultSelectedKeys={this.state.defaultSelectedKeys}
            selectedKeys={this.state.selectedKeys}
            mode="inline"
            style={{height: '100%'}}>
        {
          this.state.routes.map((route, i) => {
            return (<MenuItem key={i} onClickCallback={() => this.onClickMenuItem(route, i)}>
              <Icon type={route.icon}/>
              <span>{route.label}</span>
            </MenuItem>)
          })
        }
      </Menu>
    );
  }

}

export default LeftNavigation;
