// @flow weak

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import aIcon1Front from '../../images/Slicing-46.png';
import aIcon1Back from '../../images/Slicing-46-1.png';
import aIcon2Front from '../../images/Slicing-47.png';
import aIcon2Back from '../../images/Slicing-47-1.png';
import aIcon3Front from '../../images/Slicing-48.png';
import aIcon3Back from '../../images/Slicing-48-1.png';
import aIcon4Front from '../../images/Slicing-49.png';
import aIcon4Back from '../../images/Slicing-49-1.png';
import aIcon5Front from '../../images/Slicing-50.png';
import aIcon5Back from '../../images/Slicing-50-1.png';
import anime from 'animejs';

class AnimatedIcon extends Component {
  static propTypes = {
    children: PropTypes.node,
    animated: PropTypes.bool
  };

  static defaultProps = {
    animated: true
  };

  componentDidMount() {
    this.animation1();
    this.animation2();
    this.animation3();
    this.animation4();
    this.animation5();
  }

  animation1() {
    var timelineParameters = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters
      .add({
        targets: '.js-animated-icon-1',
        translateX: [{value: -20}, {value: 40}],
        translateY: [{value: 0}, {value: 0}],
        duration: 3000,
        delay: 500
      });
  }

  animation2() {
    var timelineParameters = anime.timeline({
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters
      .add({
        targets: '.js-animated-icon-2',
        translateX: [{value: -20}, {value: -60}, {value: 0}],
        translateY: [{value: -10}, {value: 20}, {value: 0}],
        duration: 4000,
        delay: 1000
      });
  }

  animation3() {
    var timelineParameters = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters
      .add({
        targets: '.js-animated-icon-3',
        translateX: [{value: 0}, {value: 0}],
        translateY: [{value: -10}, {value: 10}],
        elasticity: 200,
        duration: 1000
      });
  }

  animation4() {
    var timelineParameters = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters
      .add({
        targets: '.js-animated-icon-4',
        translateX: [{value: 0}, {value: 0}],
        translateY: [{value: -10}, {value: 10}],
        duration: 3000
      });
  }

  animation5() {
    var timelineParameters = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters
      .add({
        targets: '.js-animated-icon-5',
        translateX: [{value: 0}, {value: 0}],
        translateY: [{value: -10}, {value: 10}],
        duration: 2000
      });
  }

  render() {
    const {
      type
    } = this.props;

    return (
      <div className="a-icon-container">
        {
          type == 'secure' ? <div className="a-icon-w">
            <img src={aIcon1Back} className="a-icon-1-b js-animated-icon-1"/>
            <img src={aIcon1Front} className="a-icon-1-f"/>
          </div> : ''
        }
        {
          type == 'transparency' ? <div className="a-icon-w">
            <img src={aIcon2Back} className="a-icon-2-b js-animated-icon-2"/>
            <img src={aIcon2Front} className="a-icon-2-f"/>
          </div> : ''
        }
        {
          type == 'professional' ? <div className="a-icon-w">
            <img src={aIcon3Back} className="a-icon-3-b js-animated-icon-3"/>
            <img src={aIcon3Front} className="a-icon-3-f"/>
          </div> : ''
        }
        {
          type == 'easy' ? <div className="a-icon-w">
            <img src={aIcon4Back} className="a-icon-4-b js-animated-icon-4"/>
            <img src={aIcon4Front} className="a-icon-4-f"/>
          </div> : ''
        }
        {
          type == 'money-up' ? <div className="a-icon-w">
            <img src={aIcon5Back} className="a-icon-5-b js-animated-icon-5"/>
            <img src={aIcon5Front} className="a-icon-5-f"/>
          </div> : ''
        }
      </div>
    );
  }
}

export default AnimatedIcon;
