// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Form, Input, Row, Col, Divider, Button} from 'antd';

const FormItem = Form.Item;

class RegisterForm extends Component {

  static propTypes = {
    children: PropTypes.node
  };

  state = {
    email: '',
    lastname: '',
    firstname: '',
    password: '',
    mobile_no: '',
    showPassword: false,
  };

  render() {
    const {children, message} = this.props;
    return (
      <div>
        <Row>
          <Col>
            <p>Tailor your experience with Malaysia's greatest property resource. Create your free account today! </p>
          </Col>
        </Row>
        <Form autoComplete="off">
          {message ? <p color="error">{message}</p> : ''}

          <Row gutter={16}>
            <Col span={12}>
              <FormItem label="First Name" required>
                <Input
                  value={this.state.firstname}
                  onChange={this.handleChange('firstname')}
                  autoFocus
                />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="Last Name" required>
                <Input
                  value={this.state.lastname}
                  onChange={this.handleChange('lastname')}
                />
              </FormItem>
            </Col>
          </Row>
          <FormItem label="Mobile No" required>
            <Input
              value={this.state.mobile_no}
              onChange={this.handleChange('mobile_no')}
            />
          </FormItem>
          <Row gutter={16}>
            <Col span={12}>
              <FormItem label="Email" required>
                <Input
                  type="email"
                  value={this.state.email}
                  onChange={this.handleChange('email')}
                />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="Password" required>
                <Input
                  type={this.state.showPassword ? 'text' : 'password'}
                  value={this.state.password}
                  onChange={this.handleChange('password')}
                />
              </FormItem>
            </Col>
          </Row>
          <FormItem>
            <small>By signing up, I agree to the Terms of Use of Propertynetworks.my Malaysia.</small>
            <br/>
            <Button type="primary" className="btn-fullwidth" onClick={() => this.props.handleOnFormSubmit({
              userType: this.props.userType || 'normal-user',
              firstname: this.state.firstname,
              lastname: this.state.lastname,
              mobile_no: this.state.mobile_no,
              email: this.state.email,
              password: this.state.password
            })}>
              Submit
            </Button>

            <br/>
            <Divider/>

            {
              this.props.handleOnClickShowLoginLink ? <Button type="primary" className="btn-fullwidth" onClick={() => this.props.handleOnClickShowLoginLink()}>
                Already have an account ? Login now.
              </Button> : ''
            }

          </FormItem>
        </Form>
      </div>
    );
  }

  handleChange = prop => event => {
    this.setState({[prop]: event.target.value});
  };

}

export default RegisterForm;
