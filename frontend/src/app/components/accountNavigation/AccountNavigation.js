// @flow weak
import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import {Menu, Icon} from 'antd';
const MenuItem = Menu.Item;

class AccountNavigation extends Component {

  static propTypes = {
    history: PropTypes.object.isRequired,
    listItem: PropTypes.array.isRequired
  };

  static defaultProps = {
    brand: 'brand'
  };

  state = {
    right: false
  };

  redirectToUrl(pathname) {
    const {
      history
    } = this.props;

    history.push({pathname});
    this.setState({anchorEl: null});
  };

  render() {
    const {
      history,
      listItem
    } = this.props;
    const currentPath = history.location.pathname;
    return (
      <div style={{marginRight: 48}}>
        <Menu
          mode="inline"
          onClick={this.handleMenuItemClick}
          style={{borderRight: 0}}
        >
          {
            listItem ? listItem.map((item, i) => {
              return (
                <MenuItem key={i} urlPath={item.path}
                           className={classnames({'active': currentPath === item.path})}>
                  <Icon type={item.icon}/>
                  <span>{item.label}</span>
                </MenuItem>)
            }) : ''
          }
          <MenuItem urlPath='/logout'>
            <Icon type="logout"/>
            <span>Logout</span>
          </MenuItem>
        </Menu>
      </div>
    );
  }

  handleMenuItemClick = (e) => {
    this.redirectToUrl(e.item.props.urlPath);
  }
}

export default AccountNavigation;
