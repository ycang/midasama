// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import navigationModel from '../../config/navigation.json';
import MainRoutes from '../../routes/MainRoutes';
import intl from 'react-intl-universal';
import { withCookies, Cookies } from 'react-cookie';
import AOS from 'aos';
import 'aos/dist/aos.css';
// locale data
const locales = {
  "en-US": require('../../locales/en-US.js'),
  "zh-CN": require('../../locales/zh-CN.js'),
};

class App extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    removeGlobalMessage: PropTypes.func.isRequired,

    // views:
    currentView: PropTypes.string

  };

  state = {
    navModel: navigationModel,
    initDone: false
  };

  componentDidMount() {
    const {checkUserIsConnected} = this.props;
    checkUserIsConnected();
    const { cookies } = this.props;
    this.state = {
      lang: cookies.get('locale') || 'zh-CN'
    };
    this.loadLocales();
    AOS.init({
      duration : 1000
    })
  }

  loadLocales() {
    // init method will load CLDR locale data according to currentLocale
    // react-intl-universal is singleton, so you should init it only once in your app
    intl.init({
      currentLocale: this.state.lang, // TODO: determine locale here
      locales,
    }).then(() => {
      // After loading CLDR locale data, start to render
      this.setState({initDone: true});
    });
  }

  redirectToUrl(pathname) {
    const {
      history
    } = this.props;

    history.push({pathname});
  }

  render() {
    return (
      <MainRoutes/>
    );
  }

}

export default withCookies(App);
