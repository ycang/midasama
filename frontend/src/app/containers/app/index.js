// @flow weak

import {connect} from 'react-redux';
import {
  bindActionCreators,
  compose
} from 'redux';
import App from './App';
import {withRouter} from 'react-router';
import * as viewsActions from '../../redux/modules/reducers';
import * as userAuthActions from '../../redux/modules/userAuth';
import * as systemActions from '../../redux/modules/system';
import * as newsActions from '../../redux/modules/news';

const mapStateToProps = (state) => {
  return {
    // views
    currentView: state.views.currentView,

    // useAuth:
    isAuthenticated: state.userAuth.isAuthenticated,
    isFetching: state.userAuth.isFetching,
    isLogging: state.userAuth.isLogging,
    globalMessages: state.system.globalMessages,
    userAuth: state.userAuth,
    news: state.news,
    system: state.system,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...systemActions,
      ...newsActions,
    },
    dispatch
  );
};

// we use here compose (from redux) just for conveniance (since compose(f,h, g)(args) looks better than f(g(h(args))))
export default compose(
  withRouter, // IMPORTANT: witRouter is "needed here" to avoid blocking routing:
  connect(mapStateToProps, mapDispatchToProps)
)(App);
