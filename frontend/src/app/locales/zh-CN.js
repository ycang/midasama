module.exports = {
  'menu.label': '菜单',
  'menu.item0': '首页',
  'menu.item1': '关于米得',
  'menu.item2': '产品类型',
  'menu.item3': '平台',
  'menu.item4': '最新活动资讯',
  'menu.item5': '合作伙伴',
  'menu.item6': '外汇知识',
  'menu.item7': '集英商学院',
  'menu.login': '登入',
  'menu.register': '注册',

  'home.banner.heading': '米得平台',
  'home.banner.subHeading':
    '作为一个国际金融平台<br/>为您打造一个永续发展的财富管道<br/>创造源源不绝的不在职收入',
  'home.banner.cta': '马上注册',

  'home.section1.heading': '为何选择米得',
  'home.section1.subHeading':
    '米得具备了适合所有金融产品类型的投资工具，并且拥有十年以上金融投资经验的投资专家与您一起投资。米得讲究透明化，稳定以及持续性，无论您是初学者，还是经验丰富的投资家，战略家，我们都可以提供您最佳的投资工具以及稳定的投资环境。',
  'home.section1.cta': '更多关于米得',
  'home.section1.item1.heading': '安全',
  'home.section1.item1.subHeading':
    '公司所对接合作的经纪商都是经过非常严格的筛选，都具有公信力高的监督机构来监督，确保客户的本金是安全的。',
  'home.section1.item2.heading': '交易透明 实时监督',
  'home.section1.item2.subHeading':
    '客户拥有属于自己名下的MT4账号，随时可以登入查询，所有交易记录都一览无遗。',
  'home.section1.item3.heading': '专业',
  'home.section1.item3.subHeading':
    '米得拥有多方位健全的团队，从操盘手，金融讲师团，市场顾问团队以及客服都是身经百战，确保客户得到最好的服务。',

  'home.section2.heading': '金融产品类型',
  'home.section2.subHeading':
    '米得具备了适合所有金融产品类型的投资工具，并且拥有十年以上金融投资经验的投资专家与您一起投资。',
  'home.section2.item1.heading': 'MIT镜像交易',
  'home.section2.item1.subHeading':
    '“镜像交易”是指金融投资者运用先进的跟单技术进行交易，由策略信号商提供交易信号，让投资新手可以实时跟随投资高手的买卖信号并自动做单的一种新型投资模式。镜像交易也是EA的一种，他可以为任何MT4平台提供服务。',

  'home.section4.heading': 'METATRADER 4 平台',
  'home.section4.subHeading':
    'MetaTrader 4是世界首选交易平台，其方便易用的界面和专业定制功能，让投资者轻松执行他们的交易策略。我们的交易者需要随时在全世界任何地方快速获得交易账户。为此，我们提供网站、平板电脑、或手机的最高效和最强大的交易平台，解决客户的所有交易需要。浏览我们的平台和服务，立即下载至您所选择的设备。',
  'home.section4.cta': '立即下载',

  'home.section5.heading': '最新消息',
  'home.section5.subHeading':
    '范例文字，请取代此段落文字。此段落文字为范例文字内容，请务必取代。范例文字，请取代此段落文字。此段落文字为范例文字内容，请务必取代。',
  'home.section5.cta': '阅读更多',

  'home.section6.heading': '个人财务风险评估',
  'home.section6.subHeading':
    '想要了解您的个人投资风险能力吗？米得专属为您提供个人财务风险评估。',
  'home.section6.cta': '马上开始',

  'home.section7.heading': '合作伙伴',
  'home.section7.cta': '了解更多合作伙伴',

  'home.section8.heading': '集英商务教育学院',
  'home.section8.subheading1': '“知识创造终极财富自由”',
  'home.section8.subheading2':
    '集英商务教育学院<b>致力培育、启发及栽培优秀的专业投资人才</b>，给予专门的课程教导以及精准的市场分析，尽心打造优秀交易记录的团队与杰出人才。 我们为各阶层的卓越人士提供独一无二的学习机会，以发展并训练他们在金融市场的投资技能。集英无与伦比的培训课程，可让您跃身成为成功企业家或发起投资事业王国的绝佳机会。',
  'home.section8.cta1': '更多详情',
  'home.section8.cta2': '更多关于集英',

  'footer.register.heading': '立即注册',
  'footer.register.subHeading': '立刻开设账户，开始进行金融交易',
  'footer.register.subHeading_academy': '立即开设集英账户',
  'footer.login.heading': '联系我们',
  'footer.login.heading_academy': '联系集英',
  'footer.login.subHeading': '如有任何疑问，欢迎电邮至support@midasama.com',
  'footer.login.subHeading_academy':
    '如有任何疑问，欢迎电邮至enquiry@elitebiz.academy',
  'footer.company.description':
    '米得具备了适合所有金融产品类型的投资工具，并且拥有十年以上金融投资经验的投资专家与您一起投资。米得讲究透明化，稳定以及持续性，无论您是初学者，还是经验丰富的投资家，战略家，我们都可以提供您最佳的投资工具以及稳定的投资环境。',
  'footer.disclaimer.heading': '风险免责声明',
  'footer.disclaimer.subHeading':
    'Midasama仅通过其平台（包括其合作伙伴平台）为投资者提供社交交易经验以 复制经纪商交易活动或跟单交易作为参考。Midasama及其合作伙伴的平台，将保持中立与独立，不参与或负责任何经纪商进行的任何交易活动。 投资者透过Midasama平台（包括其合作伙伴平台）参考经纪商的交易记录和/或分析进而做出决定以复制经纪商交易活动或进行跟单交易。在做出这个决定时，投资者承诺一切与交易活动相关的所有决定或可能造成的任何损失，Midasama及其合作伙伴的平台将免除承担与投资者遭受的亏损相关的任何责任。',
  'footer.disclaimer.subHeading2':
    '投资者必须明白，经纪商过去的表现并不代表未来的表现。Midasama及其合作伙伴的平台没有承诺投资者任何根据交易记录和分析而可能实现的盈利或亏损。同时，Midasama及其合作伙伴平台也要在此向投资者声明，在一些不可预见的情况下，投资者或客户也可能无法复制经纪商交易活动或进行跟单交易。此中文版免责声明为英文版的译本，如中、英文两个版本有任何差异，概以英文版本为准。',
  'footer.copyright': '版权所有',

  'product.item1.heading': '外汇',
  'product.item1.subHeading': '外汇市场是迄今为止全世界最大最具流动性的市场 。',
  'product.item2.heading': '股票',
  'product.item2.subHeading':
    '股权市场也被称为股票市场，同样极受差价合约交易者的欢迎。',
  'product.item3.heading': '指数',
  'product.item3.subHeading':
    '投资者乐于交易世界最大最知名股票市场的指数，而不需要去购买相关资产的股份。',
  'product.item4.heading': '商学院',
  'product.item4.subHeading':
    '教导与培养投资者对于投资的正确理念以及如何筛选投资产品。',
  'product.item5.heading': '贵金属',
  'product.item5.subHeading': '贵金属被视为“避风港”交易工具。',

  'riskprofile.question.heading': '个人财富风险评估',
  'riskprofile.question1':
    '您愿意挪出您净值的多少成，以进行投资？<br/>请注意，当您投资在投资产品时，您的资本就有可能会蒙受亏损*。',
  'riskprofile.question1.option1': '零巴仙',
  'riskprofile.question1.option2': '介于0巴仙到50巴仙之间',
  'riskprofile.question1.option3': '超过50巴仙',
  'riskprofile.question2':
    '一般上，投资涉及风险和回酬的取舍，过去的记录显示，获得高回酬的投资者往往都经历过相应比较高幅度的波动及亏损。为了获得您理想中的回酬，以下哪一句话最能贴切地形容您愿意承担的亏损程度？',
  'riskprofile.question2.option1': '我愿意接受最低数额的资本亏损',
  'riskprofile.question2.option2': '我愿意接受中等程度的资本亏损',
  'riskprofile.question2.option3': '我愿意接受极大幅度的资本亏损',
  'riskprofile.question3': '整体来说，以下哪一项最能贴切地形容您的投资目标？',
  'riskprofile.question3.option1': '资本保存',
  'riskprofile.question3.option2': '长期稳定的收入',
  'riskprofile.question3.option3': '结合收入与资本成长',
  'riskprofile.question3.option4': '达至大幅度的长期资本成长',
  'riskprofile.question3.option5': '资本大幅度增值',
  'riskprofile.question4':
    '以下答案选项例举五个投资组合及它们在短时期内（比如一年内）的潜在*收益及亏损。哪一个投资组合例子最吸引您？',
  'riskprofile.question4.option1':
    '投资组合(a) — 为了9%的上涨潜能，我愿意接受亏损3%的风险。',
  'riskprofile.question4.option2':
    '投资组合(b) — 为了18%的上涨潜能，我愿意接受亏损10%的风险。',
  'riskprofile.question4.option3':
    '投资组合(c) — 为了25%的上涨潜能，我愿意接受亏损15%的风险。',
  'riskprofile.question4.option4':
    '投资组合(d) — 为了31%的上涨潜能，我愿意接受亏损20%的风险。',
  'riskprofile.question4.option5':
    '投资组合(e) — 为了40%的上涨潜能，我愿意接受亏损27%的风险。',
  'riskprofile.question5':
    '如果您的投资因市场波动而在一段时间内价格滑落，您会怎么处理您的投资？',
  'riskprofile.question5.option1':
    '我不想保留任何出现亏损的投资，所以我将脱售我的投资。',
  'riskprofile.question5.option2': '如果跌幅很大，我将脱售我的投资。',
  'riskprofile.question5.option3':
    '如果跌幅很大（大幅度贬值），我将脱售一些投资，并将等待剩余的投资价值回弹。',
  'riskprofile.question5.option4':
    '不论跌幅多大，我都不会脱售投资，因为我要等待投资价值回弹。',
  'riskprofile.question5.option5':
    '不论跌幅多大，我都不会脱售投资，而且还会趁低价买进更多投资产品。',
  'riskprofile.question6':
    '一般上，回酬越高，风险和波动越高。以下答案选项说明五种不同的投资组合在一段长时间内的波动，比如十年。您最能接受哪一项？',
  'riskprofile.question6.option1':
    '投资组合(a) – 价值可能会出现有限度的波动，上下5%，可能会小赚或小亏。',
  'riskprofile.question6.option2':
    '投资组合(b) – 价值可能会出现更大的波动，即上下10%，可能赚幅更大，亏损也更大。',
  'riskprofile.question6.option3':
    '投资组合(c) – 价值可能会出现中等幅度的波动，上下15%，可能会有中等的赚幅与亏损。',
  'riskprofile.question6.option4':
    '投资组合(d) – 价值可能会出现相当大的波动，上下20%，可能会有较大的赚幅及亏损。',
  'riskprofile.question6.option5':
    '投资组合(e) – 价值可能会出现极大的波动，上下超过20%，可能会有极大的赚幅及亏损。',
  'riskprofile.answer1.heading': '安全',
  'riskprofile.answer1.point1':
    '您一般上不想承担任何投资风险，因为您无法接受任何投资上的亏损。',
  'riskprofile.answer1.point2': '投资性质的金融产品并不适合您。',
  'riskprofile.answer1.point3':
    '比较适合您的是以现行利率赚取回酬的产品，无论利率是否随通货膨胀率调整。',
  'riskprofile.answer2.heading': '非常谨慎',
  'riskprofile.answer2.point1':
    '您一般上愿意承担最低的投资风险，以换取最低的回酬潜力。',
  'riskprofile.answer2.point2':
    '可能适合您的产品的资本价值或许会有波动，甚至可能滑落至低于您的投资本金。在正常的市场条件下，波动估计会维持在最低幅度（虽然无法保证一定如此），而您能接受这个程度的波动。',
  'riskprofile.answer2.point3': '1号风险评级的投资产品或许适合您。',
  'riskprofile.answer3.heading': '谨慎',
  'riskprofile.answer3.point1':
    '您一般上愿意承担低投资风险，以换取低水平的回酬潜力。',
  'riskprofile.answer3.point2':
    '可能适合您的产品的资本价值或许会有波动，甚至可能滑落至低于您的投资本金。在正常的市场条件下，波动幅度估计相当低（虽然无法保证一定如此），而您能接受这个程度的波动。',
  'riskprofile.answer3.point3': '2号或以下风险评级的投资产品可能适合您。',
  'riskprofile.answer4.heading': '平衡',
  'riskprofile.answer4.point1':
    '您一般上愿意承担中等水平的投资风险，以换取中等水平的回酬潜力。',
  'riskprofile.answer4.point2':
    '资本价值或许会有波动，甚至可能滑落至低于您的投资本金。波动幅度估计高于适合低风险投资者的投资产品，但低于适合高风险投资者的投资产品。',
  'riskprofile.answer4.point3': '3号或以下风险评级的投资产品可能适合您。',
  'riskprofile.answer5.heading': '大胆',
  'riskprofile.answer5.point1':
    '您一般上愿意承担高水平的投资风险，以换取高水平的回酬潜力。',
  'riskprofile.answer5.point2':
    '资本价值或许会出现明显的波动，甚至可能滑落至远远低于您投资本金。您了解风险/回酬的定律，并且可以接受这个程度的波动。',
  'riskprofile.answer5.point3': '4号或以下风险评级的投资产品可能适合您。',
  'riskprofile.answer6.heading': '投机',
  'riskprofile.answer6.point1':
    '您一般上愿意承最大的投资风险，以换取最大的回酬潜力。',
  'riskprofile.answer6.point2':
    '资本价值或许会出现极大幅度的波动，甚至可能滑落至远远低于您投资本金。您了解风险/回酬的定律，并且可以接受这个程度的波动。',
  'riskprofile.answer6.point3': '5号或以下风险评级的投资产品可能适合您。',
  'riskprofile.result.heading': '风险评估结果',
  'riskprofile.button.previous': '返回',
  'riskprofile.button.next': '继续',

  'product.heroBanner.heading': '金融产品类型',
  'product.heroBanner.subheading': '了解米得平台的投资金融产品类型',
  'product.section1.heading': 'MIT镜像交易',
  'product.section1.description':
    '"镜像交易"是指金融投资者运用先进的跟单技术进行交易，由策略信号商提供交易信号，让投资新手可以实时跟随投资高手的买卖信号并自动做单的一种新型投资模式。镜像交易也是EA的一种，可以为任何MT4平台提供服务。也是指在某第三方镜像平台与交易商签署账户关联协议后，经投资人授权，然后委托平台同步操作账户，投资人在平台上指定某操盘手，该操盘手一旦操作了自己的账户，平台就会根据户口的资金管理方案做同步复制交易。镜像交易让非专业的投资者也可以享受专业投资者带来的收益，并且降低投资风险。',

  'about.heroBanner.heading': '为何选择米得',
  'about.heroBanner.subheading': '了解米得平台的外汇投资模式',
  'about.section1.heading': '颠覆性外汇投资模式',
  'about.section1.subheading':
    '一个全新外汇投资模式：外汇跟单交易，在炎炎夏日为投资市场带来了一股清凉。 米得外汇跟单交易平台提供金融理财的一站式服务，省时省力省心，互利双赢。',
  'about.section2.heading': '全世界最顶尖的外汇操盘团队',
  'about.section2.heading2': '投资工具',
  'about.section2.heading3':
    '米得具备了适合所有金融<br class=\'hidden-sm\'/>产品类型的',
  'about.section2.subheading':
    '米得跟单交易平台是由一群金融和外汇专家创建和组成，是一个累计了庞大的交易数据库的外汇平台。平台的两端，连接着精选的操盘高手和投资者。通过平台的筛选机制，把高手的交易信号做排行。投资者可以把账户资金，根据自己的判断自由分配跟随到不同的高手信号中。每当“交易高手”执行交易操作，投资者的账户会自动跟随执行操作。',
  'about.section2.subheading2':
    '并且拥有十年以上金融投资经验的投资专家与您一起投资。米得讲究透明化，稳定以及持续性，无论您是初学者，还是经验丰富的投资家，战略家，我们都可以提供您最佳的投资工具以及稳定的投资环境。',
  'about.product1.heading': '外汇',
  'about.product1.subheading': '外汇市场是迄今为止全世界最大最具流动性的市场',
  'about.product2.heading': '股票',
  'about.product2.subheading':
    '股权市场也被称为股票市场，同样极受差价合约交易者的欢迎',
  'about.product3.heading': '指数',
  'about.product3.subheading':
    '投资者乐于交易世界最大最知名股票市场的指数，而不需要去购买相关资产的股份',
  'about.product4.heading': '商学院',
  'about.product4.subheading':
    '教导与培养投资者对于投资的正确理念以及如何筛选投资产品',
  'about.product5.heading': '贵金属',
  'about.product5.subheading': '贵金属被视为“避风港”交易工具',
  'about.section4.heading': '米得具备以下特点',
  'about.benefit1.heading': '安全性高',
  'about.benefit1.subheading':
    '米得不以集资方式进行操盘。交易账户以自己的名字注册，只有本人才有权利提取资金，操盘团队是代为操盘，投资的安全性高。',
  'about.benefit2.heading': '交易透明。实时监督',
  'about.benefit2.subheading':
    '在米得跟单系统中，所做的每一笔交易都透明且清晰可查，我们可随时对账户进行监督。',
  'about.benefit3.heading': '专业的操盘手',
  'about.benefit3.subheading':
    '米得不担保也不承诺固定收益，不夸大收益。市场瞬息万变，基金收益率也跟着波动，跟单的收益率是跟着市场波动来决定的。',
  'about.benefit4.heading': '方便快捷',
  'about.benefit4.subheading':
    '我们不需要操作外汇，在米得平台上委托操盘手打理，我们只要关注盈利多少即可，并可以选择撤资或更换团队。',
  'about.benefit5.heading': '双赢的代理制度',
  'about.benefit5.subheading':
    '通过米得，我们为您展现多种合作赢利通道，您可以创业成为代理商，为自己创造巨大可观的财富！',
  'about.section5.heading': '米得的使命',
  'about.section5.subheading':
    '持续创新提供有竞争力的投资理财方案及服务，<br/>帮助客户创造最大价值。',
  'about.vision1.heading': '价值',
  'about.vision1.subheading':
    '我们是一班充满活力和有经验的团队组成，专注于创新与坚守诚信是我们的专业。',
  'about.vision2.heading': '憧憬',
  'about.vision2.subheading':
    '我们致力于追求提供高质量的服务与解决方案，得以达到客户的期望。',
  'about.vision3.heading': '团队',
  'about.vision3.subheading':
    '米得汇聚了最顶尖的投资精英来为投资者们提供服务。我们力求成为一个卓越，领导，发展与诚信的团队。凭借着我们的专业，我们坚信成为最有优势的投资团队。',

  'news.heroBanner.heading': '最新活动资讯',
  'news.heroBanner.subheading': '阅读有关米得的外汇新闻和活动',

  'platform.heroBanner.heading': '平台',
  'platform.heroBanner.subheading': '下载米得所使用的平台版本',
  'platform.section1.heading':
    '<span class=\'text-letterspacing-0\'>METATRADER 4</span><br class=\'visible-xs\'/>客户端',
  'platform.section1.subheading':
    'MetaTrader 4是世界首选交易平台，其方便易用的界面和专业定制功能，让投资者轻松执行他们的交易策略。我们的交易者需要随时在全世界任何地方快速获得交易账户。为此，我们提供网站、平板电脑、或手机的最高效和最强大的交易平台，解决客户的所有交易需要。浏览我们的平台和服务，立即下载至您所选择的设备。',
  'platform.advantage.heading': '主要优势',
  'platform.advantage.point1': '轻松方便的使用界面',
  'platform.advantage.point2': '一键轻松进行交易',
  'platform.advantage.point3': '预编程设置的分析工具',
  'platform.advantage.point4': '允许覆盖适用分析工具',
  'platform.advantage.point5': '多个图标以及分析',
  'platform.advantage.point6': '多语言平台',
  'platform.advantage.point7': '每日提供对帐单',
  'platform.advantage.point8':
    '实时客户帐户小结，包括帐户净值，浮动盈利及损失等',
  'platform.advantage.point9': '止损设施',
  'platform.section2.subheading':
    '有九个时间图为每个金融工具提供详细的报价动态分析。50多个内置指标和工具帮助简化分析过程，判别趋势，鉴定各种图形，确定出入点，等等。另外，一个工具亦可加用于别个，这样对各种交易系统都非常方便。',
  'platform.section3.heading':
    '下载<span class=\'text-letterspacing-0\'>METATRADER 4</span>',
  'platform.desktop.heading': '电脑版',
  'platform.desktop.subheading':
    'MT4是所有不同经验水平的交易者的完美之选，因其友好的用户界面、先进的功能、技术分析工具和自动交易能力而受到推崇。',
  'platform.desktop.point1': '今天高效交易平台的黄金准则',
  'platform.desktop.point2':
    '支持外汇交易、期货、指数、股票及其他CFD产品的工具',
  'platform.desktop.point3': '超过30种语言',
  'platform.desktop.point4': '金融微软Windows操作系统的设备',
  'platform.desktop.button.download': '下载电脑版本',
  'platform.desktop.button.download2': '下载电脑版本',
  'platform.mobile.heading': '手机版',
  'platform.mobile.subheading':
    '我们免费的安卓和iPhone/iPad app，让您能在移动中通过您的移动设备在米得平台上交易。只需要连接互联网，就能进入您的交易账户和畅游激动人心的金融市场。',
  'platform.mobile.point1': '专为MT4平台设置的当地应用程序',
  'platform.mobile.point2':
    '与所有安卓、iPhone和iPad plus、安卓4.0及以上的所有版本兼容',
  'platform.mobile.point3': '点值计算器、经济新闻日历、实时定价和交易执行',
  'platform.mobile.point4': '不同时间框架、更高级的图表功能设备',
  'platform.mobile.button.download': '下载安卓版本',
  'platform.mobile.button.download2': '下载IOS版本',

  'partnerships.heroBanner.heading': '合作伙伴',
  'partnerships.heroBanner.subheading': '全球领先、安全、稳定的云计算产品',

  'education.heroBanner.heading': '外汇知识',
  'education.heroBanner.subheading': '相关影片让您了解更多外汇市场和交易',

  'academy.heroBanner.heading': '集英商学院',
  'academy.section1.heading': '“知识创造终极财富自由”',
  'academy.section1.subheading':
    '集英商务教育学院致力培育、启发及栽培优秀的专业投资人才，给予专门的课程教导以及精准的市场分析，尽心打造优秀交易记录的团队与杰出人才。 我们为各阶层的卓越人士提供独一无二的学习机会，以发展并训练他们在金融市场的投资技能。集英无与伦比的培训课程，可让您跃身成为成功企业家或发起投资事业王国的绝佳机会。',
  'academy.section2.subheading':
    '集英商务教育学院拥有经验丰富的专业人士，为大众提供一个简单而全面的知识传授课程，使您能够有效及精明的管理自己的投资组合，同时在不同的市场管道创造多范围的收入渠道，成为一名真正成功的精明投资者。',
  'academy.course.heading': '集英商务教学',
  'academy.course.item1.heading': '专业团队私相传授',
  'academy.course.item1.subheading':
    '金融市场历练老成的专业人士经验与技能传授。',
  'academy.course.item2.heading': '实践培训',
  'academy.course.item2.subheading':
    '我们提供“着手学习” 实景的培训方式让您能更深入了解以及适应真实的市场交易环境。70%的实践课程和30%的理论课程。',
  'academy.course.item3.heading': '多元化投资机会教学',
  'academy.course.item3.subheading':
    '我们将分享如何将投资资产多元化，并如何有效益管理以及应对不同市场的变化，创造更多元的财富。我们提供有效的专业知识，教导您在如何减低风险及应对市场波动的同时可不影响收益回报。',
  'academy.course.item4.heading': '为您的投资增值',
  'academy.course.item4.subheading':
    '学习从当前的投资中获取利润，同时在金融市场上创造出多个收入来源。',
  'academy.source.item1.heading': '最短时间与精力创造额外的收入来源',
  'academy.source.item2.heading': '精准的交易技术与提示分享，以赚取巨大利润',
  'academy.source.item3.heading':
    '整合资源平台，提供个人提升及发展技能学习潜力',
  'academy.source.item4.heading':
    '获得当前金融市场动向与前景趋势第一手内幕消息',
  'academy.source.item5.heading': '多元化投资可减低损失风险并达到可观收益',
  'academy.learn.heading': '集英商务学院',
  'academy.learn.subheading': '投资资源',
  'academy.learn.item1.heading': '最短时间与精力创造额外的收入来源',
  'academy.learn.item2.heading': '获得当前金融市场动向与前景趋势第一手内幕消息',
  'academy.learn.item3.heading': '精准的交易技术与提示分享，以赚取巨大利润',
  'academy.learn.item4.heading': '多元化投资可减低损失风险并达到可观收益',
  'academy.learn.item5.heading': '整合资源平台，提供个人提升及发展技能学习潜力',
  'academy.point1.heading': '专业金融顾问',
  'academy.point1.subheading': '给予最全面及独家培训',
  'academy.point2.heading': '实践课程',
  'academy.point2.subheading': '独家投资锲机分享、专有交易流程',
  'academy.point3.heading': '发掘及掌握股票市场',
  'academy.point3.subheading':
    '外汇交易市场、加密货币市场、房地产、商业于创业投资等的黄金机会',
  'academy.form.heading': '立即预订名额！',
  'academy.form.firstname': '姓氏',
  'academy.form.firstname.error': '请填入您的姓氏',
  'academy.form.lastname': '名字',
  'academy.form.lastname.error': '请填入您的名字',
  'academy.form.contactno': '联络电话',
  'academy.form.contactno.error': '请填入您的联络电话',
  'academy.form.email': '邮箱',
  'academy.form.email.error': '请填入您的邮箱',
  'academy.form.nationality': '国籍',
  'academy.form.nationality.error': '请填入您的国籍',
  'academy.form.username': '米得会员账号(非必填)',
  'academy.form.username.error': '请填入您的米得会员账号',
  'academy.form.submit': '提交',
  'academy.form.response.success': '表单呈交成功。我们会尽快回复您。',
  'academy.form.response.error': '表单呈交失败。请待会再试。'
};
