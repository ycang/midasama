module.exports = {
  'menu.label': 'Menu',
  'menu.item0': 'Home',
  'menu.item1': 'About Us',
  'menu.item2': 'Products',
  'menu.item3': 'Platform',
  'menu.item4': 'Latest News',
  'menu.item5': 'Partnership',
  'menu.item6': 'Education',
  'menu.item7': 'EliteBiz Academy',
  'menu.login': 'Sign In',
  'menu.register': 'Register',

  'home.banner.heading': 'Midasama',
  'home.banner.subHeading':
    'As an international financial platform for you <br class=\'hidden-xs\'/>to create a sustainable wealth pipeline to create <br class=\'hidden-xs\'/> an endless stream of passive income.',
  'home.banner.cta': 'Register Now',

  'home.section1.heading': 'Why Choose Midasama?',
  'home.section1.subHeading':
    'Midasama has investment tools suitable for all types of financial products, and investment experts with more than a decade of financial investment experience invest with you. With a focus on transparency, stability, and sustainability, Midasama can provide you with the best investment tools and a stable investment environment whether you are a beginner or an experienced investor or strategist.',
  'home.section1.cta': 'More About Midasama',
  'home.section1.item1.heading': 'Safety',
  'home.section1.item1.subHeading':
    'The brokers that the company cooperates with are all subject to very rigorous screening. They all have supervision institutions with high credibility to supervise and ensure that the principals of clients are safe.',
  'home.section1.item2.heading': 'Transparency',
  'home.section1.item2.subHeading':
    'Customers have their own MT4 account and can log in at any time. All transaction records are available at a glance.',
  'home.section1.item3.heading': 'Profession',
  'home.section1.item3.subHeading':
    'Midasama has a multi-faceted and sound team, from fryers, financial instructors, marketing consultants and customer service are all in battle, to ensure that customers receive the best service.',

  'home.section2.heading': 'Products',
  'home.section2.subHeading':
    'Midasama is equipped with the investment tools of all types of financial products and backed by a team of professional traders with more than ten years’ experience in financing investment who will trade together with you.',
  'home.section2.item1.heading': 'MIT Mirror Trading',
  'home.section2.item1.subHeading':
    '‘Mirror trading’ is a new investment model that allows financial investors to use state-of-the art trailing techniques to perform the trading tasks. The strategic signal provider would provide trading signals that enable the beginners to track the professional investors’ trading signals in real-time and place an order automatically. Mirror trading is a type of EA that works on all MT4 platforms.',

  'home.section4.heading': 'METATRADER 4 Platform',
  'home.section4.subHeading':
    'Metatrader 4 is the world’s leading trading platform. Its user-friendly interface and customized functions allow the investors to execute their trading strategies easily. Since our traders need to access their trading accounts anytime and anywhere around the world, we offer the most efficient and powerful trading platform via website, tablet or mobile phone to cater to the needs of our clients. Explore our platform and services and download it to your selected devices now.',
  'home.section4.cta': 'Download Now',

  'home.section5.heading': 'Latest News',
  'home.section5.subHeading':
    '范例文字，请取代此段落文字。此段落文字为范例文字内容，请务必取代。范例文字，请取代此段落文字。此段落文字为范例文字内容，请务必取代。',
  'home.section5.cta': 'Read More',

  'home.section6.heading': 'Risk Profile Survey',
  'home.section6.subHeading':
    'Would you like to know better about your risk profile ? Find out more here !',
  'home.section6.cta': 'Let\'s Get Started',

  'home.section7.heading': 'Partnership',
  'home.section7.cta': 'Read More',

  'home.section8.heading': 'EliteBiz Academy',
  'home.section8.subheading1': '“Knowledge to Ultimate Financial Freedom”',
  'home.section8.subheading2':
    'EliteBiz Academy is geared to <b>cultivate, develop and grow superior investment professional with excellent track records</b>. We offer a unique opportunity for exceptional individuals to develop and hone their skills in the financial market. Our training courses could be your best chance of becoming an entrepreneur or launching your investment career.',
  'home.section8.cta1': 'Read More',
  'home.section8.cta2': 'Read More About EliteBiz',

  'footer.register.heading': 'Register Now',
  'footer.register.subHeading': 'Register now and start trading',
  'footer.register.subHeading_academy': 'Register for EliteBiz Academy',
  'footer.login.heading': 'Contact Us',
  'footer.login.heading_academy': 'Contact EliteBiz',
  'footer.login.subHeading_academy': 'enquiry@elitebiz.academy',
  'footer.login.subHeading': 'support@midasama.com',
  'footer.company.description':
    'Midasama is equipped with the investment tools of all types of financial products and backed by a team of professional traders with more than ten years’ experience in financing investment who will trade together with you. ',
  'footer.disclaimer.heading': 'Risk Disclaimer',
  'footer.disclaimer.subHeading':
    'Midasama only provides social trading experiences as a reference to its investors through its platform (and its partners’ platforms) by copying the providers’ trading activities or transactions. Midasama and its partners’ platforms are neutral and independent parties who would not involve in or responsible for any trading activities of the providers.',
  'footer.disclaimer.subHeading2':
    'The investors would refer to the providers’ trading record and/or analyses through Midasama’s platform (and its partners’ platforms) to decide whether to copy the providers’ trading activities or the transactions. By making the decisions, the investors agree to bear all the loses that might occur due to the decisions made in relation to his/her trading activities and that Midasama and its partners’ platforms shall have no liability for any losses suffered by the investors.<br/>Investors must understand that past performance is not indicative of future results. Midasama and its partners’ platforms do not guarantee the investors any possible returns and loses based on the trading records or analyses. Midasama and its partners hereby inform the investors that there are unforeseen circumstances that can prevent the investors or clients from copying the agents’ trading activities or the transactions. Furthermore, our Chinese Disclaimer is translated from the English version. In the event of any discrepancy between the Chinese and English versions, the English version shall prevail',
  'footer.copyright': 'Copyright',

  'product.item1.heading': 'Forex',
  'product.item1.subHeading':
    'The Forex market is currently the biggest and the most liquid market in the world.',
  'product.item2.heading': 'Stock',
  'product.item2.subHeading':
    'Also known as share market, the stock market is popular among traders who invest in the contract for difference (CFD) trading.',
  'product.item3.heading': 'Indices',
  'product.item3.subHeading':
    'Some investors prefer to trade on the indices of the world’s biggest stock markets than purchase the shares of the assets itself.',
  'product.item4.heading': 'Business School',
  'product.item4.subHeading':
    'Teach the investors and cultivate the practice of adopting the right principles of investment and the right method in choosing the investment products.',
  'product.item5.heading': 'Precious Metal',
  'product.item5.subHeading':
    'Precious metal is deemed a ‘safe-haven’ trading tool.',

  'riskprofile.question.heading': '个人财富风险评估',
  'riskprofile.question1':
    'What portion of your net worth would you like to set aside for investments? Please note that there is a potential for loss of your capital when investing in investment products*',
  'riskprofile.question1.option1': '0%',
  'riskprofile.question1.option2': 'Between >0% and 50%',
  'riskprofile.question1.option3': 'Over 50% ',
  'riskprofile.question2':
    'Generally, investing involves a trade-off between risk and return. It has been historically shown that investors who achieve high returns have experienced correspondingly high fluctuations and losses. In order to achieve your expected returns, which statement best describes the degree of losses you are willing to take? ',
  'riskprofile.question2.option1':
    'I am willing to accept minimal amount of capital loss.',
  'riskprofile.question2.option2':
    'I am willing to accept moderate capital loss.',
  'riskprofile.question2.option3': 'I am willing to accept high capital loss.',
  'riskprofile.question3': 'I am willing to accept high capital loss.',
  'riskprofile.question3.option1': 'Capital Preservation',
  'riskprofile.question3.option2': 'A regular stream of stable income 0%',
  'riskprofile.question3.option3': 'A combination of income and capital growth',
  'riskprofile.question3.option4':
    'Achieve substantial long term capital growth',
  'riskprofile.question3.option5': 'High capital appreciation',
  'riskprofile.question4':
    'The following answer options are descriptions of 5 sample portfolios and their potential* portfolio gain and loss outcomes over a short time horizon (i.1 year). Which of the sample portfolio would be most attractive to you? ',
  'riskprofile.question4.option1':
    'Portfolio (a) - I am willing to accept a potential loss of up to 3% for up to 9% upside.',
  'riskprofile.question4.option2':
    'Portfolio (b) - I am willing to accept a potential loss of 10% for up to 18% upside.',
  'riskprofile.question4.option3':
    'Portfolio (c) - I am willing to accept a potential loss of 15% for up to 25% upside.',
  'riskprofile.question4.option4':
    'Portfolio (d) - I am willing to accept a potential loss of 20% for up to 31% upside.',
  'riskprofile.question4.option5':
    'Portfolio (e) - I am willing to accept a potential loss of 27% for up to 40% upside.',
  'riskprofile.question5':
    'What will you do with your investments if the value drops over a period of time due to market fluctuations? ',
  'riskprofile.question5.option1':
    'I do not wish to hold on to any investments at a loss and will sell the investments immediately even if the drop in value is small.',
  'riskprofile.question5.option2':
    'I will sell the investments if the drop in value is large.',
  'riskprofile.question5.option3':
    'I will sell some of the investments if the drop in value is large, and wait for the remaining investments to recover in value.',
  'riskprofile.question5.option4':
    'I will not sell the investments, regardless of the drop in value, as I would like to wait for the investment to recover in value.',
  'riskprofile.question5.option5':
    'I will not sell the investments, regardless of the drop in value, and will buy more to capitalize on the cheaper price.',
  'riskprofile.question6':
    'Generally, higher returns are coupled with higher risks and fluctuations. The following     answer options describe the level of fluctuations in the value of 5 different investment portfolios over a long period of time, e.g. 10 years. Which would you be most comfortable investing in? ',
  'riskprofile.question6.option1':
    'Portfolio (a) – The value may have limited fluctuation of 5% in both directions, with the potential for small gains and losses.',
  'riskprofile.question6.option2':
    'Portfolio (b) – The value may have more fluctuation of 10% in both directions, with the potential for more gains and losses.',
  'riskprofile.question6.option3':
    'Portfolio (c) – The value may have moderate fluctuation of 15% in both directions, with the potential for moderate gains and losses.',
  'riskprofile.question6.option4':
    'Portfolio (d) – The value may have considerable fluctuation of 20% in both directions, with the potential for greater gains and losses.',
  'riskprofile.question6.option5':
    'Portfolio (e) – The value may have extensive fluctuation of over 20% in both directions, with the potential for substantial gains and losses.',
  'riskprofile.answer1.heading': 'SECURE',
  'riskprofile.answer1.point1':
    'You generally do not want to take any investment risk, since you can accept no investment loss.',
  'riskprofile.answer1.point2':
    'Financial products with an investment element are not suitable for you.',
  'riskprofile.answer1.point3':
    'Products that are potentially suitable for you are likely to produce returns that are based on prevailing interest rates which may or may not keep pace with inflation.',
  'riskprofile.answer2.heading': 'VERY <br/>CAUTIOUS',
  'riskprofile.answer2.point1':
    'You are generally comfortable with achieving minimal level of return potential on your investment coupled with minimal risks.',
  'riskprofile.answer2.point2':
    'Capital values of products that are potentially suitable for you can fluctuate and may fall below your original investment. In normal market conditions fluctuation is expected to be minimal (although this is not guaranteed), and you are comfortable with this level of fluctuation.',
  'riskprofile.answer2.point3':
    'Investment products with risk rating 1 are likely to be suitable for you.',
  'riskprofile.answer3.heading': 'CAUTIOUS',
  'riskprofile.answer3.point1':
    'You are generally comfortable with achieving a low level of return potential on your investment coupled with a low level of risk.',
  'riskprofile.answer3.point2':
    'Capital values of products that are potentially suitable for you can fluctuate and may fall below your original investment. In normal market conditions fluctuation is expected to be low (although this is not guaranteed), and you are comfortable with this level of fluctuation.',
  'riskprofile.answer3.point3':
    'Investment products with risk rating 2 or below are likely to be suitable for you.',
  'riskprofile.answer4.heading': 'BALANCED',
  'riskprofile.answer4.point1':
    'You are generally comfortable with achieving a moderate level of return potential on your investment coupled with a moderate level of risk.',
  'riskprofile.answer4.point2':
    'Capital values can fluctuate and may fall below your original investment. Fluctuation is expected to be higher than products that are suitable for investors in lower risk tolerance categories, but not as much as for higher risk tolerance categories.',
  'riskprofile.answer4.point3':
    'Investment products with risk rating 3 or below are likely to be suitable for you.',
  'riskprofile.answer5.heading': 'ADVENTUROUS',
  'riskprofile.answer5.point1':
    'You are generally comfortable with achieving a high level of return potential on your investment coupled with high level of risk.',
  'riskprofile.answer5.point2':
    'Capital values can fluctuate significantly and may fall quite substantially below your original investment. You understand the risk/reward equation, and are comfortable with this level of fluctuation.',
  'riskprofile.answer5.point3':
    'Investment products with risk rating 4 or below are likely to be suitable for you.',
  'riskprofile.answer6.heading': 'SPECULATIVE',
  'riskprofile.answer6.point1':
    'You are generally comfortable with maximising your return potential on investment coupled with maximized risk.',
  'riskprofile.answer6.point2':
    'Capital values can fluctuate widely and may fall substantially below your original investment. You understand the risk/reward equation, and are comfortable with this level of fluctuation.',
  'riskprofile.answer6.point3':
    'Investment products with risk rating 5 or below are likely to be suitable for you.',
  'riskprofile.result.heading': 'Risk Level',
  'riskprofile.button.previous': 'Back',
  'riskprofile.button.next': 'Continue',

  'product.heroBanner.heading': 'Products',
  'product.heroBanner.subheading': '了解米得平台的投资金融产品类型',
  'product.section1.heading': 'MIT Mirror Trading',
  'product.section1.description':
    '‘Mirror trading’ is a new investment model that allows financial investors to use state-of-the art trailing techniques to perform the trading tasks. The strategic signal provider would provide trading signals that enable the beginners to track the professional investors’ trading signals in real-time and place an order automatically. Mirror trading is a type of EA that works on all MT4 platforms.',

  'about.heroBanner.heading': 'About Us',
  'about.heroBanner.subheading': '了解米得平台的外汇投资模式',
  'about.section1.heading': 'A Subvertive Forex Investment Model',
  'about.section1.subheading':
    'A state-of-the-art Forex investment model: The ‘copy trading’ model has brought fresh air to the investment market in the torrid season. Midasama’s copy trading platform provides a convenient and hassle-free one-stop financial management service that creates a win-win situation. ',
  'about.section2.heading': 'The World\'s Top Forex Professional Traders',
  'about.section2.subheading':
    'Midasama’s copy trading platform is founded by and consists of a group of top forex professional traders in the world. It is a Forex trading platform that has accumulated a vast database of trades. The platform connects a huge number of selected professional traders, who are on one end of the model, to the investors on the other end. Its selection mechanism would rank the professional traders’ trading signals and enable the investors to distribute remaining funds in their accounts to different signals, based on their free will and personal judgments. Whenever the ‘master hands’ make a transaction, the investors’ account would follow the pre-set instructions and copy the transactions automatically.',
  'about.section3.heading': '米得具备了适合所有金融产品类型的',
  'about.section3.heading2': '投资工具',
  'about.section3.subheading':
    '并且拥有十年以上金融投资经验的专家与你一起投资。米得讲究透明化，稳定以及持续性，无论你是初学者，还是经验丰富的投资家，战略家，我们都可以提供你最佳的投资工具以及稳定的投资环境。',

  'about.product1.heading': 'Forex',
  'about.product1.subheading':
    'The Forex market is currently the biggest and the most liquid market in the world.',
  'about.product2.heading': 'Stock',
  'about.product2.subheading':
    'Also known as share market, the stock market is popular among traders who invest in the contract for difference (CFD) trading.',
  'about.product3.heading': 'Indices',
  'about.product3.subheading':
    'Some investors prefer to trade on the indices of the world’s biggest stock markets than purchase the shares of the assets itself.',
  'about.product4.heading': 'Business School',
  'about.product4.subheading':
    'Teach the investors and cultivate the practice of adopting the right principles of investment and the right method in choosing the investment products.',
  'about.product5.heading': 'Precious Metal',
  'about.product5.subheading':
    'Precious metal is deemed a ‘safe-haven’ trading tool.',

  'about.section4.heading': 'Midasama Features',
  'about.benefit1.heading': 'High security level',
  'about.benefit1.subheading':
    'Midasama does not operate by mean of crowdfunding. The investors are required to register with their own name. Hence, only the account holders have the right to withdraw the funds in their own accounts. The trading team will then execute on behalf of the investors. Hence it is a model of investment with high level of security. ',
  'about.benefit2.heading': 'Transparent transaction',
  'about.benefit2.subheading':
    'Real-time monitoring – any transaction made in the Midasama system is transparent, clear and auditable. We can monitor the accounts at any time.',
  'about.benefit3.heading': 'Professional traders',
  'about.benefit3.subheading':
    'Midasama does not guarantee and promise any fixed return or exaggerate any return. The market is rapidly changing, so does the return rate. The return rate of copy trading is determined by the market’s fluctuation.',
  'about.benefit4.heading': 'Convenient and fast',
  'about.benefit4.subheading':
    'You do not have to trade foreign currencies. By entrusting professional traders on the Midasama platform to handle the trading, you only have to focus on the returns. Furthermore, you can choose to withdraw your investment or change the team at any time.',
  'about.benefit5.heading': 'Infinity generation system',
  'about.benefit5.subheading':
    'In Midasama, we provide you multiple profit-making channels through our collaboration with other platforms. You can become an agent to create substantial wealth for yourself!！',
  'about.section5.heading': 'Midasama\'s Mission',
  'about.section5.subheading':
    'Strive for continuous innovation to provide competitive investment management plans <br/>and services to facilitate our clients in creating the greatest value for their investments.',
  'about.vision1.heading': 'Value',
  'about.vision1.subheading':
    'We are a dynamic and fast-growing team consists of energetic and young people. Being a professional service provider, we always pursue innovation and uphold integrity.',
  'about.vision2.heading': 'Vision',
  'about.vision2.subheading':
    'We aspire to provide high quality and excellent solutions and services to fulfill our clients\' expectations. ',
  'about.vision3.heading': 'Our Team',
  'about.vision3.subheading':
    'Midasama has gathered a group of top elite investors to provide the best services to our clients. We strive to become an outstanding, leading, growing and credible team. We strongly believe, with our professionalism, we will become the most invincible team.',

  'news.heroBanner.heading': 'Latest News',
  'news.heroBanner.subheading': '阅读有关米得的外汇新闻和活动',

  'platform.heroBanner.heading': 'Platform',
  'platform.heroBanner.subheading': '下载米得所使用的平台版本',
  'platform.section1.heading':
    '<span class=\'text-letterspacing-0\'>METATRADER 4</span><br class=\'visible-xs\'/> APPLICATION',
  'platform.section1.subheading':
    'Metatrader 4 is the world’s leading trading platform. Its user-friendly interface and customized functions allow the investors to execute their trading strategies easily. Since our traders need to access their trading accounts anytime and anywhere around the world, we offer the most efficient and powerful trading platform via website, tablet or mobile phone to cater to the needs of our clients. Explore our platform and services and download it to your selected devices now.',
  'platform.advantage.heading': 'Advantages',
  'platform.advantage.point1': 'Easy and convenient user interface',
  'platform.advantage.point2': 'Execute trading with just one click',
  'platform.advantage.point3': 'Pre-programmed analysing tools',
  'platform.advantage.point4': 'Allow overlay of analysing tools',
  'platform.advantage.point5': 'Multiple charts and analyses',
  'platform.advantage.point6': 'Multilingual platform',
  'platform.advantage.point7': 'Provide daily statements',
  'platform.advantage.point8': 'Provide daily statements',
  'platform.advantage.point9': 'Stop loss capability',
  'platform.section2.subheading':
    '<br/>Metatrader4 has nine time-charts that provide detailed analyses on the price dynamics. It is equipped with over 50 build-in indicators and tools to help simplify the analysis process, identify trends and all types of charts, confirm entry and exit points etc. Furthermore, a tool can be applied on another tool. This is an important feature that can bring convenience to all type of trading systems.',
  'platform.section3.heading': 'METATRADER 4 Installation',
  'platform.desktop.heading': 'Desktop Version',
  'platform.desktop.subheading':
    'MT4 is the perfect solution for traders of different levels of experiences. It is highly approved and recommended for its user-friendly interface, advanced functionality, technical analysis tools and automated trading functions. ',
  'platform.desktop.point1':
    'Gold standard for high efficiency trading platform ',
  'platform.desktop.point2':
    'A tool that support Forex trading, future, index, stock and other CFD products.',
  'platform.desktop.point3': 'Support over 30 different languages',
  'platform.desktop.point4': 'Windows compatible Finance software',
  'platform.desktop.button.download': 'Download Desktop App',
  'platform.desktop.button.download2': 'Download Desktop App',
  'platform.mobile.heading': 'Mobile Version',
  'platform.mobile.subheading':
    'Our free android and iPhone/iPad app allows you to execute trades on Midasama platform while you are on the run. As long as there is an internet connection, you can login to your trading account and start to explore the exhilarating financial market.',
  'platform.mobile.point1': 'Specifically designed for MT4 platform. ',
  'platform.mobile.point2':
    'Compatible with all Android, iPhone and iPad plus, Android 4.0 and above. ',
  'platform.mobile.point3':
    'Pip calculator, financial news calendar, real time quote and trading execution.',
  'platform.mobile.point4':
    'Multiple time frame and advanced charting function.',
  'platform.mobile.button.download': 'Download Android App',
  'platform.mobile.button.download2': 'Download IOS App',

  'partnerships.heroBanner.heading': 'Partnership',
  'partnerships.heroBanner.subheading': '范例文字，请取代此段落文字',

  'education.heroBanner.heading': 'Education',
  'education.heroBanner.subheading': '范例文字，请取代此段落文字',

  'academy.heroBanner.heading': 'EliteBiz Academy',
  'academy.section1.heading': '“Knowledge to Ultimate Financial Freedom”',
  'academy.section1.subheading':
    'EliteBiz Academy is geared to cultivate, develop and grow superior investment professional with excellent track records. We offer a unique opportunity for exceptional individuals to develop and hone their skills in the financial market. Our training courses could be your best chance of becoming an entrepreneur or launching your investment career.',
  'academy.section2.subheading':
    'At EliteBiz Academy, experienced professionals will provide you with a straight-forward and comprehensive education that will enable you to manage your own portfolio and creating multiple streams of income, becoming a truly successful investor. ',
  'academy.course.heading': 'EliteBiz Academy',
  'academy.course.item1.heading': 'Learn from the Professionals',
  'academy.course.item1.subheading':
    'Benefit from the experience and the skills of our financial market professionals.',
  'academy.course.item2.heading': 'Hands-On Training',
  'academy.course.item2.subheading':
    'We offer a unique “learning by doing” training approach that will prepare you to real life situations. Allocation of 70% practical courses and 30% theory classes.',
  'academy.course.item3.heading': 'Diversification Opportunities',
  'academy.course.item3.subheading':
    'We teach to own multiple asset classes that behave differently in various market conditions and respond differently to various economic events. We provide effective tool for reducing risk and volatility without compromising the returns.',
  'academy.course.item4.heading': 'Get the Best Value for Your Investment',
  'academy.course.item4.subheading':
    'Learn to generate profit from your current investment and at the same time create multiple income streams from the financial market.',
  'academy.source.item1.heading':
    'Create additional income with minimal time and effort',
  'academy.source.item2.heading':
    'High precision trading tips allow to generate massive profit',
  'academy.source.item3.heading':
    'Integrated platform provides personal advancement and maximizes skill learning potential',
  'academy.source.item4.heading':
    'Get insider insights on the current financial market outlook',
  'academy.source.item5.heading':
    'Diversification of investment helps to limit losses and capture financial gains',
  'academy.point1.heading': 'Comprehensive and exclusive training',
  'academy.point1.subheading':
    'In financial markets by influential professional advisors',
  'academy.point2.heading': 'Live sessions',
  'academy.point2.subheading':
    'Unique investment opportunities sharing, proprietary deal flow',
  'academy.point3.heading': 'Stock market, forex exchange market',
  'academy.point3.subheading':
    'Cryptocurrency, real estate, business and startup investment and more',
  'academy.learn.heading': 'EliteBiz Academy',
  'academy.learn.subheading': 'Investment Resources',
  'academy.learn.item1.heading':
    'Create additional income with minimal time and effort',
  'academy.learn.item2.heading':
    'Get insider insights on the current financial market outlook',
  'academy.learn.item3.heading':
    'High precision trading tips allow to generate massive profit',
  'academy.learn.item4.heading':
    'Diversification of investment helps to limit losses and capture financial gains',
  'academy.learn.item5.heading':
    'Integrated platform provides personal advancement and maximizes skill learning potential',
  'academy.form.heading': 'Reserve your seat now!',
  'academy.form.firstname': 'First Name',
  'academy.form.firstname.error': 'Please fill up your First Name',
  'academy.form.lastname': 'Last Name',
  'academy.form.lastname.error': 'Please fill up your Last Name',
  'academy.form.contactno': 'Contact No',
  'academy.form.contactno.error': 'Please fill up your Contact No',
  'academy.form.email': 'Email',
  'academy.form.email.error': 'Please fill up your Email',
  'academy.form.nationality': 'Nationality',
  'academy.form.nationality.error': 'Please fill up your Nationality',
  'academy.form.username': 'Midasama Username (optional)',
  'academy.form.username.error': 'Please fill up your Midasama Username',
  'academy.form.submit': 'Submit',
  'academy.form.response.success':
    'Form submitted successful. We will get back to you as soon as possible.',
  'academy.form.response.error':
    'Form submitted failed. Please try again later.'
};
