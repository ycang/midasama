// @flow

// #region imports
import React, {
  Component
} from 'react';
import {
  // Router, // using now ConnectedRouter from react-router-redux v5.x (the only one compatible react-router 4)
  Switch,
  Route
} from 'react-router-dom';
import {ConnectedRouter} from 'react-router-redux';
import {Provider} from 'react-redux';
import configureStore from './redux/store/configureStore';
import {history} from './redux/store/configureStore';
import App from './containers/app';
import Login from './views/login';
import PageNotFound from './views/pageNotFound/PageNotFound'; // not connected to redux (no index.js)
import LogoutRoute from './components/logoutRoute/LogoutRoute';
import ScrollToTop from './components/scrollToTop/ScrollToTop';
// #endregion
import {IntlProvider} from 'react-intl';
import {CookiesProvider} from 'react-cookie';

// #region flow types
type
  Props = any;
type
  State = any;
// #endregion

const store = configureStore();
class Root extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          <CookiesProvider>
            <ConnectedRouter history={history}>
              <ScrollToTop>
                <Switch>
                  {/* logout: just redirects to login (App will take care of removing the token) */}
                  <LogoutRoute path="/logout"/>
                  <IntlProvider locale="en">
                    <App/>
                  </IntlProvider>
                  <Route component={PageNotFound}/>
                </Switch>
              </ScrollToTop>
            </ConnectedRouter>
          </CookiesProvider>
        </div>
      </Provider>
    );
  }
}

export default Root;
