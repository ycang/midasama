// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../../redux/modules/views';
import EducationEdit                  from './EducationEdit';
import * as userAuthActions from '../../../../redux/modules/userAuth';
import * as educationActions from '../../../../redux/modules/education';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    userAuth: state.userAuth,
    education: state.education,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...educationActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EducationEdit);
