// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Row, Form, Input, Icon, Col, Upload, Select, Button, Switch, Divider, Spin, notification} from 'antd';
import {Link} from 'react-router-dom';
import {uploadProps,getUploadedFileId} from '../../../../services/utils';

const {TextArea} = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class EducationEdit extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };


  constructor(props) {
    super(props);
    this.state = {
      id: '',
      title_en: '',
      title_cn: '',
      video_link: '',
      status: false,
      isEdit: false,
      loading: true,
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      this.setState({isEdit: true});
      const {fetchEducationById} = this.props;
      fetchEducationById(id).then((res) => {
        const {education} = this.props;
        const {current} = education;
        this.setState({
          id: current.id,
          video_link: current.video_link,
          title_en: current.title_en,
          title_cn: current.title_cn,
          status: current.status == 'active' ? true : false,
          loading: false,
        });
        return res;
      }).catch((res) => {
        this.setState({loading: false});
        return res;
      });
    } else {
      this.setState({loading: false});
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;

    return (
      <div>
        <Row>
          <Col>
            <h1>{this.state.isEdit ? 'Edit' : 'New'} Education</h1>
            <div>Fill up all the required fields.</div>
            <br/>
            <p>"Video Link" Input Field Explanation:</p>
            <ol>
              <li>Register an account in http://www.youku.com/</li>
              <li>Upload an video in http://playlists.youku.com/and wait for approval (normally within 1 or 2 days)</li>
              <li>Go to your video and click "分享"</li>
              <li>Under "分享" click "复制通用代码" and paste to any text editor, you will get something like this : <br/> <code>iframe height="498" width="510" src='http://player.youku.com/embed/XMzYzMTQ4MDc3Mg==' frameBorder='0' 'allowFullScreen'</code></li>
              <li>Only copy the "src" value and paste to "Video Link". In this case : http://player.youku.com/embed/XMzYzMTQ4MDc3Mg==</li>
              <li>Change 'http' to 'https'. In this case : https://player.youku.com/embed/XMzYzMTQ4MDc3Mg==</li>
            </ol>
            <Link
              to={'/admin/education'}>
              <Icon type="rollback"/> Back
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Form onSubmit={this.handleSubmit}>

                <FormItem
                  {...formItemLayout}
                  label="Title [en]"
                  hasFeedback
                >
                  {getFieldDecorator('title_en', {
                    rules: [
                      {required: true, message: 'Please key in your title!'},
                    ],
                    initialValue: this.state.title_en,
                    onChange: (e) => this.handleChange(e, 'title_en'),
                  })(
                    <Input placeholder=""/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Title [cn]"
                  hasFeedback
                >
                  {getFieldDecorator('title_cn', {
                    rules: [
                      {required: true, message: 'Please key in your title!'},
                    ],
                    initialValue: this.state.title_cn,
                    onChange: (e) => this.handleChange(e, 'title_cn'),
                  })(
                    <Input placeholder=""/>
                  )}
                </FormItem>


                <FormItem
                  {...formItemLayout}
                  label="Video Link"
                  hasFeedback
                >
                  {getFieldDecorator('video_link', {
                    rules: [
                      {required: true, message: 'Please key in your Video Link !'},
                    ],
                    initialValue: this.state.video_link,
                    onChange: (e) => this.handleChange(e, 'video_link'),
                  })(
                    <Input placeholder=""/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Status"
                  validateStatus=""
                >
                  {getFieldDecorator('status', {
                    valuePropName: 'checked',
                    initialValue: this.state.status,
                    onChange: (e) => this.handleChange(e, 'status'),
                  })(
                    <Switch/>
                  )}
                </FormItem>

                <FormItem
                  wrapperCol={{span: 12, offset: 5}}
                >
                  <Button type="primary" htmlType="submit">Submit</Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }

  /** handle file actions **/
  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item, i) => {
      if (item.uid == e.uid) {
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key]: fileObj});
  };

  handleFileChange(e, key) {
    if (e.fileList != null) {
      if(e.fileList.length > 1){
        e.fileList.map((item,i) => {
          e.fileList.splice(0, 1);
        });
      }
      this.setState({[key]: e.fileList});
    }
  }

  /** end of handle file actions **/

  handleSubmit = (e) => {
    const {createEducation, editEducation} = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.id = this.state.id;
        if (this.state.isEdit) {
          editEducation(values).then((res) => {
            notification['success']({ message: 'Education update successful.'});
            this.props.history.push('/admin/education');
          }).catch((err) => {
            notification['error']({ message: 'Education update fail.'});
          });
        } else {
          createEducation(values).then((res) => {
            notification['success']({ message: 'Education create successful.'});
            this.props.history.push('/admin/education');
          }).catch((err) => {
            notification['error']({ message: 'Education create fail.'});
          });
        }
      }
    });
  };

  handleChange(e, key) {
    if (e.target != null) {
      this.props.form.setFieldsValue({
        [key]: e.target.value,
      });
    } else {
      this.props.form.setFieldsValue({
        [key]: e,
      });
    }
  };
}

export default Form.create()(EducationEdit);
