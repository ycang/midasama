// @flow weak

import React, {
  Component
} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Badge} from 'antd';
import moment from 'moment';

const {Column} = Table;
const confirm = Modal.confirm;

class Education extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const {fetchEducationList} = this.props;
    fetchEducationList().then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  render() {
    return (

      <div>
        <Row>
          <Col>
            <h1>Education</h1>
            <div>Manage Video in Education page</div>
            <br/>
            <Link
              to={'/admin/education/new'}>
              <Icon type="plus-circle-o"/> Create Education
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Table dataSource={this.props.education.list} scroll={{x: 600}}>
                <Column
                  title="Title"
                  dataIndex="title_en"
                  key="title_en"
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  render={(text, record) => (
                    <Badge status={text == 'active' ? 'success' : 'error'}
                           text={text == 'active' ? 'Published' : 'Draft'}/>
                  )}
                />
                />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <span>
                      <Link to={`/admin/education/edit/` + record.id}>Edit</Link>
                      <Divider type="vertical"/>
                      <a href="javascript:;" onClick={()=> this.showConfirm(record.id)}>Delete</a>
                    </span>
                  )}
                />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>);
  }

  showConfirm(id) {
    const { deleteEducation } = this.props;
    confirm({
      title: 'Do you want to delete this education?',
      content: 'Click OK to proceed delete.',
      onOk() {
        deleteEducation(id);
      },
      onCancel() {},
    });
  }
}

export default Education;
