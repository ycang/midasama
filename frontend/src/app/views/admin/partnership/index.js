// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../redux/modules/views';
import Partnership                  from './Partnership';
import * as userAuthActions from '../../../redux/modules/userAuth';
import * as partnershipActions from '../../../redux/modules/partnership';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    userAuth: state.userAuth,
    partnership: state.partnership,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...partnershipActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Partnership);
