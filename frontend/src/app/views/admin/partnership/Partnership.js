// @flow weak

import React, {
  Component
} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Badge} from 'antd';
import moment from 'moment';

const {Column} = Table;
const confirm = Modal.confirm;

class Partnership extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const {fetchPartnershipList} = this.props;
    fetchPartnershipList().then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  render() {
    return (

      <div>
        <Row>
          <Col>
            <h1>Partnership</h1>
            <div>Manage partnership in Partnership page</div>
            <br/>
            <Link
              to={'/admin/partnership/new'}>
              <Icon type="plus-circle-o"/> Create Partnership
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Table dataSource={this.props.partnership.list} scroll={{x: 600}}>
                <Column
                  title="Title"
                  dataIndex="title_en"
                  key="title_en"
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  render={(text, record) => (
                    <Badge status={text == 'active' ? 'success' : 'error'}
                           text={text == 'active' ? 'Published' : 'Draft'}/>
                  )}
                />
                />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <span>
                      <Link to={`/admin/certificate/` + record.id}>Manage Certificate</Link>
                      <Divider type="vertical"/>
                      <Link to={`/admin/partnership/edit/` + record.id}>Edit</Link>
                      <Divider type="vertical"/>
                      <a href="javascript:;" onClick={()=> this.showConfirm(record.id)}>Delete</a>
                    </span>
                  )}
                />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>);
  }

  showConfirm(id) {
    const { deletePartnership } = this.props;
    confirm({
      title: 'Do you want to delete this partnership?',
      content: 'Click OK to proceed delete.',
      onOk() {
        deletePartnership(id);
      },
      onCancel() {},
    });
  }
}

export default Partnership;
