// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {auth} from '../../../../services/auth/index';
import {Row, Form, Input, Icon, Col, Table ,Select, Button, Divider, Spin, notification, DatePicker} from 'antd';
import {Link} from 'react-router-dom';
import {uploadProps,getUploadedFileId} from '../../../../services/utils';
import cx from 'classnames';
import moment from 'moment';
const {TextArea} = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const {Column} = Table;
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class AcademyEdit extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };


  constructor(props) {
    super(props);
    this.state = {
      id: '',
      audits: [],
      first_name: '',
      last_name: '',
      username: '',
      email: '',
      contact_no: '',
      paid_date: null,
      remarks: '',
      nationality: '',
      reference: '',
      status: 'pending-for-payment',
      isEdit: false,
      loading: true,
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      this.setState({isEdit: true});
      const {fetchAcademyById} = this.props;
      fetchAcademyById(id).then((res) => {
        const {academy} = this.props;
        const {current} = academy;
        this.setState({
          id: current.id,
          audits: current.audits,
          first_name: current.first_name,
          last_name: current.last_name,
          username: current.username,
          email: current.email,
          contact_no: current.contact_no,
          nationality: current.nationality,
          reference: current.reference,
          remarks: current.remarks,
          paid_date: current.paid_date ? moment(current.paid_date) : '',
          status: current.status,
          loading: false,
        });
        return res;
      }).catch((res) => {
        this.setState({loading: false});
        return res;
      });
    } else {
      this.setState({loading: false});
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;

    return (
      <div>
        <Row>
          <Col>
            <h1>{this.state.isEdit ? 'Edit' : 'New'} Academy</h1>
            <div>Fill up all the required fields.</div>
            <Link
              to={'/admin/academy'}>
              <Icon type="rollback"/> Back
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Form onSubmit={this.handleSubmit}>
                <FormItem
                  {...formItemLayout}
                  label="First Name"
                >
                  <span className="ant-form-text">{this.state.first_name}</span>
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Last Name"
                >
                  <span className="ant-form-text">{this.state.last_name}</span>
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Contact No"
                >
                  <span className="ant-form-text">{this.state.contact_no}</span>
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Email"
                >
                  <span className="ant-form-text">{this.state.email}</span>
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Nationality"
                >
                  <span className="ant-form-text">{this.state.nationality}</span>
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Midasama Username"
                >
                  <span className="ant-form-text">{this.state.username}</span>
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Reference"
                  hasFeedback
                >
                  {getFieldDecorator('reference', {
                    initialValue: this.state.reference,
                    onChange: (e) => this.handleChange(e, 'reference'),
                  })(
                    <Input placeholder=""/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Remarks"
                  hasFeedback
                >
                  {getFieldDecorator('remarks', {
                    initialValue: this.state.remarks,
                    onChange: (e) => this.handleChange(e, 'remarks'),
                  })(
                    <TextArea style={{minHeight: 200}}/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Status"
                  hasFeedback
                >
                  {getFieldDecorator('status', {
                    rules: [
                      {required: true, message: 'Please select your Status!'},
                    ],
                    initialValue: this.state.status,
                    onChange: (e) => this.handleChange(e, 'status'),
                  })(
                    <Select>
                      <Option value="pending-for-payment">Pending for Payment</Option>
                      <Option value="payment-done">Payment Done</Option>
                      <Option value="others">Others</Option>
                    </Select>
                  )}
                </FormItem>

                <div className={cx({'hide': this.props.form.getFieldValue('status') != 'payment-done'})}>
                  <FormItem
                    {...formItemLayout}
                    label="Paid Date"
                    hasFeedback
                  >
                    {getFieldDecorator('paid_date', {
                      initialValue: this.state.paid_date,
                      onChange: (e) => this.handleChange(e, 'paid_date'),
                    })(
                      <DatePicker/>
                    )}
                  </FormItem>
                </div>

                <FormItem
                  wrapperCol={{span: 12, offset: 5}}
                >
                  <Button type="primary" htmlType="submit">Submit</Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Spin>
        <Divider/>
        <h1>Audit Logs</h1>
        <div>All the data changes logs.</div>
        <br/>
        <Row>
          <Col>
            <Table dataSource={this.state.audits} scroll={{x: 800}}>
              <Column
                title="Old Value"
                dataIndex="old_values"
                key="old_values"
                render={(arr, record) => this.renderValue(arr)}
                width="40%"
              />
              <Column
                title="Event"
                dataIndex="event"
                key="event"
              />
              <Column
                title="New Value"
                dataIndex="new_values"
                key="new_values"
                render={(arr, record) => this.renderValue(arr)}
                width="40%"
              />
              <Column
                title="Last Updated Date"
                dataIndex="updated_at"
                key="updated_at"
                render={(text, record) => (
                  <span>
                      {
                        (text) ? moment(text.date).format("DD-MM-YYYY") : ''
                      }
                    </span>
                )}
              />
            </Table>
          </Col>
        </Row>
      </div>
    );
  }

  renderValue(arr){
    let html = '';
    let counter = 0;
    for (var key in arr){
      if(counter != 0){
        html += ' , ';
      }
      if (arr.hasOwnProperty(key)) {
        if(key == 'paid_date'){
          html += arr[key] ? `${key} : ${ arr[key] ? moment(arr[key]).format('YYYY-MM-DD') : '-' }` : '-';
        }else{
          html += `${key} : ${arr[key] ? arr[key] : '-'} `;
        }
      }
      counter++;
    }
    return html;
  }

  /** handle file actions **/
  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item, i) => {
      if (item.uid == e.uid) {
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key]: fileObj});
  };

  handleFileChange(e, key) {
    if (e.fileList != null) {
      if(e.fileList.length > 1){
        e.fileList.map((item,i) => {
          e.fileList.splice(0, 1);
        });
      }
      this.setState({[key]: e.fileList});
    }
  }

  /** end of handle file actions **/

  handleSubmit = (e) => {
    const {createAcademy, editAcademy} = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.id = this.state.id;

        if (this.state.isEdit) {
          editAcademy(values).then((res) => {
            notification['success']({ message: 'Form update successful.'});
            this.props.history.push('/admin/academy');
          }).catch((err) => {
            notification['error']({ message: 'Form update fail.'});
          });
        }
      }
    });
  };

  handleChange(e, key) {
    if (e && e.target != null) {
      this.props.form.setFieldsValue({
        [key]: e.target.value,
      });
    } else {
      this.props.form.setFieldsValue({
        [key]: e,
      });
    }
  };
}

export default Form.create()(AcademyEdit);
