// @flow weak

import React, {
  Component
} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Badge} from 'antd';
import moment from 'moment';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

const {Column} = Table;
const confirm = Modal.confirm;

class Academy extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const {fetchAcademyList} = this.props;
    fetchAcademyList().then((res) => {
      this.setState({loading: false});
    }).catch((res) => this.setState({loading: false}));
  }

  render() {
    return (

      <div>
        <Row>
          <Col>
            <h1>Academy</h1>
            <div>Manage form submission in Academy page</div>
            <br/>
            <ReactHTMLTableToExcel
              id="test-table-xls-button"
              className="ant-btn ant-btn-primary"
              table="table-to-xls"
              filename="academy-interest-form"
              sheet="tablexls"
              buttonText="Download as XLS"/>
            <Divider/>
          </Col>
        </Row>
        <table id="table-to-xls" className="hide">
          <thead>
            <tr>
              <td>First Name</td>
              <td>Last Name</td>
              <td>Contact No</td>
              <td>Email</td>
              <td>Nationality</td>
              <td>Reference</td>
              <td>Paid Date</td>
              <td>Submitted Date</td>
              <td>Status</td>
              <td>Remarks</td>
            </tr>
          </thead>
          <tbody>
          {
            this.props.academy.list.map((record, key) => {
              return(<tr key={key}>
                  <td>{record.first_name}</td>
                  <td>{record.last_name}</td>
                  <td>{record.contact_no}</td>
                  <td>{record.email}</td>
                  <td>{record.nationality}</td>
                  <td>{record.reference ? record.reference : 'N/A'}</td>
                  <td>{record.paid_date  ? moment(record.paid_date).format("DD-MM-YYYY") : 'N/A'}</td>
                  <td>{record.created_at ? moment(record.created_at.date).format("DD-MM-YYYY") : 'N/A'}</td>
                  <td className="text-uppercase">{record.status ? record.status.replace(/-/g , " ") : 'N/A'}</td>
                  <td>{record.remarks ? record.remarks : 'N/A'}</td>
              </tr>)
            })
          }
          </tbody>
        </table>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Table dataSource={this.props.academy.list} scroll={{x: 800}}>
                <Column
                  title="First Name"
                  dataIndex="first_name"
                  key="first_name"
                />
                <Column
                  title="Last Name"
                  dataIndex="last_name"
                  key="last_name"
                />
                <Column
                  title="Contact No"
                  dataIndex="contact_no"
                  key="contact_no"
                />
                <Column
                  title="Email"
                  dataIndex="email"
                  key="email"
                />
                <Column
                  title="Nationality"
                  dataIndex="nationality"
                  key="nationality"
                />
                <Column
                  title="Reference"
                  dataIndex="reference"
                  key="reference"
                  render={(text, record) => (
                    <span>
                      {
                        (text) ? text : 'N/A'
                      }
                    </span>
                  )}
                />
                <Column
                  title="Paid Date"
                  dataIndex="paid_date"
                  key="paid_date"
                  render={(text, record) => (
                    <span>
                      {
                        (text) ? moment(text.date).format("DD-MM-YYYY") : 'N/A'
                      }
                    </span>
                  )}
                />
                <Column
                  title="Submitted Date"
                  dataIndex="created_at"
                  key="created_at"
                  render={(text, record) => (
                    <span>
                      {
                        (text) ? moment(text.date).format("DD-MM-YYYY") : 'N/A'
                      }
                    </span>
                  )}
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  className="text-uppercase"
                  render={(text, record) => (
                    <span>
                      {
                        (text) ? text.replace(/-/g , " ") : ''
                      }
                    </span>
                  )}
                />
                <Column
                  title="Remarks"
                  dataIndex="remarks"
                  key="remarks"
                  render={(text, record) => (
                    <span>
                      {
                        (text) ? text : 'N/A'
                      }
                    </span>
                  )}
                />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <span>
                      <Link to={`/admin/academy/edit/` + record.id}>Edit</Link>
                      <Divider type="vertical"/>
                      <a href="javascript:;" onClick={()=> this.showConfirm(record.id)}>Delete</a>
                    </span>
                  )}
                />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }

  onChangeStatus(id, status) {
    const {editAcademy, fetchAcademyList} = this.props;
    this.setState({isLoading: true});
    editAcademy({id, status}).then((res) => {
      fetchAcademyList().then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
    }).catch((err) => {
      this.setState({isLoading: false});
    })
  }

  showConfirm(id) {
      const { deleteAcademy } = this.props;
      confirm({
          title: 'Do you want to delete this record?',
          content: 'Click OK to proceed delete.',
          onOk() {
              deleteAcademy(id);
          },
          onCancel() {},
      });
  }
}

export default Academy;
