// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {auth} from '../../../services/auth';
import {Row, Col, Icon} from 'antd';

class Dashboard extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <div className="container">
        <Row>
          <Col xs={24}>
            <p>Dashboard</p>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Dashboard;
