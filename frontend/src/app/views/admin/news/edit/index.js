// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../../redux/modules/views';
import NewsEdit                  from './NewsEdit';
import * as userAuthActions from '../../../../redux/modules/userAuth';
import * as newsActions from '../../../../redux/modules/news';
import * as systemActions from '../../../../redux/modules/system';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    userAuth: state.userAuth,
    news: state.news,
    system: state.system,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...newsActions,
      ...systemActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsEdit);
