// @flow weak

import React, {
  Component
} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Badge} from 'antd';
import moment from 'moment';

const {Column} = Table;
const confirm = Modal.confirm;

class News extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const {fetchNewsList} = this.props;
    fetchNewsList().then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  render() {
    return (

      <div>
        <Row>
          <Col>
            <h1>News</h1>
            <div>Manage news article in Latest News page</div>
            <br/>
            <Link
              to={'/admin/news/new'}>
              <Icon type="plus-circle-o"/> Create News
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Table dataSource={this.props.news.list} scroll={{x: 800}}>
                <Column
                  title="Title"
                  dataIndex="title_en"
                  key="title_en"
                />
                <Column
                  title="News Type"
                  dataIndex="news_type"
                  key="news_type"
                  className="text-uppercase"
                  render={(text, record) => (
                    <span>
                      {
                        text == 'text-only' ? "text only" : text
                      }
                    </span>
                  )}
                />
                <Column
                  title="News Date"
                  dataIndex="news_date"
                  key="news_date"
                  render={(text, record) => (
                    <span>
                      {
                        (text) ? moment(text.date).format("DD-MM-YYYY") : ''
                      }
                    </span>
                  )}
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  className="text-uppercase"
                  render={(text, record) => (
                  <Badge status={text == 'active' ? 'success' : 'error'}
                         text={text == 'active' ? 'Published' : 'Draft'}/>
                  )}
                  />
                  <Column
                    title="Action"
                    key="action"
                    render={(text, record) => (
                      <span>
                      {
                        record.news_type == 'gallery' ?
                          <Link to={`/admin/gallery/` + record.id}>Manage Gallery</Link> : ''
                      }
                        {
                          record.news_type == 'gallery' ? <Divider type="vertical"/> : ''
                        }
                        <Link to={`/admin/news/edit/` + record.id}>Edit</Link>
                      <Divider type="vertical"/>
                      <a href="javascript:;" onClick={() => this.showConfirm(record.id)}>Delete</a>
                    </span>
                    )}
                  />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>
  );
  }

  showConfirm(id) {
    const {deleteNews} = this.props;
    confirm({
    title: 'Do you want to delete this news article?',
    content: 'Click OK to proceed delete.',
    onOk() {
    deleteNews(id);
  },
    onCancel() {},
  });
  }
  }

  export default News;
