// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../redux/modules/views';
import Login                  from './Login';
import * as userAuthActions from '../../../redux/modules/userAuth';
import * as systemActions from '../../../redux/modules/system';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    userAuth: state.userAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...systemActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
