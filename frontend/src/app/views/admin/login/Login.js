// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Row, Col, Form, Icon, Input, Button, Card, notification, Spin} from 'antd';
import auth from '../../../services/auth';
import {
  LoginForm,
} from '../../../components/index';
import {GlobalVars} from "../../../config/global_vars";

const FormItem = Form.Item;

class Login extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  handleOnLoginFormSubmit = async ({userType, email, password}) => {
    if (event) {
      event.preventDefault();
    }
    this.setState({loading: true});

    const {history, logUserIfNeeded} = this.props;

    const userLogin = {userType, email, password};

    try {
      const response = await logUserIfNeeded(userLogin);
      const {
        data
      } = response.payload;
      auth.setToken(data.data.token);
      auth.setUserInfo(data.data);
      notification['success']({message: `Welcome back ${data.data.firstname}!`});
      this.setState({openModal: false});
      history.push({pathname: '/admin/banner'}); // back to Home
    } catch (error) {
      /* eslint-disable no-console */
      console.log(error);
      this.setState({loading: false});
      notification['error']({message: error.data.message});
      console.log('login went wrong..., error: ', error);
      /* eslint-enable no-console */
    }
  };

  render() {
    const {getFieldDecorator} = this.props.form;

    return (
      <div>
        <Row type="flex" style={{minHeight: '80vh', backgroundColor: '#fff'}} justify="space-around"
             align="middle">
          <Col
            xs={20}
            sm={12}
            md={10}
            lg={9}
            xl={6}>
            <Spin tip="Loading..." spinning={this.state.loading}>
              <Card
                hoverable
                title="Admin Panel"
              >
                <LoginForm
                  userType="admin"
                  handleOnFormSubmit={this.handleOnLoginFormSubmit}
                />
              </Card>
            </Spin>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Form.create()(Login);
