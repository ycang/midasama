// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {auth} from '../../../../services/auth/index';
import {Row, Form, Input, Icon, Col, Upload, Select, Button, Switch, Divider, Spin, notification} from 'antd';
import {Link} from 'react-router-dom';
import {uploadProps,getUploadedFileId} from '../../../../services/utils';

const {TextArea} = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class CertificateEdit extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };


  constructor(props) {
    super(props);
    this.state = {
      id: '',
      partnership_id: '',
      title_en: '',
      title_cn: '',
      content_en: '',
      content_cn: '',
      cover_image: '',
      status: false,
      isEdit: false,
      loading: true,
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      this.setState({isEdit: true});
      const {fetchCertificateById} = this.props;
      fetchCertificateById(id).then((res) => {
        const {certificate} = this.props;
        const {current} = certificate;
        const cover_image = [{
          uid: current.cover_image.id,
          name: current.cover_image.name,
          url: current.cover_image.filename,
          thumbUrl: current.cover_image.filename
        }];
        this.setState({
          id: current.id,
          partnership_id: current.partnership_id,
          title_en: current.title_en,
          title_cn: current.title_cn,
          content_en: current.content_en,
          content_cn: current.content_cn,
          cover_image,
          status: current.status == 'active' ? true : false,
          loading: false,
        });
        return res;
      }).catch((res) => {
        this.setState({loading: false});
        return res;
      });
    } else {
      this.setState({loading: false});
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;

    return (
      <div>
        <Row>
          <Col>
            <h1>{this.state.isEdit ? 'Edit' : 'New'} Certificate</h1>
            <div>Fill up all the required fields.</div>
            <br/>
            <Link to={'/admin/certificate/'+ this.state.partnership_id}>
              <Icon type="rollback"/> Back
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Form onSubmit={this.handleSubmit}>
                <FormItem
                  {...formItemLayout}
                  label="Title [en]"
                  hasFeedback
                >
                  {getFieldDecorator('title_en', {
                    rules: [
                      {required: true, message: 'Please key in your title!'},
                    ],
                    initialValue: this.state.title_en,
                    onChange: (e) => this.handleChange(e, 'title_en'),
                  })(
                    <Input placeholder=""/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Title [cn]"
                  hasFeedback
                >
                  {getFieldDecorator('title_cn', {
                    rules: [
                      {required: true, message: 'Please key in your title!'},
                    ],
                    initialValue: this.state.title_cn,
                    onChange: (e) => this.handleChange(e, 'title_cn'),
                  })(
                    <Input placeholder=""/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Content [en]"
                  hasFeedback
                >
                  {getFieldDecorator('content_en', {
                    rules: [
                      {required: true, message: 'Please key in your Content!'},
                    ],
                    initialValue: this.state.content_en,
                    onChange: (e) => this.handleChange(e, 'content_en'),
                  })(
                    <TextArea style={{minHeight: 200}}/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Content [cn]"
                  hasFeedback
                >
                  {getFieldDecorator('content_cn', {
                    rules: [
                      {required: true, message: 'Please key in your Content!'},
                    ],
                    initialValue: this.state.content_cn,
                    onChange: (e) => this.handleChange(e, 'content_cn'),
                  })(
                    <TextArea style={{minHeight: 200}}/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Status"
                  validateStatus=""
                >
                  {getFieldDecorator('status', {
                    valuePropName: 'checked',
                    initialValue: this.state.status,
                    onChange: (e) => this.handleChange(e, 'status'),
                  })(
                    <Switch/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Cover Image"
                  extra="support .jpg , .jpeg , .png only"
                >
                  {getFieldDecorator('cover_image', {
                    rules: [
                      {required: true, message: 'Please upload your Cover Image!'},
                    ],
                    onChange: (e) => this.handleFileChange(e, 'cover_image'),
                    initialValue: this.state.cover_image,
                  })(
                    <Upload {...uploadProps} fileList={this.state.cover_image}
                            onRemove={ (e) => this.handleFileRemove(e,'cover_image')}
                            headers={{authorization: `Bearer ${auth.getToken()}`}}>
                      <Button>
                        <Icon type="upload"/> Click to upload
                      </Button>
                    </Upload>
                  )}
                </FormItem>

                <FormItem
                  wrapperCol={{span: 12, offset: 5}}
                >
                  <Button type="primary" htmlType="submit">Submit</Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }

  /** handle file actions **/
  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item, i) => {
      if (item.uid == e.uid) {
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key]: fileObj});
  };

  handleFileChange(e, key) {
    if (e.fileList != null) {
      if(e.fileList.length > 1){
        e.fileList.map((item,i) => {
          e.fileList.splice(0, 1);
        });
      }
      this.setState({[key]: e.fileList});
    }
  }

  /** end of handle file actions **/

  handleSubmit = (e) => {
    const {createCertificate, editCertificate} = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.id = this.state.id;
        values.cover_image =  getUploadedFileId(values,'cover_image');
        if (this.state.isEdit) {
          editCertificate(values).then((res) => {
            notification['success']({ message: 'Certificate update successful.'});
            this.props.history.push('/admin/certificate/' + this.state.partnership_id);
          }).catch((err) => {
            notification['error']({ message: 'Certificate update fail.'});
          });
        } else {
          createCertificate(values).then((res) => {
            notification['success']({ message: 'Certificate create successful.'});
            this.props.history.push('/admin/certificate/' + this.state.partnership_id);
          }).catch((err) => {
            notification['error']({ message: 'Certificate create fail.'});
          });
        }
      }
    });
  };

  handleChange(e, key) {
    if (e.target != null) {
      this.props.form.setFieldsValue({
        [key]: e.target.value,
      });
    } else {
      this.props.form.setFieldsValue({
        [key]: e,
      });
    }
  };
}

export default Form.create()(CertificateEdit);
