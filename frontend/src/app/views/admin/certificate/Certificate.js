// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Form, Upload, Button, notification, Badge} from 'antd';
import moment from 'moment';
const FormItem = Form.Item;
const {Column} = Table;
const confirm = Modal.confirm;
import {uploadProps,getUploadedFileIds} from '../../../services/utils';
import {auth} from '../../../services/auth/index';

const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class Certificate extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      certificate_images: [],
      loading: true
    }
  }

  componentDidMount() {
    const {fetchCertificateListByPartnershipId} = this.props;
    const id = this.props.match.params.id;
    fetchCertificateListByPartnershipId(id).then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  render() {
    const {certificate} = this.props;
    const {getFieldDecorator} = this.props.form;
    return (
      <div>
        <Row>
          <Col>
            <h1>Certificate Images</h1>
            <div>Manage certificate images</div>
            <br/>
            <Link to={'/admin/partnership'}>
              <Icon type="rollback"/> Back
            </Link>
            <Divider/>
            <h1>Create Certificate Images Form</h1>
            <div>Steps:</div>
            <div>1. Upload multiple certificate images and hit submit.</div>
            <div>2. Edit each certificate image title, content and status by hitting Edit button in the table below.</div>
            <br/>
            <Form onSubmit={this.handleSubmit}>
              <FormItem
                {...formItemLayout}
                label="Certificate Images"
                extra="support .jpg , .jpeg , .png only"
              >
                {getFieldDecorator('certificate_images', {
                  rules: [
                    {required: true, message: 'Please upload your Certificate Images!'},
                  ],
                  onChange: (e) => this.handleFileChange(e, 'certificate_images'),
                  initialValue: this.state.certificate_images,
                })(
                  <Upload {...uploadProps} multiple={true} fileList={this.state.certificate_images}
                          onRemove={ (e) => this.handleFileRemove(e,'certificate_images')}
                          headers={{authorization: `Bearer ${auth.getToken()}`}}>
                    <Button>
                      <Icon type="upload"/> Click to upload
                    </Button>
                  </Upload>
                )}
              </FormItem>
              <FormItem
                wrapperCol={{span: 12, offset: 5}}
              >
                <Button type="primary" htmlType="submit">Submit</Button>
              </FormItem>
            </Form>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row gutter={18}>
            <Col>
              <Table dataSource={certificate.list} scroll={{x: 600}}>
                <Column
                  title="Title"
                  dataIndex="title_en"
                  key="title_en"
                />
                <Column
                  title="Created Date"
                  dataIndex="created_at"
                  key="created_at"
                  render={(text, record) => (
                    <span>
                      {
                        moment(text.date).format("DD-MM-YYYY")
                      }
                    </span>
                  )}
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  render={(text, record) => (
                    <Badge status={text == 'active' ? 'success' : 'error'}
                           text={text == 'active' ? 'Published' : 'Draft'}/>
                  )}
                />
                />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <span>
                                <Link to={`/admin/certificate/edit/` + record.id}>Edit</Link>
                                <Divider type="vertical"/>
                                <a href="javascript:;" onClick={() => this.showConfirm(record.id)}>Delete</a>
                              </span>
                  )}
                />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }

  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item,i) => {
      if(item.uid == e.uid){
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key] : fileObj});
  };

  handleFileChange(e, key){
    if (e.fileList != null) {
      this.setState({ [key] : e.fileList });
    }
  }

  handleSubmit = (e) => {
    const {createCertificate} = this.props;
    e.preventDefault();
    const id = this.props.match.params.id;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.id = this.props.match.params.id;
        values.certificate_images =  getUploadedFileIds(values,'certificate_images');
        createCertificate(values).then((res) => {
          notification['success']({ message: 'Certificate images create successful.'});
          this.setState({ certificate_images : [] });
          this.props.history.push('/admin/certificate/' + id);
        }).catch((err) => {
          notification['error']({ message: 'Certificate images create fail.'});
        });
      }
    });
  };

  showConfirm(id) {
    const {deleteCertificate} = this.props;
    const partnership_id = this.props.match.params.id;
    confirm({
      title: 'Do you want to delete this certificate?',
      content: 'Click OK to proceed delete.',
      onOk() {
        deleteCertificate(id,partnership_id);
      },
      onCancel() {
      },
    });
  }
}

export default Form.create()(Certificate);
