// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../redux/modules/views';
import Product                  from './Product';
import * as userAuthActions from '../../../redux/modules/userAuth';
import * as productActions from '../../../redux/modules/product';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    userAuth: state.userAuth,
    product: state.product,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...productActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product);
