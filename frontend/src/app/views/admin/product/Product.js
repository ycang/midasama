// @flow weak

import React, {
  Component
} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Badge} from 'antd';
import moment from 'moment';

const {Column} = Table;
const confirm = Modal.confirm;

class Product extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const {fetchProductList} = this.props;
    fetchProductList().then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  render() {
    return (

      <div>
        <Row>
          <Col>
            <h1>Product</h1>
            <div>Manage product in Product page</div>
            <br/>
            <Link
              to={'/admin/product/new'}>
              <Icon type="plus-circle-o"/> Create Product
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Table dataSource={this.props.product.list} scroll={{x: 600}}>
                <Column
                  title="Product Name"
                  dataIndex="product_name_en"
                  key="product_name_en"
                />
                <Column
                  title="Title"
                  dataIndex="title_en"
                  key="title_en"
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  render={(text, record) => (
                    <Badge status={text == 'active' ? 'success' : 'error'}
                           text={text == 'active' ? 'Published' : 'Draft'}/>
                  )}
                />
                />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <span>
                      <Link to={`/admin/product/edit/` + record.id}>Edit</Link>
                      <Divider type="vertical"/>
                      <a href="javascript:;" onClick={()=> this.showConfirm(record.id)}>Delete</a>
                    </span>
                  )}
                />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>);
  }

  showConfirm(id) {
    const { deleteProduct } = this.props;
    confirm({
      title: 'Do you want to delete this product?',
      content: 'Click OK to proceed delete.',
      onOk() {
        deleteProduct(id);
      },
      onCancel() {},
    });
  }
}

export default Product;
