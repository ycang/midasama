// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Form, Upload, Button, notification, Badge} from 'antd';
import moment from 'moment';
import {auth} from '../../../services/auth/index';
import {uploadProps,getUploadedFileIds} from '../../../services/utils';

const FormItem = Form.Item;
const {Column} = Table;
const confirm = Modal.confirm;

const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class Gallery extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      gallery_images: [],
      loading: true
    }
  }

  componentDidMount() {
    const {fetchGalleryListByNewsId} = this.props;
    const id = this.props.match.params.id;
    fetchGalleryListByNewsId(id).then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  render() {
    const {gallery} = this.props;
    const {getFieldDecorator} = this.props.form;
    return (
      <div>
        <Row>
          <Col>
            <h1>Gallery Images</h1>
            <div>Manage gallery images</div>
            <br/>
            <Link to={'/admin/news'}>
              <Icon type="rollback"/> Back
            </Link>
            <Divider/>
            <h1>Create Gallery Images Form</h1>
            <div>Steps:</div>
            <div>1. Upload multiple gallery images and hit submit.</div>
            <div>2. Edit each gallery image title, content and status by hitting Edit button in the table below.</div>
            <br/>
            <Form onSubmit={this.handleSubmit}>
              <FormItem
                {...formItemLayout}
                label="Gallery Images"
                extra="support .jpg , .jpeg , .png only"
              >
                {getFieldDecorator('gallery_images', {
                  rules: [
                    {required: true, message: 'Please upload your Gallery Images!'},
                  ],
                  onChange: (e) => this.handleFileChange(e, 'gallery_images'),
                  initialValue: this.state.gallery_images,
                })(
                  <Upload {...uploadProps} multiple={true} fileList={this.state.gallery_images}
                          onRemove={ (e) => this.handleFileRemove(e,'gallery_images')}
                          headers={{authorization: `Bearer ${auth.getToken()}`}}>
                    <Button>
                      <Icon type="upload"/> Click to upload
                    </Button>
                  </Upload>
                )}
              </FormItem>
              <FormItem
                wrapperCol={{span: 12, offset: 5}}
              >
                <Button type="primary" htmlType="submit">Submit</Button>
              </FormItem>
            </Form>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row gutter={18}>
            <Col>
              <Table dataSource={gallery.list} scroll={{x: 600}}>
                <Column
                  title="Title"
                  dataIndex="title_en"
                  key="title_en"
                />
                <Column
                  title="Created Date"
                  dataIndex="created_at"
                  key="created_at"
                  render={(text, record) => (
                    <span>
                      {
                        moment(text.date).format("DD-MM-YYYY")
                      }
                    </span>
                  )}
                />
                <Column
                  title="Position"
                  dataIndex="position"
                  key="position"
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  render={(text, record) => (
                    <Badge status={text == 'active' ? 'success' : 'error'}
                           text={text == 'active' ? 'Published' : 'Draft'}/>
                  )}
                />
                />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <span>
                                <Link to={`/admin/gallery/edit/` + record.id}>Edit</Link>
                                <Divider type="vertical"/>
                                <a href="javascript:;" onClick={() => this.showConfirm(record.id)}>Delete</a>
                              </span>
                  )}
                />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }

  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item,i) => {
      if(item.uid == e.uid){
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key] : fileObj});
  };

  handleFileChange(e, key){
    if (e.fileList != null) {
      this.setState({ [key] : e.fileList });
    }
  }

  handleSubmit = (e) => {
    const {createGallery} = this.props;
    e.preventDefault();
    const id = this.props.match.params.id;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.id = this.props.match.params.id;
        values.gallery_images =  getUploadedFileIds(values,'gallery_images');
        createGallery(values).then((res) => {
          notification['success']({ message: 'Gallery images create successful.'});
          this.setState({ gallery_images : [] });
          this.props.history.push('/admin/gallery/' + id);
        }).catch((err) => {
          notification['error']({ message: 'Gallery images create fail.'});
        });
      }
    });
  };

  showConfirm(id) {
    const {deleteGallery} = this.props;
    const news_id = this.props.match.params.id;
    confirm({
      title: 'Do you want to delete this gallery?',
      content: 'Click OK to proceed delete.',
      onOk() {
        deleteGallery(id,news_id);
      },
      onCancel() {
      },
    });
  }
}

export default Form.create()(Gallery);
