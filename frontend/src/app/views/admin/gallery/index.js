// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../redux/modules/views';
import Gallery                  from './Gallery';
import * as userAuthActions from '../../../redux/modules/userAuth';
import * as galleryActions   from '../../../redux/modules/gallery';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    gallery: state.gallery,
    userAuth: state.userAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...galleryActions,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Gallery);
