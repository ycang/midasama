// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Form,
  Input,
  Icon,
  Col,
  Upload,
  Select,
  Button,
  Switch,
  InputNumber,
  Divider,
  Spin,
  notification
} from 'antd';
import {auth} from '../../../../services/auth/index';
import {uploadProps,getUploadedFileId} from '../../../../services/utils';
import {Link} from 'react-router-dom';

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class GalleryEdit extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      news_id: '',
      title_en: '',
      title_cn: '',
      content_en: '',
      content_cn: '',
      gallery_image: '',
      position: 0,
      status: false,
      isEdit: false,
      loading: true
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      this.setState({isEdit: true});
      const {fetchGalleryById} = this.props;
      fetchGalleryById(id).then((res) => {
        const {gallery} = this.props;
        const {current} = gallery;
        const gallery_image = (current.gallery_image.id) ? [{
          uid: current.gallery_image.id,
          name: current.gallery_image.name,
          url: current.gallery_image.filename,
          thumbUrl: current.gallery_image.filename
        }] : [];
        this.setState({
          id: current.id,
          news_id: current.news_id,
          title_en: current.title_en,
          title_cn: current.title_cn,
          content_en: current.content_en,
          content_cn: current.content_cn,
          gallery_image,
          position: current.position,
          status: current.status == 'active' ? true : false,
          loading: false
        });
        return res;
      }).catch((res) => {
        this.setState({loading: false});
        return res;
      });
    } else {
      this.setState({loading: false});
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    return (
      <div>
        <Row>
          <Col>
            <h1>{this.state.isEdit ? 'Edit' : 'New'} Gallery</h1>
            <div>Fill up all the required fields.</div>
            <br/>
            <Link to={'/admin/gallery/'+ this.state.news_id}>
              <Icon type="rollback"/> Back
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Form onSubmit={this.handleSubmit}>
                <FormItem
                  {...formItemLayout}
                  label="Title [en]"
                  hasFeedback
                >
                  {getFieldDecorator('title_en', {
                    rules: [
                      {required: true, message: 'Please key in your title!'},
                    ],
                    initialValue: this.state.title_en,
                    onChange: (e) => this.handleChange(e, 'title_en'),
                  })(
                    <Input placeholder="" name="title_en"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Content [en]"
                  hasFeedback
                >
                  {getFieldDecorator('content_en', {
                    rules: [
                      {required: true, message: 'Please key in your content!'},
                    ],
                    initialValue: this.state.content_en,
                    onChange: (e) => this.handleChange(e, 'content_en'),
                  })(
                    <Input placeholder="" name="content_en"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Title [cn]"
                  hasFeedback
                >
                  {getFieldDecorator('title_cn', {
                    rules: [
                      {required: true, message: 'Please key in your title!'},
                    ],
                    initialValue: this.state.title_cn,
                    onChange: (e) => this.handleChange(e, 'title_cn'),
                  })(
                    <Input placeholder="" name="title_cn"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Content [cn]"
                  hasFeedback
                >
                  {getFieldDecorator('content_cn', {
                    rules: [
                      {required: true, message: 'Please key in your content!'},
                    ],
                    initialValue: this.state.content_cn,
                    onChange: (e) => this.handleChange(e, 'content_cn'),
                  })(
                    <Input placeholder="" name="content_cn"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Position"
                  hasFeedback
                >
                  {getFieldDecorator('position', {
                    initialValue: this.state.position,
                    onChange: (e) => this.handleChange(e, 'position')
                  })(
                    <InputNumber name="position"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Gallery Image [desktop]"
                  extra="support .jpg , .jpeg , .png only"
                >
                  {getFieldDecorator('gallery_image', {
                    rules: [
                      {required: true, message: 'Please upload your Gallery Image!'},
                    ],
                    onChange: (e) => this.handleFileChange(e, 'gallery_image'),
                    initialValue: this.state.gallery_image,
                  })(
                    <Upload {...uploadProps} fileList={this.state.gallery_image}
                            onRemove={ (e) => this.handleFileRemove(e,'gallery_image')}
                            headers={{authorization: `Bearer ${auth.getToken()}`}}>
                      <Button>
                        <Icon type="upload"/> Click to upload
                      </Button>
                    </Upload>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Status"
                  validateStatus=""
                >
                  {getFieldDecorator('status', {
                    valuePropName: 'checked',
                    initialValue: this.state.status,
                    onChange: (e) => this.handleChange(e, 'status')
                  })(
                    <Switch name="status"/>
                  )}
                </FormItem>

                <FormItem
                  wrapperCol={{span: 12, offset: 5}}
                >
                  <Button type="primary" htmlType="submit">Submit</Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  };

  /** handle file actions **/
  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item,i) => {
      if(item.uid == e.uid){
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key] : fileObj});
  };

  handleFileChange(e, key){
    if (e.fileList != null) {
      this.setState({ [key] : e.fileList });
    }
  }
  /** end of handle file actions **/

  handleSubmit = (e) => {
    const {createGallery, editGallery} = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.id = this.state.id;
        values.gallery_image =  getUploadedFileId(values,'gallery_image');
        if (this.state.isEdit) {
          editGallery(values).then((res) => {
            notification['success']({ message: 'Gallery image update successful.'});
            this.props.history.push('/admin/gallery/' + this.state.news_id);
          }).catch((err) => {
            notification['error']({ message: 'Gallery image update fail.'});
          });
        } else {
          createGallery(values).then((res) => {
            notification['success']({ message: 'Gallery image create successful.'});
            this.props.history.push('/admin/gallery/' + this.state.news_id);
          }).catch((err) => {
            notification['error']({ message: 'Gallery image create fail.'});
          });
        }
      }
    });
  };

  handleChange(e, key) {
    if (e.target != null) {
      this.props.form.setFieldsValue({
        [key]: e.target.value,
      });
    } else {
      this.props.form.setFieldsValue({
        [key]: e,
      });
    }
  }
}

export default Form.create()(GalleryEdit);
