// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../../redux/modules/views';
import GalleryEdit                  from './GalleryEdit';
import * as userAuthActions from '../../../../redux/modules/userAuth';
import * as galleryActions   from '../../../../redux/modules/gallery';
import * as assetActions   from '../../../../redux/modules/asset';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    gallery: state.gallery,
    asset: state.asset,
    userAuth: state.userAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...galleryActions,
      ...assetActions,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GalleryEdit);
