// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../../redux/modules/views';
import SettingEdit                  from './SettingEdit';
import * as userAuthActions from '../../../../redux/modules/userAuth';
import * as systemSettingActions from '../../../../redux/modules/systemSetting';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    userAuth: state.userAuth,
    systemSetting: state.systemSetting,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...systemSettingActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingEdit);
