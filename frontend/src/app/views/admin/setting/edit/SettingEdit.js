// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {Row, Form, Input, Icon, Col, Upload, Select, Button, Switch, Divider, Spin, notification} from 'antd';
import {Link} from 'react-router-dom';
import {auth} from '../../../../services/auth/index';
import {uploadProps,getUploadedFileId} from '../../../../services/utils';

const {TextArea} = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class SettingEdit extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };


  constructor(props) {
    super(props);
    this.state = {
      id: '',
      about_us_video_url: '',
      loading: true,
    };
  }

  componentDidMount() {
    const {fetchSystemSettingList} = this.props;
    fetchSystemSettingList().then((res) => {
      const {systemSetting} = this.props;
      systemSetting.list.map((setting) => {
        if(setting.code == 'about_us_video_url'){
          this.setState({ about_us_video_url : setting.value });
        }
      });
      this.setState({
        loading: false,
      });
      return res;
    }).catch((res) => {
      this.setState({loading: false});
      return res;
    });
  }

  render() {
    const {getFieldDecorator} = this.props.form;

    return (
      <div>
        <Row>
          <Col>
            <h1>System Setting</h1>
            <div>Fill up all the required fields.</div>
            <br/>
            <p>"About Us page video URL" Input Field Explanation:</p>
            <ol>
              <li>Register an account in http://www.youku.com/</li>
              <li>Upload an video in http://playlists.youku.com/and wait for approval (normally within 1 or 2 days)</li>
              <li>Go to your video and click "分享"</li>
              <li>Under "分享" click "复制通用代码" and paste to any text editor, you will get something like this : <br/> <code>iframe height="498" width="510" src='http://player.youku.com/embed/XMzYzMTQ4MDc3Mg==' frameBorder='0' 'allowFullScreen'</code></li>
              <li>Only copy the "src" value and paste to "About Us page video URL". In this case : http://player.youku.com/embed/XMzYzMTQ4MDc3Mg==</li>
              <li>Change 'http' to 'https'. In this case : https://player.youku.com/embed/XMzYzMTQ4MDc3Mg==</li>
            </ol>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Form onSubmit={this.handleSubmit}>
                <FormItem
                  {...formItemLayout}
                  label="About Us page video URL"
                  hasFeedback
                  extra="leave it blank if want to hide video"
                >
                  {getFieldDecorator('about_us_video_url', {
                    initialValue: this.state.about_us_video_url,
                    onChange: (e) => this.handleChange(e, 'about_us_video_url'),
                  })(
                    <Input placeholder=""/>
                  )}
                </FormItem>

                <FormItem
                  wrapperCol={{span: 12, offset: 5}}
                >
                  <Button type="primary" htmlType="submit">Submit</Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }

  /** handle file actions **/
  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item, i) => {
      if (item.uid == e.uid) {
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key]: fileObj});
  };

  handleFileChange(e, key) {
    if (e.fileList != null) {
      if(e.fileList.length > 1){
        e.fileList.map((item,i) => {
          e.fileList.splice(0, 1);
        });
      }
      this.setState({[key]: e.fileList});
    }
  }

  /** end of handle file actions **/

  handleSubmit = (e) => {
    const {editSystemSetting} = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        editSystemSetting(values).then((res) => {
          notification['success']({ message: 'System settings update successful.'});
          this.props.history.push('/admin/systemSetting');
        }).catch((err) => {
          notification['error']({ message: 'System settings update fail.'});
        });
      }
    });
  };

  handleChange(e, key) {
    if (e.target != null) {
      this.props.form.setFieldsValue({
        [key]: e.target.value,
      });
    } else {
      this.props.form.setFieldsValue({
        [key]: e,
      });
    }
  };
}

export default Form.create()(SettingEdit);
