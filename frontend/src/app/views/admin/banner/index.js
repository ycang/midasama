// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../redux/modules/views';
import Banner                  from './Banner';
import * as userAuthActions from '../../../redux/modules/userAuth';
import * as bannerActions   from '../../../redux/modules/banner';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    banner: state.banner,
    userAuth: state.userAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...bannerActions,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Banner);
