// @flow weak

import React, {
  Component
} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Row, Col, Table, Icon, Divider, Spin, Modal, Badge} from 'antd';

const {Column} = Table;
const confirm = Modal.confirm;

class Banner extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const {fetchBannerList} = this.props;
    fetchBannerList().then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  render() {
    const {banner} = this.props;
    return (
      <div>
        <Row>
          <Col>
            <h1>Banner</h1>
            <div>Manage banner in Home page</div>
            <br/>
            <Link
              to={'/admin/banner/new'}>
              <Icon type="plus-circle-o"/> Create Banner
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row gutter={18}>
            <Col>
              <Table dataSource={banner.list} scroll={{x: 600}}>
                <Column
                  title="Title"
                  dataIndex="title_en"
                  key="title_en"
                />
                <Column
                  title="Position"
                  dataIndex="position"
                  key="position"
                />
                <Column
                  title="Status"
                  dataIndex="status"
                  key="status"
                  render={(text, record) => (
                    <Badge status={text == 'active' ? 'success' : 'error'}
                           text={text == 'active' ? 'Published' : 'Draft'}/>
                  )}
                />
                />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <span>
                      <Link to={`/admin/banner/edit/` + record.id}>Edit</Link>
                      <Divider type="vertical"/>
                      <a href="javascript:;" onClick={() => this.showConfirm(record.id)}>Delete</a>
                    </span>
                  )}
                />
              </Table>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }

  showConfirm(id) {
    const {deleteBanner} = this.props;
    confirm({
      title: 'Do you want to delete this banner?',
      content: 'Click OK to proceed delete.',
      onOk() {
        deleteBanner(id);
      },
      onCancel() {
      },
    });
  }
}

export default Banner;
