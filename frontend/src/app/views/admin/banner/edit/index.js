// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../../../redux/modules/views';
import BannerEdit                  from './BannerEdit';
import * as userAuthActions from '../../../../redux/modules/userAuth';
import * as bannerActions   from '../../../../redux/modules/banner';
import * as assetActions   from '../../../../redux/modules/asset';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    banner: state.banner,
    asset: state.asset,
    userAuth: state.userAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...userAuthActions,
      ...bannerActions,
      ...assetActions,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BannerEdit);
