// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Form,
  Input,
  Icon,
  Col,
  Upload,
  Select,
  Button,
  Switch,
  InputNumber,
  Divider,
  Spin,
  notification
} from 'antd';
import {uploadProps,getUploadedFileId} from '../../../../services/utils';
import {Link} from 'react-router-dom';
import {auth} from '../../../../services/auth/index';

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 5},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

class BannerEdit extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      title_en: '',
      title_cn: '',
      subtitle_en: '',
      subtitle_cn: '',
      banner_image_desktop: '',
      banner_image_mobile: '',
      position: 0,
      status: false,
      isEdit: false,
      loading: true
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (id) {
      this.setState({isEdit: true});
      const {fetchBannerById} = this.props;
      fetchBannerById(id).then((res) => {
        const {banner} = this.props;
        const {current} = banner;
        const banner_image_desktop = current.banner_image_desktop.id ? [{
          uid: current.banner_image_desktop.id,
          name: current.banner_image_desktop.name,
          url: current.banner_image_desktop.filename,
          thumbUrl: current.banner_image_desktop.filename
        }] : [];
        const banner_image_mobile = current.banner_image_mobile.id ? [{
          uid: current.banner_image_mobile.id,
          name: current.banner_image_mobile.name,
          url: current.banner_image_mobile.filename,
          thumbUrl: current.banner_image_mobile.filename
        }] : [];
        this.setState({
          id: current.id,
          title_en: current.title_en,
          title_cn: current.title_cn,
          subtitle_en: current.subtitle_en,
          subtitle_cn: current.subtitle_cn,
          banner_image_desktop,
          banner_image_mobile,
          position: current.position,
          status: current.status == 'active' ? true : false,
          loading: false
        });
        return res;
      }).catch((res) => {
        this.setState({loading: false});
        return res;
      });
    } else {
      this.setState({loading: false});
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    return (
      <div>
        <Row>
          <Col>
            <h1>{this.state.isEdit ? 'Edit' : 'New'} Banner</h1>
            <div>Fill up all the required fields.</div>
            <br/>
            <Link to={'/admin/banner'}>
              <Icon type="rollback"/> Back
            </Link>
            <Divider/>
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={this.state.loading}>
          <Row>
            <Col>
              <Form onSubmit={this.handleSubmit}>
                <FormItem
                  {...formItemLayout}
                  label="Title [en]"
                  hasFeedback
                >
                  {getFieldDecorator('title_en', {
                    initialValue: this.state.title_en,
                    onChange: (e) => this.handleChange(e, 'title_en'),
                  })(
                    <Input placeholder="" name="title_en"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Sub Title [en]"
                  hasFeedback
                >
                  {getFieldDecorator('subtitle_en', {
                    initialValue: this.state.subtitle_en,
                    onChange: (e) => this.handleChange(e, 'subtitle_en'),
                  })(
                    <Input placeholder="" name="subtitle_en"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Title [cn]"
                  hasFeedback
                >
                  {getFieldDecorator('title_cn', {
                    initialValue: this.state.title_cn,
                    onChange: (e) => this.handleChange(e, 'title_cn'),
                  })(
                    <Input placeholder="" name="title_cn"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Sub Title [cn]"
                  hasFeedback
                >
                  {getFieldDecorator('subtitle_cn', {
                    initialValue: this.state.subtitle_cn,
                    onChange: (e) => this.handleChange(e, 'subtitle_cn'),
                  })(
                    <Input placeholder="" name="subtitle_cn"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Position"
                  hasFeedback
                >
                  {getFieldDecorator('position', {
                    initialValue: this.state.position,
                    onChange: (e) => this.handleChange(e, 'position')
                  })(
                    <InputNumber name="position"/>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Banner Image [desktop]"
                  extra="support .jpg , .jpeg , .png only"
                >
                  {getFieldDecorator('banner_image_desktop', {
                    rules: [
                      {required: true, message: 'Please upload your Banner Image!'},
                    ],
                    onChange: (e) => this.handleFileChange(e, 'banner_image_desktop'),
                    initialValue: this.state.banner_image_desktop,
                  })(
                    <Upload {...uploadProps} fileList={this.state.banner_image_desktop}
                            onRemove={ (e) => this.handleFileRemove(e,'banner_image_desktop')}
                            headers={{authorization: `Bearer ${auth.getToken()}`}}>
                      <Button>
                        <Icon type="upload"/> Click to upload
                      </Button>
                    </Upload>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Banner Image [mobile]"
                  extra="support .jpg , .jpeg , .png only"
                >
                  {getFieldDecorator('banner_image_mobile', {
                    rules: [
                      {required: true, message: 'Please upload your Banner Image!'},
                    ],
                    onChange: (e) => this.handleFileChange(e, 'banner_image_mobile'),
                    initialValue: this.state.banner_image_mobile,
                  })(
                    <Upload {...uploadProps} fileList={this.state.banner_image_mobile}
                            onRemove={ (e) => this.handleFileRemove(e,'banner_image_mobile')}
                            headers={{authorization: `Bearer ${auth.getToken()}`}}>
                      <Button>
                        <Icon type="upload"/> Click to upload
                      </Button>
                    </Upload>
                  )}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label="Status"
                  validateStatus=""
                >
                  {getFieldDecorator('status', {
                    valuePropName: 'checked',
                    initialValue: this.state.status,
                    onChange: (e) => this.handleChange(e, 'status')
                  })(
                    <Switch name="status"/>
                  )}
                </FormItem>

                <FormItem
                  wrapperCol={{span: 12, offset: 5}}
                >
                  <Button type="primary" htmlType="submit">Submit</Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  };

  /** handle file actions **/
  handleFileRemove = (e, key) => {
    let fileObj = [];
    fileObj = this.state[key]
    fileObj.map((item,i) => {
      if(item.uid == e.uid){
        fileObj.splice(i, 1);
      }
    });
    this.setState({[key] : fileObj});
  };

  handleFileChange(e, key){
    if (e.fileList != null) {
      if(e.fileList.length > 1){
        e.fileList.map((item,i) => {
          e.fileList.splice(0, 1);
        });
      }
      this.setState({ [key] : e.fileList });
    }
  }
  /** end of handle file actions **/

  handleSubmit = (e) => {
    const {createBanner, editBanner} = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.id = this.state.id;
        values.banner_image_desktop =  getUploadedFileId(values,'banner_image_desktop');
        values.banner_image_mobile = getUploadedFileId(values,'banner_image_mobile');
        if (this.state.isEdit) {
          editBanner(values).then((res) => {
            notification['success']({ message: 'Banner update successful.'});
            this.props.history.push('/admin/banner');
          }).catch((err) => {
            notification['error']({ message: 'Banner update fail.'});
          });
        } else {
          createBanner(values).then((res) => {
            notification['success']({ message: 'Banner create successful.'});
            this.props.history.push('/admin/banner');
          }).catch((err) => {
            notification['error']({ message: 'Banner create fail.'});
          });
        }
      }
    });
  };

  handleChange(e, key) {
    if (e.target != null) {
      this.props.form.setFieldsValue({
        [key]: e.target.value,
      });
    } else {
      this.props.form.setFieldsValue({
        [key]: e,
      });
    }
  }
}

export default Form.create()(BannerEdit);
