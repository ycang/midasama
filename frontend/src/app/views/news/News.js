// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
// import classnames     from 'classnames/bind';
import intl from 'react-intl-universal';
import {HeroBanner, MultiAssetCard} from '../../components/';
import {Row, Col, Carousel, Modal} from 'antd';
import Slicing56 from '../../images/Slicing-56.jpg';
import {isEnUsLocale} from "../../services/utils";
import moment from 'moment';

// IMPORTANT: we need to bind classnames to CSSModule generated classes:
// const cx = classnames.bind(styles);
import cx from 'classnames';

class News extends PureComponent {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterHome: PropTypes.func.isRequired,
    leaveHome: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      isLoading: true,
      modalNewsVisible: false,
      currentNewsModalType: 'text-only', // text-only , video , gallery
      currentNews: ''
    }

  }

  componentDidMount() {
    const {fetchNewsList} = this.props;
    fetchNewsList({isPublished: true});
  }

  componentWillUnmount() {
    const {leaveHome} = this.props;
    leaveHome();
  }

  render() {
    const {news} = this.props;
    return (
      <Row className="misc-news-dots-bg">
        <HeroBanner title={intl.getHTML('news.heroBanner.heading')}
                    description=""
                    imageUrl={Slicing56}/>
        <Col md={24}>
          <div className="container-compact container-vertical">
            <Row gutter={18} className="p-t-50">
              {
                news.list.length > 0 ?
                  news.list.map((n, i) => {
                    return (
                      <Col xs={24} sm={12} lg={6} key={i} data-aos='fade-up'
                           data-aos-delay={(i <= 3) ? i * 300 : 600}>
                        <MultiAssetCard onClickModal={() => this.handleOnClickModal(true, n)}
                                        isVideo={n.video_link ? true : false}
                                        created_at={(n.news_date) ? n.news_date : ''}
                                        coverImage={n.cover_image.filename}
                                        title={(isEnUsLocale()) ? n.title_en : n.title_cn}
                                        description={(isEnUsLocale()) ? n.content_en : n.content_cn}
                        />
                      </Col>
                    )
                  })
                  : ''
              }
            </Row>
          </div>
        </Col>
        <Modal
          title={null}
          wrapClassName="vertical-center-modal modal-global"
          visible={this.state.modalNewsVisible}
          onOk={() => this.setModalNewsVisible(false)}
          onCancel={() => {
            this.setModalNewsVisible(false);
            this.setState({currentNews: []})
          }}
          footer={null}
        >
          <div
            className={cx('bg-white p-20', {'hide': this.state.currentNewsModalType != 'text-only'})}>
            <Row className="border-b-primary">
              <Col md={20} className="border-r-primary"><h4
                className="m-b-5">{(isEnUsLocale()) ? this.state.currentNews.title_en : this.state.currentNews.title_cn}</h4>
              </Col>
              <Col md={4} className="text-right"><h5
                className="text-letterspacing-0 m-0">{ (this.state.currentNews.news_date) ? moment( this.state.currentNews.news_date).format('YYYY-MM-DD') : ''}</h5>
              </Col>
            </Row>
            <Row>
              <Col><p
                className="m-t-10">{(isEnUsLocale()) ? this.state.currentNews.content_en : this.state.currentNews.content_cn}</p>
              </Col>
            </Row>
          </div>
          <div className={cx({'hide': this.state.currentNewsModalType != 'video'})}>
            <iframe width="100%" height="500" src={this.state.currentNews.video_link}
                    frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe>
          </div>
          <div className={cx({'hide': this.state.currentNewsModalType != 'gallery'})}>
            <Carousel arrows={true}>
              {
                this.props.gallery.list.length > 0 ?
                  this.props.gallery.list.map((gallery, i) => {
                    return <div key={i}>
                      <img src={gallery.gallery_image.filename} width="100%"/>
                      <div className="modal-content-wrapper">
                        <Row className="border-b-white">
                          <Col md={20} className="border-r-white"><h4
                            className="text-white m-b-5">{(isEnUsLocale()) ? gallery.title_en : gallery.title_cn}</h4>
                          </Col>
                          <Col md={4} className="text-right"><h5
                            className="text-letterspacing-0 text-white m-0">{ (this.state.currentNews.news_date) ? moment(this.state.currentNews.news_date).format('YYYY-MM-DD') : ''}</h5>
                          </Col>
                        </Row>
                        <Row>
                          <Col><p
                            className="text-white m-t-10">{(isEnUsLocale()) ? gallery.content_en : gallery.content_cn}</p>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  })
                  : ''
              }
            </Carousel>
          </div>
        </Modal>
      </Row>
    );
  }

  setModalNewsVisible(modalNewsVisible) {
    this.setState({modalNewsVisible});
  }

  handleOnClickModal(modalNewsVisible, n) {
    if (n.news_type == 'gallery') {
      this.props.fetchGalleryListByNewsId(n.id);
    }
    this.setState({currentNewsModalType: n.news_type, currentNews: n});
    this.setState({modalNewsVisible});
  }
}

export default News;
