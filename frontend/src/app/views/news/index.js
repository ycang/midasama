// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../redux/modules/views';
import * as newsActions   from '../../redux/modules/news';
import * as galleryActions   from '../../redux/modules/gallery';
import News                   from './News';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    news: state.news,
    gallery: state.gallery,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...newsActions,
      ...galleryActions,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(News);
