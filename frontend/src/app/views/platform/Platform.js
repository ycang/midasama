// @flow weak

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
// import classnames     from 'classnames/bind';
import intl from "react-intl-universal";
import { HeroBanner } from "../../components/";
import { Row, Col, Button } from "antd";
import Slicing08 from "../../images/PrutonCapital.logo-400.png";
import Slicing09 from "../../images/DawedaExchange-Logo.png";
import Slicing16 from "../../images/Slicing-16.png";
import Slicing33 from "../../images/Slicing-33.jpg";
import Slicing35 from "../../images/Slicing-35.png";
import Slicing36 from "../../images/Slicing-36.png";
import Slicing34 from "../../images/Slicing-34.png";
import Slicing341 from "../../images/Slicing-34-1.png";

// IMPORTANT: we need to bind classnames to CSSModule generated classes:
// const cx = classnames.bind(styles);

class Platform extends PureComponent {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterHome: PropTypes.func.isRequired,
    leaveHome: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {}

  componentWillUnmount() {
    const { leaveHome } = this.props;
    leaveHome();
  }

  render() {
    const {} = this.props;
    return (
      <div>
        <HeroBanner
          title={intl.getHTML("platform.heroBanner.heading")}
          description=""
          imageUrl={Slicing33}
        />
        <div className="bg-white">
          <div className="container-compact misc-platform-pro-section">
            <Row>
              <Col md={16}>
                <h1 className="text-madison" data-aos="fade-up">
                  <b>{intl.getHTML("platform.section1.heading")}</b>
                </h1>
                <p data-aos="fade-up" data-aos-delay="300">
                  {intl.getHTML("platform.section1.subheading")}
                </p>
              </Col>
            </Row>
            <Row className="p-t-60">
              <Col md={6} data-aos="fade-right">
                <h4 className="text-madison text-letterspacing-0">
                  METATRADER 4
                </h4>
                <h1 className="text-madison text-lineheight-1">
                  <b>{intl.getHTML("platform.advantage.heading")}</b>
                </h1>
              </Col>
              <Col md={5} data-aos="fade-left" data-aos-delay="300">
                <ul className="custom-list-style">
                  <li>
                    <p>{intl.getHTML("platform.advantage.point1")}</p>
                  </li>
                  <li>
                    <p>{intl.getHTML("platform.advantage.point2")}</p>
                  </li>
                  <li>
                    <p>{intl.getHTML("platform.advantage.point3")}</p>
                  </li>
                </ul>
              </Col>
              <Col md={5} data-aos="fade-left" data-aos-delay="600">
                <ul className="custom-list-style">
                  <li>
                    <p>{intl.getHTML("platform.advantage.point4")}</p>
                  </li>
                  <li>
                    <p>{intl.getHTML("platform.advantage.point5")}</p>
                  </li>
                  <li>
                    <p>{intl.getHTML("platform.advantage.point6")}</p>
                  </li>
                </ul>
              </Col>
              <Col md={7} data-aos="fade-left" data-aos-delay="900">
                <ul className="custom-list-style">
                  <li>
                    <p>{intl.getHTML("platform.advantage.point7")}</p>
                  </li>
                  <li>
                    <p>{intl.getHTML("platform.advantage.point8")}</p>
                  </li>
                  <li>
                    <p>{intl.getHTML("platform.advantage.point9")}</p>
                  </li>
                </ul>
              </Col>
            </Row>
          </div>
        </div>
        <Row className="bg-white misc-platform-middle-section">
          <div className="container-compact">
            <Col sm={16} />
            <Col sm={8} data-aos="fade-up" data-aos-delay="900">
              <p>{intl.getHTML("platform.section2.subheading")}</p>
            </Col>
          </div>
        </Row>
        <Row className="bg-shark misc-platform-device-section">
          <img
            src={Slicing16}
            className="misc-platform-computer"
            data-aos="fade-up"
            data-aos-delay="1200"
          />
          <div className="container-compact">
            <Col xs={24} className="text-center p-b-60">
              <h1 className="text-primary" data-aos="fade-up">
                {intl.getHTML("platform.section3.heading")}
              </h1>
            </Col>
            <Col md={12}>
              <div className="misc-platform__device-container">
                <div className="text-center m-b-10">
                  <img
                    src={Slicing34}
                    data-aos="fade-up"
                    width="100%"
                    style={{ maxWidth: 392 }}
                  />
                </div>
                <div className="text-center">
                  <h4 className="text-white m-b-35" data-aos="fade-up">
                    <b>{intl.getHTML("platform.desktop.heading")}</b>
                  </h4>
                  <p data-aos="fade-up" className="text-white text-left">
                    {intl.getHTML("platform.desktop.subheading")}
                  </p>
                  <br />
                  <br />
                  <br />
                </div>
                <ul className="custom-list-style misc-platform-device-list text-white">
                  <li data-aos="fade-left" data-aos-delay="300">
                    {intl.getHTML("platform.desktop.point1")}
                  </li>
                  <li data-aos="fade-left" data-aos-delay="600">
                    {intl.getHTML("platform.desktop.point2")}
                  </li>
                  <li data-aos="fade-left" data-aos-delay="900">
                    {intl.getHTML("platform.desktop.point3")}
                  </li>
                  <li data-aos="fade-left" data-aos-delay="1000">
                    {intl.getHTML("platform.desktop.point4")}
                  </li>
                </ul>
                <br />
                <Row gutter={18} className="m-b-60">
                  <Col
                    xs={24}
                    md={0}
                    data-aos="fade-up"
                    data-aos-delay="600"
                    className="m-b-30 text-center"
                  >
                    <div className="m-b-10 misc-platform-download-btn">
                      <img src={Slicing08} width="160" />
                    </div>
                    <Button
                      target="_blank"
                      type="primary"
                      href="http://prutoncapital.com/metatrader-4-client-terminal-pc"
                      icon="download"
                      data-aos="fade-up"
                      data-aos-delay="600"
                    >
                      {intl.getHTML("platform.desktop.button.download")}
                    </Button>
                  </Col>
                  <Col
                    xs={24}
                    md={0}
                    data-aos="fade-up"
                    data-aos-delay="600"
                    className="text-center"
                  >
                    <div className="m-b-10 misc-platform-download-btn">
                      <img src={Slicing09} width="130" />
                    </div>
                    <Button
                      target="_blank"
                      type="primary"
                      href="http://dawedafx.com/metatrader4"
                      icon="download"
                      data-aos="fade-up"
                      data-aos-delay="600"
                    >
                      {intl.getHTML("platform.desktop.button.download2")}
                    </Button>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col md={12}>
              <div className="misc-platform__device-container">
                <div className="text-center m-b-10">
                  <img
                    src={Slicing341}
                    data-aos="fade-up"
                    width="100%"
                    style={{ maxWidth: 231 }}
                  />
                </div>
                <div className="text-center">
                  <h4 className="text-white m-b-35" data-aos="fade-up">
                    <b>{intl.getHTML("platform.mobile.heading")}</b>
                  </h4>
                  <p data-aos="fade-up" className="text-white text-left">
                    {intl.getHTML("platform.mobile.subheading")}
                  </p>
                  <br />
                  <br />
                  <br />
                </div>
                <ul className="custom-list-style misc-platform-device-list text-white">
                  <li data-aos="fade-left" data-aos-delay="300">
                    {intl.getHTML("platform.mobile.point1")}
                  </li>
                  <li data-aos="fade-left" data-aos-delay="600">
                    {intl.getHTML("platform.mobile.point2")}
                  </li>
                  <li data-aos="fade-left" data-aos-delay="900">
                    {intl.getHTML("platform.mobile.point3")}
                  </li>
                  <li data-aos="fade-left" data-aos-delay="1000">
                    {intl.getHTML("platform.mobile.point4")}
                  </li>
                </ul>
                <br />
                <Row gutter={18}>
                  <Col
                    xs={24}
                    md={0}
                    data-aos="fade-up"
                    data-aos-delay="600"
                    className="m-b-30 text-center"
                  >
                    <div>
                      <img
                        src={Slicing35}
                        width="70"
                        className="p-r-10 p-b-10"
                      />
                    </div>
                    <Button
                      target="_blank"
                      href="https://play.google.com/store/apps/details?id=net.metaquotes.metatrader4"
                      type="primary"
                      icon="download"
                    >
                      {intl.getHTML("platform.mobile.button.download")}
                    </Button>
                  </Col>
                  <Col
                    xs={24}
                    md={0}
                    data-aos="fade-up"
                    data-aos-delay="600"
                    className="text-center"
                  >
                    <div>
                      <img
                        src={Slicing36}
                        width="70"
                        className="p-r-10 p-b-10"
                      />
                    </div>
                    <Button
                      type="primary"
                      target="_blank"
                      href="https://itunes.apple.com/us/app/metatrader-4/id496212596"
                      icon="download"
                    >
                      {intl.getHTML("platform.mobile.button.download2")}
                    </Button>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xs={0} md={24} data-aos-delay="300">
              <Row type="flex" justify="space-between" align="bottom">
                <Col xs={24} md={6} data-aos="fade-up" className="text-center">
                  <div className="m-b-10 misc-platform-download-btn">
                    <img src={Slicing08} width="160" />
                  </div>
                  <Button
                    target="_blank"
                    type="primary"
                    href="http://prutoncapital.com/metatrader-4-client-terminal-pc"
                    icon="download"
                  >
                    {intl.getHTML("platform.desktop.button.download")}
                  </Button>
                </Col>
                <Col xs={24} md={6} data-aos="fade-up" className="text-center">
                  <div className="m-b-10 misc-platform-download-btn">
                    <img src={Slicing09} width="130" />
                  </div>
                  <Button
                    target="_blank"
                    type="primary"
                    href="https://download.mql5.com/cdn/web/12908/mt4/dawedalimited4setup.exe"
                    icon="download"
                  >
                    {intl.getHTML("platform.desktop.button.download2")}
                  </Button>
                </Col>
                <Col xs={24} md={6} data-aos="fade-up" className="text-center">
                  <div className="m-b-10 misc-platform-download-btn">
                    <img src={Slicing35} width="70" />
                  </div>
                  <Button
                    target="_blank"
                    href="https://play.google.com/store/apps/details?id=net.metaquotes.metatrader4"
                    type="primary"
                    icon="download"
                  >
                    {intl.getHTML("platform.mobile.button.download")}
                  </Button>
                </Col>
                <Col xs={24} md={6} data-aos="fade-up" className="text-center">
                  <div className="m-b-10 misc-platform-download-btn">
                    <img src={Slicing36} width="70" />
                  </div>
                  <Button
                    type="primary"
                    target="_blank"
                    href="https://itunes.apple.com/us/app/metatrader-4/id496212596"
                    icon="download"
                  >
                    {intl.getHTML("platform.mobile.button.download2")}
                  </Button>
                </Col>
              </Row>
            </Col>
          </div>
        </Row>
      </div>
    );
  }
}

export default Platform;
