// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
// import classnames     from 'classnames/bind';
import {Row, Col, Carousel, Button, Tabs} from 'antd';
import intl from 'react-intl-universal';
import {Link} from 'react-router-dom';
import {HeroBanner} from '../../components/';
import Slicing32 from '../../images/Slicing-32.png';
import homepageBanner01 from '../../images/homepage-banner-01.png';
import homepageBanner02 from '../../images/homepage-banner-02.png';
import homepageBanner03 from '../../images/homepage-banner-03.png';
import Slicing31 from '../../images/Slicing-31.jpg';
import {isEnUsLocale} from "../../services/utils";

import AOS from 'aos';
const TabPane = Tabs.TabPane;
// IMPORTANT: we need to bind classnames to CSSModule generated classes:
// const cx = classnames.bind(styles);

class Products extends PureComponent {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterHome: PropTypes.func.isRequired,
    leaveHome: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      isLoading: true
    }

  }

  componentDidMount() {
    const {fetchProductList} = this.props;
    fetchProductList({isPublished: true}).then((res) => this.setState({loading: false})).catch((res) => this.setState({loading: false}));
  }

  componentWillUnmount() {
    const {leaveHome} = this.props;
    leaveHome();
  }

  render() {
    const {} = this.props;
    return (
      <div>
        <HeroBanner title={intl.getHTML('product.heroBanner.heading')}
                    description=""
                    imageUrl={Slicing31}/>
        <Row className="bg-athens-gray misc-product-description-section misc-product-dots-bg">
          <div className="container-compact">
            <Col md={7} data-aos='fade-up' data-aos-delay="300">
              <h2 className="text-madison">{intl.getHTML('product.section1.heading')}</h2>
            </Col>
            <Col md={17} data-aos='fade-up'>
              <h5 className="border-l-primary p-l-20">{intl.getHTML('product.section1.description')}</h5>
            </Col>
          </div>
        </Row>
        <Row>
          <Col>
            <Tabs defaultActiveKey="0" className="product-tab" animated={false}>
              {
                this.props.product.list.length > 0 ?
                  this.props.product.list.map((product,i) => {
                    return (

                      <TabPane tab={(isEnUsLocale()) ? product.product_name_en : product.product_name_cn} key={i}  style={{'backgroundImage': `url('${product.cover_image.filename}')`}}
                               className="product-tab-product-bg">
                      <Row style={{'backgroundImage': `url('${Slicing32}')`}} className="product-tab-right-content-wrapper">
                        <Col md={10}>
                        </Col>
                        <Col md={12}>
                          <div className="product-tab-right-copy-wrapper">
                            <h2 className="text-primary">{(isEnUsLocale()) ? product.title_en : product.title_cn}</h2>
                            <h5 className="text-white">{(isEnUsLocale()) ? product.content_en : product.content_cn}</h5>
                          </div>
                        </Col>
                      </Row>
                    </TabPane>)
                  })
                  : ''
              }
            </Tabs>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Products;
