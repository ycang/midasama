// @flow weak

import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
// import classnames     from 'classnames/bind';
import intl from 'react-intl-universal';
import cx from 'classnames';
import {HeroBanner} from '../../components/';
import {isEnUsLocale} from "../../services/utils";
import {Row, Col, Button, Form, Input, Carousel, Divider, Icon} from 'antd';
import Slicing59 from '../../images/Slicing-59.jpg';
import elitebizLogo from '../../images/Slicing-60.png';
import Slicing68 from '../../images/Slicing-68.png';
import Slicing63 from '../../images/Slicing-63.png';
import Slicing64 from '../../images/Slicing-64.png';
import Slicing65 from '../../images/Slicing-65.png';
import Slicing66 from '../../images/Slicing-66.png';
import Slicing67 from '../../images/Slicing-67.png';
import Slicing78 from '../../images/Slicing-78.png';
// animation 1
import A1P1 from '../../images/animation-1/animation-1-preview.png';
import A1S1 from '../../images/animation-1/animation-slicing-00.png';
import A1S2 from '../../images/animation-1/animation-slicing-01.png';
import A1S3 from '../../images/animation-1/animation-slicing-02.png';
import A1S4 from '../../images/animation-1/animation-slicing-03.png';
import A1S5 from '../../images/animation-1/animation-slicing-04.png';
import A1S6 from '../../images/animation-1/animation-slicing-05.png';
import A1S7 from '../../images/animation-1/animation-slicing-06.png';
import A1S8 from '../../images/animation-1/animation-slicing-07.png';

// animation2
import A2P1 from '../../images/animation-2/animation-2-preview.png';
import A2S2 from '../../images/animation-2/animation-slicing-08.png';
import A2S3 from '../../images/animation-2/animation-slicing-09.png';
import A2S4 from '../../images/animation-2/animation-slicing-10.png';
import A2S5 from '../../images/animation-2/animation-slicing-11.png';
import A2S6 from '../../images/animation-2/animation-slicing-12.png';

// animation3
import A3P1 from '../../images/animation-3/animation-3-preview.png';
import A3S1 from '../../images/animation-3/animation-slicing-13.png';
import A3S2 from '../../images/animation-3/animation-slicing-14.png';
import A3S3 from '../../images/animation-3/animation-slicing-15.png';
import A3S4 from '../../images/animation-3/animation-slicing-16.png';
import A3S5 from '../../images/animation-3/animation-slicing-17.png';
import A3S6 from '../../images/animation-3/animation-slicing-18.png';
import A3S7 from '../../images/animation-3/animation-slicing-19.png';

// animation4
import A4P1 from '../../images/animation-4/animation-4-preview.png';
import A4S1 from '../../images/animation-4/animation-slicing-20.png';
import A4S2 from '../../images/animation-4/animation-slicing-21.png';
import A4S3 from '../../images/animation-4/animation-slicing-22.png';
import A4S4 from '../../images/animation-4/animation-slicing-23.png';
import A4S5 from '../../images/animation-4/animation-slicing-24.png';
import anime from 'animejs';
import ScrollMagic from '../../services/utils/ScrollMagic';
import {TimelineLite, TweenMax, Sine, Expo} from "gsap/TweenMax";

const FormItem = Form.Item;

// IMPORTANT: we need to bind classnames to CSSModule generated classes:
// const cx = classnames.bind(styles);

class Academy extends Component {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterHome: PropTypes.func.isRequired,
    leaveHome: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      currentCourse: 'course-1',
      first_name: '',
      last_name: '',
      contact_no: '',
      email: '',
      nationality: '',
      isLoading: true,
      courseTween1: new TimelineLite(),
      courseTween2: new TimelineLite(),
      courseTween3: new TimelineLite(),
      courseTween4: new TimelineLite(),
      courseTween5: new TimelineLite(),
      responseMessage: ''
    }

  }

  componentDidMount() {
    this.animation1();
    this.animation2();
    this.animation3();
    this.animation4();
    this.animation5();
  }

  animation5() {
    var animation = this.state.courseTween5
      .from($('.js-learn-1'), 1, {opacity: 0, scale: 0, x: -200, y: -200, ease: Expo.easeOut})
      .from($('.js-learn-1-text'), 1, {opacity: 0})
      .from($('.js-learn-2'), 1, {opacity: 0, scale: 0, x: -200, y: -200, ease: Expo.easeOut}, 0.5)
      .from($('.js-learn-2-text'), 1, {opacity: 0})
      .from($('.js-learn-3'), 1, {opacity: 0, scale: 0, x: 0, y: -200, ease: Expo.easeOut}, 1)
      .from($('.js-learn-3-text'), 1, {opacity: 0})
      .from($('.js-learn-4'), 1, {opacity: 0, scale: 0, x: 200, y: -200, ease: Expo.easeOut}, 1.5)
      .from($('.js-learn-4-text'), 1, {opacity: 0})
      .from($('.js-learn-5'), 1, {opacity: 0, scale: 0, x: 200, y: -200, ease: Expo.easeOut}, 2)
      .from($('.js-learn-5-text'), 1, {opacity: 0});

    $('.js-learn').mouseover(function (e) {
      new TweenMax.to($(this), .2, {scale: 1.2});
    })
    $('.js-learn').mouseout(function (e) {
      new TweenMax.to($(this), .2, {scale: 1});
    });
    const controller = new ScrollMagic.Controller();
    new ScrollMagic.Scene({
      triggerElement: '#js-academy-learn-scroll'
    })
      .setTween(animation)
      .addTo(controller);
  }

  animateFloat(selector, duration, y) {
    var timelineParameters = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters
      .add({
        targets: selector,
        translateY: [{value: -y}, {value: y}],
        elasticity: 200,
        duration: duration
      });
  }

  animation1() {
    this.animateFloat('.js-course-1-logo', 1500, 8);
    this.animateFloat('.js-course-1-pie', 4000, 10);
    const academyCourse = this.state.courseTween1
      .fromTo($('.js-course-1-stage'), .7, {x: 50, y: -50}, {opacity: 1, x: 0, y: 0})
      .fromTo($('.js-course-1-people'), .7, {opacity: 0, x: -50, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      }, 0.5)
      .fromTo($('.js-course-1-logo'), .7, {opacity: 0}, {opacity: 1}, 1)
      .fromTo($('.js-course-1-graph'), .7, {opacity: 0, x: 0, y: 50}, {opacity: 1, x: 0, y: 0}, 1.5)
      .fromTo($('.js-course-1-graph-shadow-2'), .7, {opacity: 0, x: 0, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      }, 2)
      .fromTo($('.js-course-1-graph-shadow-1'), .7, {opacity: 0, x: 0, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      }, 2.5)
      .fromTo($('.js-course-1-floor-light'), .7, {opacity: 0, x: -50, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      })
      .fromTo($('.js-course-1-pie'), .7, {opacity: 0}, {opacity: 1});

    const controller = new ScrollMagic.Controller();
    new ScrollMagic.Scene({
      triggerElement: '#js-academy-course-scroll'
    })
      .setTween(academyCourse)
      .addTo(controller);
  }

  animation2() {
    this.animateFloat('.js-course-2-logo', 1500, 8);
    this.state.courseTween2
      .fromTo($('.js-course-2-stage'), .7, {x: 50, y: -50}, {opacity: 1, x: 0, y: 0})
      .fromTo($('.js-course-2-student'), .7, {opacity: 0, x: -50, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      }, 0.5)
      .fromTo($('.js-course-2-teacher'), .7, {opacity: 0}, {opacity: 1}, 1)
      .fromTo($('.js-course-2-logo'), .7, {opacity: 0}, {opacity: 1}, 1)
      .fromTo($('.js-course-2-visitor'), .7, {opacity: 0}, {opacity: 1}, 1.5)
      .fromTo($('.js-course-2-floor-light'), .7, {opacity: 0, x: -50, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      });
  }

  animation3() {
    this.state.courseTween3
      .fromTo($('.js-course-3-floor'), .7, {opacity: 0}, {opacity: 1})
      .fromTo($('.js-course-3-table'), .7, {opacity: 0, x: -50, y: -50}, {
        opacity: 1,
        x: 0,
        y: 0
      }, 0.5)
      .fromTo($('.js-course-3-teacher'), .7, {opacity: 0}, {opacity: 1})
      .fromTo($('.js-course-3-graph-1'), .7, {opacity: 0}, {opacity: 1})
      .fromTo($('.js-course-3-graph-2'), .7, {opacity: 0}, {opacity: 1},1.8)
      .fromTo($('.js-course-3-graph-3'), .7, {opacity: 0}, {opacity: 1})
      .fromTo($('.js-course-3-graph-4'), .7, {opacity: 0}, {opacity: 1},2.3)
      .fromTo($('.js-course-3-floor-light'), .7, {opacity: 0, x: -50, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      });
  }

  animation4() {
    this.state.courseTween4
      .fromTo($('.js-course-4-floor'), .7, {x: 0, y: 50}, {opacity: 1, x: 0, y: 0})
      .fromTo($('.js-course-4-wave'), .7, {opacity: 0, x: -50, y: -50}, {
        opacity: 1,
        x: 0,
        y: 0
      }, 0.5)
      .fromTo($('.js-course-4-up'), .7, {opacity: 0, x: 0, y: 50}, {opacity: 1, x: 0, y: 0}, 1)
      .fromTo($('.js-course-4-dollar'), .7, {opacity: 0}, {opacity: 1}, 1)
      .fromTo($('.js-course-4-coin'), .7, {opacity: 0}, {opacity: 1}, 1.5)
      .fromTo($('.js-course-4-floor-light'), .7, {opacity: 0, x: -50, y: 50}, {
        opacity: 1,
        x: 0,
        y: 0
      });
  }

  componentWillUnmount() {
    const {leaveHome} = this.props;
    leaveHome();
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    return (
      <div>
        <HeroBanner title={intl.getHTML('academy.heroBanner.heading')}
                    description=""
                    imageUrl={Slicing59}/>
        <Col md={24}>
          <div className="container-compact container-vertical">
            <Row>
              <Col md={7} className="text-center">
                <img src={elitebizLogo} data-aos='fade-up' data-aos-delay="300"
                     className="misc-academy-elitebiz-logo p-b-15 p-r-20 p-l-15" width="100%"/>
              </Col>
              <Col md={17}>
                <h1 className="text-madison academy-section1-heading"
                    data-aos='fade-up'>{intl.getHTML('academy.section1.heading')}</h1>
                <p className="border-l-primary p-l-20" data-aos='fade-up'
                   data-aos-delay="300"
                   style={{maxWidth: 880}}>{intl.getHTML('academy.section1.subheading')}</p>
                <p className="border-l-primary p-l-20 m-t-60 p-b-20" data-aos='fade-up'
                   data-aos-delay="600"
                   style={{maxWidth: 880}}>{intl.getHTML('academy.section2.subheading')}</p>

                  <Button
                  type="primary" data-aos='fade-up' data-aos-delay='300'
                  target="_blank"
                  href={ isEnUsLocale() ? 'https://elitebiz.academy/en/' : 'https://elitebiz.academy/wpebcn' }>
                      {intl.getHTML('home.section8.cta2')}<Icon type="caret-right"/>
                      </Button>
              </Col>
            </Row>
          </div>
          <div className="misc-academy-course-section" id="js-academy-course-scroll">
            <div className="container-compact container-vertical">
              <Row>
                <Col xs={24} lg={0}>
                  <h2 className="text-primary text-center"
                      data-aos='fade-up'>{intl.getHTML('academy.course.heading')}</h2>
                  <Carousel autoplay autoplaySpeed={10000}>
                    <div>
                      <img src={A1P1} width="100%" className={"academy-course-mobile-image"}/>
                      <p
                        className="h4 text-primary m-t-20 m-b-0">{intl.getHTML('academy.course.item1.heading')}</p>
                      <p
                        className="text-white m-t-10 m-b-0">{intl.getHTML('academy.course.item1.subheading')}</p>
                    </div>
                    <div>
                      <img src={A2P1} width="100%" className={"academy-course-mobile-image"}/>
                      <p
                        className="h4 text-primary m-t-20 m-b-0">{intl.getHTML('academy.course.item2.heading')}</p>
                      <p
                        className="text-white m-t-10 m-b-0">{intl.getHTML('academy.course.item2.subheading')}</p>
                    </div>
                    <div>
                      <img src={A3P1} width="100%" className={"academy-course-mobile-image"}/>
                      <p
                        className="h4 text-primary m-t-20 m-b-0">{intl.getHTML('academy.course.item3.heading')}</p>
                      <p
                        className="text-white m-t-10 m-b-0">{intl.getHTML('academy.course.item3.subheading')}</p>
                    </div>
                    <div>
                      <img src={A4P1} width="100%" className={"academy-course-mobile-image"}/>
                      <p
                        className="h4 text-primary m-t-20 m-b-0">{intl.getHTML('academy.course.item4.heading')}</p>
                      <p
                        className="text-white m-t-10 m-b-0">{intl.getHTML('academy.course.item4.subheading')}</p>
                    </div>
                  </Carousel>
                </Col>
                <Col xs={0} lg={24}>
                  <h2 className="text-primary"
                      data-aos='fade-up'>{intl.getHTML('academy.course.heading')}</h2>
                  <Row>
                    <Col md={16}>
                      <div
                        className={cx('js-course-1 course-1', {'hide': this.state.currentCourse != 'course-1'})}>
                        <img src={A1S2} className="course-animate js-course-1-graph"/>
                        <img src={A1S3} className="course-animate js-course-1-stage"/>
                        <img src={A1S4} className="course-animate js-course-1-pie"/>
                        <img src={A1S5} className="course-animate js-course-1-people "/>
                        <img src={A1S6} className="course-animate js-course-1-logo course-1-logo"/>
                        <img src={A1S7}
                             className="course-animate js-course-1-graph-shadow-1 course-1-graph-shadow-1"/>
                        <img src={A1S8}
                             className="course-animate js-course-1-graph-shadow-2 course-1-graph-shadow-2"/>
                        <img src={A1S1}
                             className="course-animate js-course-floor-light course-floor-light"/>
                      </div>
                      <div
                        className={cx('js-course-2 course-2', {'hide': this.state.currentCourse != 'course-2'})}>
                        <img src={A2S2} className="course-animate js-course-2-stage"/>
                        <img src={A2S3} className="course-animate js-course-2-teacher"/>
                        <img src={A2S4} className="course-animate js-course-2-student"/>
                        <img src={A2S5}
                             className="course-animate js-course-2-visitor course-2-visitor"/>
                        <img src={A2S6} className="course-animate js-course-2-logo"/>
                        <img src={A1S1}
                             className="course-animate js-course-2-floor-light course-floor-light"/>
                      </div>
                      <div
                        className={cx('js-course-3 course-3', {'hide': this.state.currentCourse != 'course-3'})}>
                        <img src={A3S1} className="course-animate js-course-3-floor"/>
                        <img src={A3S2} className="course-animate js-course-3-table"/>
                        <img src={A3S3} className="course-animate js-course-3-teacher"/>
                        <img src={A3S4} className="course-animate js-course-3-graph-1"/>
                        <img src={A3S5} className="course-animate js-course-3-graph-2"/>
                        <img src={A3S6} className="course-animate js-course-3-graph-3"/>
                        <img src={A3S7} className="course-animate js-course-3-graph-4"/>
                        <img src={A1S1}
                             className="course-animate js-course-3-floor-light course-floor-light"/>
                      </div>
                      <div
                        className={cx('js-course-4 course-4', {'hide': this.state.currentCourse != 'course-4'})}>
                        <img src={A4S1} className="course-animate js-course-4-floor"/>
                        <img src={A4S2} className="course-animate js-course-4-wave"/>
                        <img src={A4S3} className="course-animate js-course-4-up"/>
                        <img src={A4S4}
                             className="course-animate course-4-dollar js-course-4-dollar"/>
                        <img src={A4S5} className="course-animate js-course-4-coin"/>
                        <img src={A1S1}
                             className="course-animate js-course-4-floor-light course-floor-light"/>
                      </div>
                    </Col>
                    <Col md={8}>
                      <Row type="flex" align="top"
                           onClick={() => this.setCurrentCourse('course-1')}
                           className={cx('academy-course-step', {'active': this.state.currentCourse == 'course-1'})}
                      >
                        <Col xs={2}><p className="h1 academy-course-numbering m-0">1</p></Col>
                        <Col xs={22}><p
                          className="academy-course-heading h4 m-0">{intl.getHTML('academy.course.item1.heading')}</p>
                          <p
                            className="academy-course-subheading m-0">{intl.getHTML('academy.course.item1.subheading')}</p>
                        </Col>
                      </Row>
                      <Divider style={{margin: '5px 0'}}/>
                      <Row type="flex" align="top"
                           onClick={() => this.setCurrentCourse('course-2')}
                           className={cx('academy-course-step', {'active': this.state.currentCourse == 'course-2'})}
                      >
                        <Col xs={2}><p className="h1 academy-course-numbering m-0">2</p></Col>
                        <Col xs={22}><p
                          className="academy-course-heading h4 m-0">{intl.getHTML('academy.course.item2.heading')}</p>
                          <p
                            className="academy-course-subheading m-0">{intl.getHTML('academy.course.item2.subheading')}</p>
                        </Col>
                      </Row>
                      <Divider style={{margin: '5px 0'}}/>
                      <Row type="flex" align="top"
                           onClick={() => this.setCurrentCourse('course-3')}
                           className={cx('academy-course-step', {'active': this.state.currentCourse == 'course-3'})}
                      >
                        <Col xs={2}><p className="h1 academy-course-numbering m-0">3</p></Col>
                        <Col xs={22}><p
                          className="academy-course-heading h4 m-0">{intl.getHTML('academy.course.item3.heading')}</p>
                          <p
                            className="academy-course-subheading m-0">{intl.getHTML('academy.course.item3.subheading')}</p>
                        </Col>
                      </Row>
                      <Divider style={{margin: '5px 0'}}/>
                      <Row type="flex" align="top"
                           onClick={() => this.setCurrentCourse('course-4')}
                           className={cx('academy-course-step', {'active': this.state.currentCourse == 'course-4'})}
                      >
                        <Col xs={2}><p className="h1 academy-course-numbering m-0">4</p></Col>
                        <Col xs={22}><p
                          className="academy-course-heading h4 m-0">{intl.getHTML('academy.course.item4.heading')}</p>
                          <p
                            className="academy-course-subheading m-0">{intl.getHTML('academy.course.item4.subheading')}</p>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
          <div className="container-compact container-vertical">
            <Row className="text-center">
              <Col md={8} data-aos='fade-up'>
                <img src={Slicing78} className="m-b-20"/>
                <h4 className="text-madison">{intl.getHTML('academy.point1.heading')}</h4>
                <p className="m-b-40">{intl.getHTML('academy.point1.subheading')}</p>
              </Col>
              <Col md={8} data-aos='fade-up' data-aos-delay="300">
                <img src={Slicing78} className="m-b-20"/>
                <h4 className="text-madison">{intl.getHTML('academy.point2.heading')}</h4>
                <p className="m-b-40">{intl.getHTML('academy.point2.subheading')}</p>
              </Col>
              <Col md={8} data-aos='fade-up' data-aos-delay="600">
                <img src={Slicing78} className="m-b-20"/>
                <h4 className="text-madison">{intl.getHTML('academy.point3.heading')}</h4>
                <p>{intl.getHTML('academy.point3.subheading')}</p>
              </Col>
            </Row>
          </div>
          <Row>
            <Col xs={24} lg={0}>
              <div className="misc-academy-learn-mobile-section">
                <h2
                  className="text-primary text-center">{intl.getHTML('academy.learn.heading')}<br/>{intl.getHTML('academy.learn.subheading')}
                </h2>
                <br/>
                <Row className="misc-academy-learn-mobile-content">
                  <Col xs={12} className="p-l-10 p-r-10 text-center m-b-20">
                    <div className="academy-learn-mobile-content" data-aos='fade-up'>
                      <img src={Slicing63} className="academy-learn-image"/>
                      <p
                        className="text-white m-t-15">{intl.getHTML('academy.learn.item1.heading')}</p>
                    </div>
                  </Col>
                  <Col xs={12} className="p-l-10 p-r-10 text-center m-b-20">
                    <div className="academy-learn-mobile-content" data-aos='fade-up'>
                      <img src={Slicing64} className="academy-learn-image"/>
                      <p
                        className="text-white m-t-15">{intl.getHTML('academy.learn.item2.heading')}</p>
                    </div>
                  </Col>
                  <Col xs={12} className="p-l-10 p-r-10 text-center m-b-20">
                    <div className="academy-learn-mobile-content" data-aos='fade-up' data-aos-delay="300">
                      <img src={Slicing65} className="academy-learn-image"/>
                      <p
                        className="text-white m-t-15">{intl.getHTML('academy.learn.item3.heading')}</p>
                    </div>
                  </Col>
                  <Col xs={12} className="p-l-10 p-r-10 text-center m-b-20">
                    <div className="academy-learn-mobile-content" data-aos='fade-up' data-aos-delay="300">
                      <img src={Slicing66} className="academy-learn-image"/>
                      <p
                        className="text-white m-t-15">{intl.getHTML('academy.learn.item4.heading')}</p>
                    </div>
                  </Col>
                  <Col xs={24} className="p-l-10 p-r-10 text-center m-b-20">
                    <div className="academy-learn-mobile-content" data-aos='fade-up' data-aos-delay="600">
                      <img src={Slicing67} className="academy-learn-image"/>
                      <p
                        className="text-white m-t-15">{intl.getHTML('academy.learn.item5.heading')}</p>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xs={0} lg={24}>
              <div className="misc-academy-learn-section" id="js-academy-learn-scroll">
                <div className="academy-learn academy-learn-1 text-center js-learn-1 js-learn">
                  <img src={Slicing63} className="academy-learn-image"/>
                  <p
                    className="text-white m-t-15 js-learn-1-text">{intl.getHTML('academy.learn.item1.heading')}</p>
                </div>
                <div className="academy-learn academy-learn-2 text-center js-learn-2 js-learn">
                  <img src={Slicing64} className="academy-learn-image"/>
                  <p
                    className="text-white m-t-15 js-learn-2-text">{intl.getHTML('academy.learn.item2.heading')}</p>
                </div>
                <div className="academy-learn academy-learn-3 text-center js-learn-3 js-learn">
                  <img src={Slicing65} className="academy-learn-image"/>
                  <p
                    className="text-white m-t-15 js-learn-3-text">{intl.getHTML('academy.learn.item3.heading')}</p>
                </div>
                <div className="academy-learn academy-learn-4 text-center js-learn-4 js-learn">
                  <img src={Slicing66} className="academy-learn-image"/>
                  <p
                    className="text-white m-t-15 js-learn-4-text">{intl.getHTML('academy.learn.item4.heading')}</p>
                </div>
                <div className="academy-learn academy-learn-5 text-center js-learn-5 js-learn">
                  <img src={Slicing67} className="academy-learn-image"/>
                  <p
                    className="text-white m-t-15 js-learn-5-text">{intl.getHTML('academy.learn.item5.heading')}</p>
                </div>
                <div className="misc-academy-learn-heading-container">
                  <h2
                    className="text-primary">{intl.getHTML('academy.learn.heading')}<br/>{intl.getHTML('academy.learn.subheading')}
                  </h2>
                </div>
              </div>
            </Col>
          </Row>
          <div className="misc-academy-form-section">
            <div className="container-compact container-vertical">
              <Row className="text-center">
                <Col><h1 className="text-madison"
                         data-aos='fade-up'>{intl.getHTML('academy.form.heading')}</h1><br/></Col>
              </Row>
              <Row>
                <Col>
                  <p className="text-center text-primary">{this.state.responseMessage}</p>
                  <Form onSubmit={this.handleSubmit} style={{maxWidth: 500, margin: '0 auto'}}
                        className="form--academy" data-aos='fade-up' data-aos-delay="600">
                    <FormItem
                      hasFeedback
                    >
                      {getFieldDecorator('first_name', {
                        rules: [
                          {required: true, message: intl.get('academy.form.firstname.error')},
                        ],
                        initialValue: this.state.first_name,
                        onChange: (e) => this.handleChange(e, 'first_name'),
                      })(
                        <Input placeholder={intl.get('academy.form.firstname')}/>
                      )}
                    </FormItem>

                    <FormItem
                      hasFeedback
                    >
                      {getFieldDecorator('last_name', {
                        rules: [
                          {required: true, message: intl.get('academy.form.lastname.error')},
                        ],
                        initialValue: this.state.last_name,
                        onChange: (e) => this.handleChange(e, 'last_name'),
                      })(
                        <Input placeholder={intl.get('academy.form.lastname')}/>
                      )}
                    </FormItem>

                    <FormItem
                      hasFeedback
                    >
                      {getFieldDecorator('contact_no', {
                        rules: [
                          {required: true, message: intl.get('academy.form.contactno.error')},
                        ],
                        initialValue: this.state.contact_no,
                        onChange: (e) => this.handleChange(e, 'contact_no'),
                      })(
                        <Input placeholder={intl.get('academy.form.contactno')}/>
                      )}
                    </FormItem>

                    <FormItem
                      hasFeedback
                    >
                      {getFieldDecorator('email', {
                        rules: [
                          {required: true, message: intl.get('academy.form.email.error')},
                        ],
                        initialValue: this.state.email,
                        onChange: (e) => this.handleChange(e, 'email'),
                      })(
                        <Input placeholder={intl.get('academy.form.email')}/>
                      )}
                    </FormItem>

                    <FormItem
                      hasFeedback
                    >
                      {getFieldDecorator('nationality', {
                        rules: [
                          {required: true, message: intl.get('academy.form.nationality.error')},
                        ],
                        initialValue: this.state.last_name,
                        onChange: (e) => this.handleChange(e, 'nationality'),
                      })(
                        <Input placeholder={intl.get('academy.form.nationality')}/>
                      )}
                    </FormItem>
                    <FormItem
                      hasFeedback
                    >
                      {getFieldDecorator('username', {
                        initialValue: this.state.last_name,
                        onChange: (e) => this.handleChange(e, 'username'),
                      })(
                        <Input placeholder={intl.get('academy.form.username')}/>
                      )}
                    </FormItem>

                    <FormItem className="text-center">
                      <Button type="primary"
                              htmlType="submit">{intl.getHTML('academy.form.submit')}</Button>
                    </FormItem>
                  </Form>
                </Col>
              </Row>
            </div>
            <img src={Slicing68} className="misc-academy-hand-countdown"
                 data-aos='fade-left' data-aos-delay="600"/>
          </div>
        </Col>
      </div>
    );
  }

  handleSubmit = (e) => {
    const {createAcademy} = this.props;
    e.preventDefault();
    this.setState({responseMessage: ''});
    this.props.form.validateFields((err, values) => {
      if (!err) {
        createAcademy(values).then((res) => {
          this.setState({responseMessage: intl.getHTML('academy.form.response.success')});
        }).catch((err) => {
          this.setState({responseMessage: intl.getHTML('academy.form.response.error')});
        });
      }
    });
  };

  setCurrentCourse(course) {
    console.log(this.state.myTween1);
    if (course == 'course-1') {
      this.state.courseTween1.restart();
    }
    if (course == 'course-2') {
      this.state.courseTween2.restart();
    }
    if (course == 'course-3') {
      this.state.courseTween3.restart();
    }
    if (course == 'course-4') {
      this.state.courseTween4.restart();
    }
    this.setState({currentCourse: course})
  }

  handleChange(e, key) {
    if (e.target != null) {
      this.props.form.setFieldsValue({
        [key]: e.target.value,
      });
    } else {
      this.props.form.setFieldsValue({
        [key]: e,
      });
    }
  };
}

export default Form.create()(Academy);
