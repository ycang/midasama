// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../redux/modules/views';
import * as academyActions      from '../../redux/modules/academy';
import Academy                   from './Academy';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    academy: state.academy,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...academyActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Academy);
