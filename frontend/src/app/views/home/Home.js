// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import moment from 'moment';
import LinesEllipsis from 'react-lines-ellipsis'
import {Row, Col, Carousel, Button, Modal, Icon} from 'antd';
import CustomBanner from "../../components/customBanner/CustomBanner";
import homepageBanner01 from '../../images/homepage-banner-01.png';
import dotsImage from '../../images/Slicing-04.png';
import intl from 'react-intl-universal';
import Slicing06 from '../../images/Slicing-06.png';
import DummyImage from '../../images/ppl.jpg';
import Slicing11 from '../../images/Slicing-11-1.jpg';
import Slicing12 from '../../images/Slicing-12-1.jpg';
import Slicing13 from '../../images/Slicing-13-1.jpg';
import Slicing14 from '../../images/Slicing-14-1.jpg';
import Slicing15 from '../../images/Slicing-15-1.jpg';
import Slicing16 from '../../images/Slicing-16.png';
import Slicing79 from '../../images/Slicing-79.png';
import Slicing80 from '../../images/Slicing-80.png';
import {isEnUsLocale} from "../../services/utils";
import {MultiAssetCard, RiskProfileQuestionnaire, AnimatedIcon} from "../../components/";
import elitebizLogo from '../../images/Slicing-60.png';

class Home extends PureComponent {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterHome: PropTypes.func.isRequired,
    leaveHome: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      modalRiskProfileVisible: false,
      modalNewsVisible: false,
      currentNewsModalType: 'text-only', // text-only , video , gallery
      currentNews: ''
    }
  }

  componentDidMount() {
    const {enterHome, fetchBannerList, fetchNewsList, fetchPartnershipList} = this.props;
    enterHome();
    fetchBannerList({isPublished: true});
    fetchNewsList({isPublished: true});
    fetchPartnershipList({isPublished: true});

    var headElem = document.getElementsByTagName('head')[0];

    setTimeout(function () {
      var tradingWidgetInitCode = document.createElement('script');
      tradingWidgetInitCode.type = "text/javascript";
      tradingWidgetInitCode.src = "https://s3.tradingview.com/external-embedding/embed-widget-market-quotes.js";
      tradingWidgetInitCode.innerHTML = '{ "locale": "en", "symbolsGroups": [ { "symbols": [ { "name": "FX_IDC:EURUSD" }, { "name": "FX_IDC:USDJPY" }, { "name": "FX_IDC:GBPUSD" }, { "name": "FX_IDC:AUDUSD" }, { "name": "FX_IDC:USDCAD" }, { "name": "FX_IDC:USDCHF" } ], "name": "Major" }, { "symbols": [ { "name": "FX_IDC:EURGBP" }, { "name": "FX_IDC:EURJPY" }, { "name": "FX_IDC:GBPJPY" }, { "name": "FX_IDC:CADJPY" }, { "name": "FX_IDC:GBPCAD" }, { "name": "FX_IDC:EURCAD" } ], "name": "Minor" }, { "symbols": [ { "name": "FX_IDC:USDSEK" }, { "name": "FX_IDC:USDMXN" }, { "name": "FX_IDC:USDZAR" }, { "name": "FX_IDC:EURTRY" }, { "name": "FX_IDC:EURNOK" }, { "name": "FX_IDC:GBPPLN" } ], "name": "Exotic" }, { "symbols": [ { "displayName": "U.S. Dollar", "name": "TVC:DXY" }, { "displayName": "Euro", "name": "TVC:EXY" }, { "displayName": "Japanese Yen", "name": "TVC:JXY" }, { "displayName": "British Pound", "name": "TVC:BXY" }, { "displayName": "Australian Dollar", "name": "TVC:AXY" }, { "displayName": "Canadian Dollar", "name": "TVC:CXY" } ], "name": "Currency Indices" } ], "title": "Currencies", "height": 450, "width": 770, "title_link": "/markets/currencies/rates-all/" ,"container_id":"widget1"}';
      headElem.appendChild(tradingWidgetInitCode);
    }, 1500);
  }

  componentWillUnmount() {
    const {leaveHome} = this.props;
    leaveHome();
  }

  render() {
    const {banner, news} = this.props;
    return (
      <div>
        <Row>
          <Col md={24}>
            <Carousel autoplay autoplaySpeed={10000}>
              <div>
                <div className="banner__wrapper banner__wrapper--custom"
                     style={{'backgroundImage': `url('${homepageBanner01 }')`}}>
                  <div className="banner__content-wrapper container-compact">
                    <h1
                      className="text-primary banner__heading" data-aos='fade-up'>
                      <b>{intl.getHTML('home.banner.heading')}</b></h1>
                    <h4 className="text-white banner__subheading" data-aos='fade-up'
                        data-aos-delay='300'>{intl.getHTML('home.banner.subHeading')}</h4>
                    <br/>
                    <Button type="primary"
                            href='https://member.midasama.com/Account/Register' data-aos='fade-up'
                            data-aos-delay='600'>{intl.getHTML('home.banner.cta')}</Button>
                  </div>
                  <div className="custom-banner__wrapper">
                    <CustomBanner/>
                  </div>
                </div>
              </div>
              {
                banner.list.length > 0 ? banner.list.map((banner, i) => {
                  return (banner.status == 'active') ?
                    (
                      <div key={i}>
                        <div className="banner__wrapper banner__wrapper--desktop"
                             style={{'backgroundImage': `url(${banner.banner_image_desktop.filename})`}}>
                          <div className="banner__content-wrapper container-compact">
                            <h1
                              className="text-primary banner__heading">
                              <b>{(isEnUsLocale()) ? banner.title_en : banner.title_cn}</b></h1>
                            <h4
                              className="text-white banner__subheading">{(isEnUsLocale()) ? banner.subtitle_en : banner.subtitle_cn}</h4>
                            <br/>
                          </div>
                        </div>
                        <div className="banner__wrapper banner__wrapper--mobile"
                             style={{'backgroundImage': `url(${banner.banner_image_mobile.filename})`}}>
                          <div className="banner__content-wrapper container-compact">
                            <h1
                              className="text-primary banner__heading">
                              <b>{(isEnUsLocale()) ? banner.title_en : banner.title_cn}</b></h1>
                            <h4
                              className="text-white banner__subheading">{(isEnUsLocale()) ? banner.subtitle_en : banner.subtitle_cn}</h4>
                            <br/>
                          </div>
                        </div>
                      </div>
                    ) : ''
                }) : ''
              }
            </Carousel>
          </Col>
        </Row>

        <Row>
          <Col xs={24}>
            <iframe scrolling="no" allowtransparency="true" frameBorder="0"
                    src="https://s.tradingview.com/tickerswidgetembed/?locale=en#%7B%22symbols%22%3A%5B%7B%22description%22%3A%22EURUSD%22%2C%22proName%22%3A%22FX%3AEURUSD%22%7D%2C%7B%22description%22%3A%22USDJPY%22%2C%22proName%22%3A%22FX%3AUSDJPY%22%7D%2C%7B%22description%22%3A%22GBPUSD%22%2C%22proName%22%3A%22FX%3AGBPUSD%22%7D%2C%7B%22description%22%3A%22AUDUSD%22%2C%22proName%22%3A%22FX%3AAUDUSD%22%7D%2C%7B%22description%22%3A%22USDCAD%22%2C%22proName%22%3A%22FX%3AUSDCAD%22%7D%2C%7B%22description%22%3A%22USDCHF%22%2C%22proName%22%3A%22FX%3AUSDCHF%22%7D%2C%7B%22description%22%3A%22EURGBP%22%2C%22proName%22%3A%22FX%3AEURGBP%22%7D%2C%7B%22description%22%3A%22EURJPY%22%2C%22proName%22%3A%22FX%3AEURJPY%22%7D%2C%7B%22description%22%3A%22GBPJPY%22%2C%22proName%22%3A%22FX%3AGBPJPY%22%7D%5D%2C%22width%22%3A%22100%25%22%2C%22height%22%3A104%2C%22utm_source%22%3A%22www.tradingview.com%22%2C%22utm_medium%22%3A%22widget_new%22%2C%22utm_campaign%22%3A%22tickers%22%7D"
                    style={{
                      boxSizing: 'border-box',
                      height: 'calc(72px)',
                      width: '100%',
                      position: 'relative',
                      top: -5
                    }}></iframe>
            <div className="tradingview-widget-copyright text-center"><a
              href="https://www.tradingview.com" rel="noopener" target="_blank"><span
              className="blue-text text-letterspacing-0">Quotes</span></a> <span
              className=" text-letterspacing-0">by TradingView</span>
            </div>
          </Col>
        </Row>

        <Row className="bg-athens-gray container-vertical overflow-none">
          <Col xs={24} className="z-index-1" data-aos='fade-up'>
            <div className="container-tide">
              <div className=" misc-home-section-heading">
                <div className="text-white numbering-01 numbering--left" data-aos='fade-up'>01</div>
                <h1 className="text-madison"><b>{intl.getHTML('home.section1.heading')}</b></h1>
                <h5 className="text-abbey">{intl.getHTML('home.section1.subHeading')}</h5>
              </div>
              <br/>
              <Button
                type="primary"
                onClick={() => this.redirectToUrl('/about')}>
                {intl.getHTML('home.section1.cta')}<Icon type="caret-right"/>
              </Button>
            </div>
          </Col>

          <div className="container-compact">
            <Col xs={24} md={8} className="text-center p-10" data-aos='fade-up'>
              <AnimatedIcon type="secure"/>
              <h4 className="text-madison"><b>{intl.getHTML('home.section1.item1.heading')}</b></h4>
              <h5 className="text-abbey">{intl.getHTML('home.section1.item1.subHeading')}</h5>
            </Col>
            <Col xs={24} md={8} className="text-center p-10" data-aos='fade-up'
                 data-aos-delay='300'>
              <AnimatedIcon type="transparency"/>
              <h4 className="text-madison"><b>{intl.getHTML('home.section1.item2.heading')}</b></h4>
              <h5 className="text-abbey">{intl.getHTML('home.section1.item2.subHeading')}</h5>
            </Col>
            <Col xs={24} md={8} className="text-center p-10" data-aos='fade-up'
                 data-aos-delay='600'>
              <AnimatedIcon type="professional"/>
              <h4 className="text-madison"><b>{intl.getHTML('home.section1.item3.heading')}</b></h4>
              <h5 className="text-abbey">{intl.getHTML('home.section1.item3.subHeading')}</h5>
            </Col>
          </div>
        </Row>

        <Row className="bg-outer-space bg-dots overflow-none"
             style={{'backgroundImage': `url('${dotsImage}')`}}>
          <Col xs={24} className="z-index-1">
            <div className="container-tide container-vertical">
              <div className="text-alto numbering-02 numbering--right" data-aos='fade-up'>02</div>
              <Row>
                <Col md={20} lg={16}>
                  <h1 className="text-primary" data-aos='fade-up'>
                    <b>{intl.getHTML('home.section2.heading')}</b></h1>
                  <p className="text-white" data-aos='fade-up'
                     data-aos-delay='300'>{intl.getHTML('home.section2.subHeading')}</p>
                </Col>
              </Row>
              <br/>
              <br/>
              <br/>
              <Row>
                <Col md={20} lg={13} data-aos='fade-up' data-aos-delay='600'>
                  <p className="text-white">{intl.getHTML('home.section2.item1.heading')}</p>
                  <div className="border-l-primary" style={{paddingBottom: 30}}>
                    <p
                      className="text-white p-l-20">{intl.getHTML('home.section2.item1.subHeading')}</p>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>

        <Row>
          <Col md={8} style={{'backgroundImage': `url('${Slicing11}')`}}
               className="product-box" data-aos='fade-up'
               onClick={(e) => this.setProductBoxActive(e)}>
            <div className="product-box__content-wrapper">
              <h3
                className="text-right product-box__heading">{intl.getHTML('product.item1.heading')}</h3>
              <div className="product-box__subHeading-wrapper">
                <h5
                  className="product-box__subHeading">{intl.getHTML('product.item1.subHeading')}</h5>
              </div>
            </div>
          </Col>
          <Col md={8} style={{'backgroundImage': `url('${Slicing12}')`}}
               className="product-box" data-aos='fade-up' data-aos-delay='300'
               onClick={(e) => this.setProductBoxActive(e)}>
            <div className="product-box__content-wrapper">
              <h3
                className="text-right product-box__heading">{intl.getHTML('product.item2.heading')}</h3>
              <div className="product-box__subHeading-wrapper">
                <h5
                  className="product-box__subHeading">{intl.getHTML('product.item2.subHeading')}</h5>
              </div>
            </div>
          </Col>
          <Col md={8} style={{'backgroundImage': `url('${Slicing13}')`}}
               className="product-box" data-aos='fade-up' data-aos-delay='600'
               onClick={(e) => this.setProductBoxActive(e)}>
            <div className="product-box__content-wrapper">
              <h3
                className="text-right product-box__heading">{intl.getHTML('product.item3.heading')}</h3>
              <div className="product-box__subHeading-wrapper">
                <h5
                  className="product-box__subHeading">{intl.getHTML('product.item3.subHeading')}</h5>
              </div>
            </div>
          </Col>
          <Col md={12} style={{'backgroundImage': `url('${Slicing14}')`}}
               className="product-box" data-aos='fade-up' data-aos-delay='300'
               onClick={(e) => this.setProductBoxActive(e)}>
            <div className="product-box__content-wrapper">
              <h3
                className="text-right product-box__heading">{intl.getHTML('product.item4.heading')}</h3>
              <div className="product-box__subHeading-wrapper">
                <h5
                  className="product-box__subHeading">{intl.getHTML('product.item4.subHeading')}</h5>
              </div>
            </div>
          </Col>
          <Col md={12} style={{'backgroundImage': `url('${Slicing15}')`}}
               className="product-box" data-aos='fade-up' data-aos-delay='600'
               onClick={(e) => this.setProductBoxActive(e)}>
            <div className="product-box__content-wrapper">
              <h3
                className="text-right product-box__heading">{intl.getHTML('product.item5.heading')}</h3>
              <div className="product-box__subHeading-wrapper">
                <h5
                  className="product-box__subHeading">{intl.getHTML('product.item5.subHeading')}</h5>
              </div>
            </div>
          </Col>
        </Row>

        <Row className="misc-home-academy-section">
          <Col xs={24} md={24} className="z-index-1">
            <div className="container-tide container-vertical">
              <div className="text-alto numbering-03 numbering--left" data-aos='fade-up'>03</div>
              <h1 className="text-madison" data-aos='fade-up'>
                <b>{intl.getHTML('home.section8.heading')}</b>
                <img src={isEnUsLocale() ? Slicing80 : Slicing79} className="misc-badge-large"/>
              </h1>
              <h3 className="text-primary" data-aos='fade-up'
                  data-aos-delay='300'>{intl.getHTML('home.section8.subheading1')}</h3>
              <h5 className="text-abbey misc-home-academy-description" data-aos='fade-up' data-aos-delay='500'>{intl.getHTML('home.section8.subheading2')}</h5>
              <br/>
              <Button
                type="primary" data-aos='fade-up' data-aos-delay='600' data-aos-offset='-20'
                onClick={() => this.redirectToUrl('/academy')} className="m-r-20">
                {intl.getHTML('home.section8.cta1')}<Icon type="caret-right"/>
              </Button>
              <Button
                type="primary" data-aos='fade-up' data-aos-delay='600' data-aos-offset='-20'
                target="_blank"
                href={isEnUsLocale() ? 'https://elitebiz.academy/en/' : 'https://elitebiz.academy/wpebcn'}>
                {intl.getHTML('home.section8.cta2')}<Icon type="caret-right"/>
              </Button>
            </div>
          </Col>

          <img src={elitebizLogo} data-aos='fade-up' data-aos-delay="300" data-aos-offset='-20' className="misc-home-academy-elitebiz-logo" alt="Elitebize"/>
        </Row>

        <Row className="bg-light-gray misc-home-platform-section">
          <Col xs={24} md={24} className="z-index-1">
            <div className="container-tide container-vertical">
              <div className="text-alto numbering-04 numbering--right" data-aos='fade-up'>04</div>
              <h1 className="text-madison" data-aos='fade-up'>
                <b>{intl.getHTML('home.section4.heading')}</b></h1>
              <h5 className="text-abbey" data-aos='fade-up' data-aos-delay='300'
                  style={{maxWidth: 850}}>{intl.getHTML('home.section4.subHeading')}</h5>
              <br/>
              <Button
                type="primary" data-aos='fade-up' data-aos-delay='300'
                onClick={() => this.redirectToUrl('/platform')}>
                {intl.getHTML('home.section4.cta')}<Icon type="caret-right"/>
              </Button>
            </div>
            <img src={Slicing16} className="misc-home-platform-computer" data-aos='fade-up'
                 data-aos-offset="-200" data-aos-delay='600'/>
          </Col>
        </Row>

        <Row className="bg-shark overflow-none misc-home-news-section">
          <Col xs={24} className="z-index-1">
            <div className="container-vertical">
              <div className="container-tide">
                <div className="text-alto numbering-05 numbering--left" data-aos='fade-up'>05</div>
                <h1 className="text-primary" data-aos='fade-up'>
                  <b>{intl.getHTML('home.section5.heading')}</b></h1>
                <h5 className="text-white p-b-60 hide"
                    style={{maxWidth: 800}} data-aos='fade-up'
                    data-aos-delay='300'>{intl.getHTML('home.section5.subHeading')}</h5>
              </div>
              <div className="container-compact">
                <Row gutter={18} className="p-t-50">
                  {
                    news.list.length > 0 ?
                      news.list.map((n, i) => {
                        let xsValue = i < 2 ? 24 : 0;
                        let smValue = i < 2 ? 12 : 0;
                        let mdValue = i == 3 ? 0 : 8;
                        if (i < 4) {
                          return (
                            <Col xs={xsValue} sm={smValue} md={mdValue} lg={mdValue} xl={mdValue}
                                 xxl={6} key={i} data-aos='fade-up' data-aos-delay={i * 300}>
                              <MultiAssetCard onClickModal={() => this.handleOnClickModal(true, n)}
                                              isVideo={n.video_link ? true : false}
                                              coverImage={(n.cover_image.filename) ? n.cover_image.filename : DummyImage}
                                              title={(isEnUsLocale()) ? n.title_en : n.title_cn}
                                              created_at={(n.news_date) ? n.news_date : ''}
                                              description={(isEnUsLocale()) ? n.content_en : n.content_cn}
                              />
                            </Col>
                          )
                        }
                      })
                      : ''
                  }
                </Row>
                <br/>
                <br/>
                <div className="text-center">
                  <Button
                    type="primary"
                    data-aos='fade-up'
                    onClick={() => this.redirectToUrl('/latest-news')}>
                    {intl.getHTML('home.section5.cta')}<Icon type="caret-right"/>
                  </Button>
                </div>
              </div>
            </div>
          </Col>
        </Row>

        <Row className="misc-home-risk-profile-section">
          <img src={Slicing06} className="misc-risk-profile-computer" data-aos='fade-right'
               data-aos-offset="-50" data-aos-delay={300}/>
          <Col sm={8}>
          </Col>
          <Col sm={16}>
            <div className="container-compact container-vertical">
              <h1 className="text-primary" data-aos='fade-up'>
                <b>{intl.getHTML('home.section6.heading')}</b></h1>
              <h5 className="text-white" data-aos='fade-up'
                  data-aos-delay='300'>{intl.getHTML('home.section6.subHeading')}</h5>
              <br/>
              <Button
                type="primary"
                data-aos='fade-up'
                data-aos-delay='600'
                onClick={() => this.setModalRiskProfileVisible(true)}
              >
                {intl.getHTML('home.section6.cta')}<Icon type="caret-right"/>
              </Button>
            </div>
          </Col>
        </Row>

        <Row className="container-vertical overflow-none">
          <div className="container-tide">
            <div className="text-white numbering-06 numbering--right" data-aos='fade-up'>06</div>
            <div className="z-index-1">
              <Col xs={24}>
                <h1 className="text-madison" data-aos='fade-up'>
                  <b>{intl.getHTML('home.section7.heading')}</b></h1>
                {
                  this.props.partnership.list.length > 0 ?
                    this.props.partnership.list.map((partnership, i) => {
                      if (i < 3) {
                        return <Row key={i} className="misc-home-partnership-list-item"
                                    data-aos='fade-up' data-aos-delay={i * 300}>
                          <Col md={9} className="text-right">
                            {
                              partnership.website ?
                                <a href={partnership.website} target="_blank">
                                  <img src={partnership.cover_image.filename}
                                       className="p-l-20 p-r-20 p-b-40 p-t-60" width="100%"
                                       style={{maxWidth: 300}}/>
                                </a> :
                                <img src={partnership.cover_image.filename}
                                     className="p-l-20 p-r-20 p-b-40 p-t-60" width="100%"
                                     style={{maxWidth: 300}}/>
                            }
                          </Col>
                          <Col md={15}>
                            <h3
                              className="text-letterspacing-0">{(isEnUsLocale()) ? partnership.title_en : partnership.title_cn}</h3>
                            <div className="border-l-primary p-l-20">
                              <LinesEllipsis
                                text={(isEnUsLocale()) ? partnership.content_en : partnership.content_cn}
                                maxLine='5'
                                ellipsis='...'
                                trimRight
                                basedOn='letters'
                              />
                            </div>
                          </Col>
                        </Row>
                      }
                    })
                    : ''
                }
              </Col>
              <Col xs={24} className="text-right">
                <Button
                  type="secondary"
                  data-aos-delay='300'
                  onClick={() => this.redirectToUrl('/partnerships')}>
                  {intl.getHTML('home.section7.cta')}<Icon type="caret-right"/>
                </Button>
              </Col>
            </div>
          </div>
        </Row>

        <Modal
          title={null}
          wrapClassName="ant-modal--fullpage risk-profile-modal"
          visible={this.state.modalRiskProfileVisible}
          onOk={() => this.setModalRiskProfileVisible(false)}
          onCancel={() => this.setModalRiskProfileVisible(false)}
          footer={null}
          width="100%"
        >
          <Row type="flex" justify="center" align="middle" style={{height: '100%'}}>
            <Col xs={24}>
              <RiskProfileQuestionnaire modalStatus={this.state.modalRiskProfileVisible}/>
            </Col>
          </Row>
        </Modal>

        <Modal
          title={null}
          wrapClassName="vertical-center-modal modal-global"
          visible={this.state.modalNewsVisible}
          onOk={() => this.setModalNewsVisible(false)}
          onCancel={() => {
            this.setModalNewsVisible(false);
            this.setState({currentNews: []})
          }}
          footer={null}
        >
          <div
            className={cx('bg-white p-20', {'hide': this.state.currentNewsModalType != 'text-only'})}>
            <Row className="border-b-primary">
              <Col md={20} className="border-r-primary"><h4
                className="m-b-5">{(isEnUsLocale()) ? this.state.currentNews.title_en : this.state.currentNews.title_cn}</h4>
              </Col>
              <Col md={4} className="text-right"><h5
                className="text-letterspacing-0 m-0">{(this.state.currentNews.news_date) ? moment(this.state.currentNews.news_date).format('YYYY-MM-DD') : ''}</h5>
              </Col>
            </Row>
            <Row>
              <Col><p
                className="m-t-10">{(isEnUsLocale()) ? this.state.currentNews.content_en : this.state.currentNews.content_cn}</p>
              </Col>
            </Row>
          </div>
          <div className={cx({'hide': this.state.currentNewsModalType != 'video'})}>
            <iframe height='498' width='100%' src={this.state.currentNews.video_link}
                    frameBorder='0' allowfullscreen></iframe>
          </div>
          <div className={cx({'hide': this.state.currentNewsModalType != 'gallery'})}>
            <Carousel>
              {
                this.props.gallery.list.length > 0 ?
                  this.props.gallery.list.map((gallery, i) => {
                    return <div key={i}>
                      <img src={gallery.gallery_image.filename} width="100%"/>
                      <div className="modal-content-wrapper">
                        <Row className="border-b-white">
                          <Col md={20} className="border-r-white"><h4
                            className="text-white m-b-5">{(isEnUsLocale()) ? gallery.title_en : gallery.title_cn}</h4>
                          </Col>
                          <Col md={4} className="text-right"><h5
                            className="text-letterspacing-0 text-white m-0">{(this.state.currentNews.news_date) ? moment(this.state.currentNews.news_date).format('YYYY-MM-DD') : ''}</h5>
                          </Col>
                        </Row>
                        <Row>
                          <Col><p
                            className="text-white m-t-10">{(isEnUsLocale()) ? gallery.content_en : gallery.content_cn}</p>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  })
                  : ''
              }
            </Carousel>
          </div>
        </Modal>
      </div>
    );
  }

  setProductBoxActive(e) {
    if ($(e.target).hasClass('active')) {
      $(e.target).removeClass('active');
    } else {
      $(e.target).addClass('active');
    }
  }

  redirectToUrl(pathname) {
    const {
      history
    } = this.props;

    history.push({pathname});
  }

  setModalNewsVisible(modalNewsVisible) {
    this.setState({modalNewsVisible});
  }

  handleOnClickModal(modalNewsVisible, n) {
    if (n.news_type == 'gallery') {
      this.props.fetchGalleryListByNewsId(n.id);
    }
    this.setState({currentNewsModalType: n.news_type, currentNews: n});
    this.setState({modalNewsVisible});
  }

  setModalRiskProfileVisible(modalRiskProfileVisible) {
    this.setState({modalRiskProfileVisible});
  }
}

export default Home;
