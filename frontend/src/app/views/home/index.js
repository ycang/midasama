// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../redux/modules/views';
import * as bannerActions   from '../../redux/modules/banner';
import * as newsActions   from '../../redux/modules/news';
import * as galleryActions   from '../../redux/modules/gallery';
import * as partnershipActions   from '../../redux/modules/partnership';
import Home                   from './Home';


const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    banner: state.banner,
    gallery: state.gallery,
    news: state.news,
    partnership: state.partnership,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...bannerActions,
      ...newsActions,
      ...galleryActions,
      ...partnershipActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
