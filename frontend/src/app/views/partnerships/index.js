// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../redux/modules/views';
import * as partnershipActions      from '../../redux/modules/partnership';
import * as certificateActions      from '../../redux/modules/certificate';
import Partnerships                   from './Partnerships';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    partnership: state.partnership,
    certificate: state.certificate
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...partnershipActions,
      ...certificateActions,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Partnerships);
