// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
// import classnames     from 'classnames/bind';
import {Row, Col, Carousel, Button, Tabs} from 'antd';
import intl from 'react-intl-universal';
import {Link} from 'react-router-dom';
import {HeroBanner} from '../../components/';
import homepageBanner01 from '../../images/homepage-banner-01.png';
import homepageBanner02 from '../../images/homepage-banner-02.png';
import Slicing37 from '../../images/Slicing-37.jpg';
import {isEnUsLocale} from "../../services/utils";

import AOS from 'aos';

const TabPane = Tabs.TabPane;

// IMPORTANT: we need to bind classnames to CSSModule generated classes:
// const cx = classnames.bind(styles);

class Partnerships extends PureComponent {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterHome: PropTypes.func.isRequired,
    leaveHome: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      isLoading: true
    }

  }

  componentDidMount() {
    const {fetchPartnershipList} = this.props;
    fetchPartnershipList({isPublished: true});
  }

  componentWillUnmount() {
  }

  render() {
    const {} = this.props;
    return (
      <Row>
        <HeroBanner title={intl.getHTML('partnerships.heroBanner.heading')}
                    description=""
                    imageUrl={Slicing37}/>
        <Col>
          <Tabs defaultActiveKey="0" className="product-tab partnership-tab" animated={false}>
            {
              this.props.partnership.list.length > 0 ?
                this.props.partnership.list.map((partnership, i) => {
                  return (
                    <TabPane tab={(isEnUsLocale()) ? partnership.title_en : partnership.title_cn}
                             key={i}>
                      <div className="container-vertical container-compact">
                        <Row key={i}>
                          <Col md={9} className="text-right">

                            {
                              partnership.website ?
                                <a href={partnership.website} target="_blank">
                                  <img src={partnership.cover_image.filename}
                                       className="p-l-20 p-r-20 p-b-40 p-t-60" width="100%"
                                       style={{maxWidth: 500}}/>
                                </a>
                                :
                                <img src={partnership.cover_image.filename}
                                     className="p-l-20 p-r-20 p-b-40 p-t-60" width="100%"
                                     style={{maxWidth: 500}}/>
                            }
                          </Col>
                          <Col md={15}>
                            <ul className="custom-list-style custom-list-style--subheading">
                              <li>
                                <h3 className="text-madison text-letterspacing-0">
                                  <b>{(isEnUsLocale()) ? partnership.title_en : partnership.title_cn}</b>
                                </h3>
                              </li>
                            </ul>
                            <div className="border-l-primary p-l-20">
                              <p
                                dangerouslySetInnerHTML={{__html: (isEnUsLocale()) ? partnership.content_en : partnership.content_cn}}></p>
                            </div>
                          </Col>
                          <Col xs={24}>
                            <Row gutter={18} className="m-t-30">
                              {
                                (partnership.certificates && partnership.certificates.data.length > 0) ?
                                  partnership.certificates.data.map((certificate, i) => {
                                    if (certificate.status == 'active') {
                                      return (
                                        <Col xs={24} md={12} key={i} className="text-center m-b-15">
                                          <p>
                                        <span
                                          className="text-madison">{(isEnUsLocale()) ? certificate.title_en : certificate.title_cn}</span><br/>
                                            <span
                                              dangerouslySetInnerHTML={{__html: (isEnUsLocale()) ? certificate.content_en : certificate.content_cn}}></span>
                                          </p>
                                          <img src={certificate.cover_image.filename} width="100%"
                                               style={{maxWidth: 500}}/>
                                        </Col>)
                                    }
                                  })
                                  : ''
                              }
                            </Row>
                          </Col>
                        </Row>
                      </div>
                    </TabPane>)
                })
                : ''
            }
          </Tabs>
        </Col>
      </Row>
    );
  }
}

export default Partnerships;
