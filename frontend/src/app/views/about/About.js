// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';
import {HeroBanner, AnimatedIcon} from '../../components/';
import {Row, Col, Divider} from 'antd';
import Slicing21 from '../../images/Slicing-21.jpg';
import Slicing30 from '../../images/Slicing-30.jpg';
import Slicing24 from '../../images/Slicing-24.png';
import Slicing25 from '../../images/Slicing-25.png';
import anime from 'animejs';

class About extends PureComponent {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterAbout: PropTypes.func.isRequired,
    leaveAbout: PropTypes.func.isRequired
  };

  constructor(props){
    super(props);
    this.state = {
      aboutVideo : ''
    }
  }

  componentDidMount() {
    const {enterAbout, fetchSystemSettingList} = this.props;
    enterAbout();

    var timelineParameters = anime.timeline({
      direction: 'alternate',
      easing: 'easeInOutQuad',
      loop: true
    });
    timelineParameters
      .add({
        targets: '.js-animated-float',
        translateX: [{value: 0}, {value: 0}],
        translateY: [{value: -10}, {value: 10}],
        elasticity: 200,
        duration: 1000
      });

    fetchSystemSettingList().then((res) => {
      const {systemSetting} = this.props;
      systemSetting.list.map((setting) => {
        if(setting.code == 'about_us_video_url'){
          this.setState({ aboutVideo : setting.value });
        }
      });
      this.setState({
        loading: false,
      });
      return res;
    }).catch((res) => {
      this.setState({loading: false});
      return res;
    });
  }

  componentWillUnmount() {
    const {leaveAbout} = this.props;
    leaveAbout();
  }

  render() {
    return (
      <div>
        <HeroBanner title={intl.getHTML('about.heroBanner.heading')}
                    description=""
                    imageUrl={Slicing21}/>
        <div className="container-compact container-vertical misc-about-section1">
          <Row>
            <Col md={24}>
              <h1 className="text-madison"
                  data-aos='fade-up'>{intl.getHTML('about.section1.heading')}</h1>
              <p className="border-l-primary p-l-20" data-aos='fade-up'
                 data-aos-delay="300"
                 style={{maxWidth: 580}}>{intl.getHTML('about.section1.subheading')}</p>
            </Col>
            <Col md={24}>
              <h1 className="text-madison m-t-60" data-aos='fade-up'
                  data-aos-delay="600">{intl.getHTML('about.section2.heading')}</h1>
              <p className="border-l-primary p-l-20" data-aos='fade-up'
                 data-aos-delay="600"
                 style={{maxWidth: 580}}>{intl.getHTML('about.section2.subheading')}</p>
            </Col>
          </Row>
        </div>
        <Row className="misc-about-product-section">
          {
            this.state.aboutVideo ? <Col xs={24} className="text-center">
              <div className="misc-about-video-container">
                <iframe height='498' width='100%' className="misc-about-video" src={this.state.aboutVideo} frameBorder='0' allowFullScreen></iframe>
              </div>
            </Col>
              : null
          }

          <div className="container-compact">
            <Col md={11} className="misc-about-product-list-heading-section">
              <h4 className="text-white"
                  data-aos='fade-up'>{intl.getHTML('about.section2.heading3')}</h4>
              <h2
                className="text-primary misc-about-product-list-heading"
                data-aos='fade-up'>{intl.getHTML('about.section2.heading2')}</h2>
            </Col>
            <Col md={13}>
              <div className="misc-about-product-list">
                <h5 className="text-white"
                    data-aos='fade-up'>{intl.getHTML('about.section2.subheading2')}</h5>
                <br/>
                <br/>
                <br/>
              </div>
            </Col>
          </div>
          <div className="container-compact">
            <Col xs={0} sm={0} md={11}>
              <img src={Slicing24} className="misc-about-icon-1 "/>
              <img src={Slicing25} className="misc-about-icon-2 js-animated-float"/>
            </Col>
            <Col md={13}>
              <div data-aos='fade-up' data-aos-delay="300">
                <h4
                  className="text-primary m-b-5 m-t-10">{intl.getHTML('about.product1.heading')}</h4>
                <p className="text-light-gray">{intl.getHTML('about.product1.subheading')}</p>
                <Divider style={{margin: '5px 0'}}/>
              </div>
              <div data-aos='fade-up' data-aos-delay="600">
                <h4
                  className="text-primary m-b-5 m-t-10">{intl.getHTML('about.product2.heading')}</h4>
                <p className="text-light-gray">{intl.getHTML('about.product2.subheading')}</p>
                <Divider style={{margin: '5px 0'}}/>
              </div>
              <div data-aos='fade-up' data-aos-delay="600">
                <h4
                  className="text-primary m-b-5 m-t-10">{intl.getHTML('about.product3.heading')}</h4>
                <p className="text-light-gray">{intl.getHTML('about.product3.subheading')}</p>
                <Divider style={{margin: '5px 0'}}/>
              </div>
              <div data-aos='fade-up' data-aos-delay="600">
                <h4
                  className="text-primary m-b-5 m-t-10">{intl.getHTML('about.product4.heading')}</h4>
                <p className="text-light-gray">{intl.getHTML('about.product4.subheading')}</p>
                <Divider style={{margin: '5px 0'}}/>
              </div>
              <div data-aos='fade-up' data-aos-delay="600">
                <h4
                  className="text-primary m-b-5 m-t-10">{intl.getHTML('about.product5.heading')}</h4>
                <p className="text-light-gray">{intl.getHTML('about.product5.subheading')}</p>
              </div>
            </Col>
          </div>
        </Row>

        <Row className=" p-t-60 p-b-60 misc-about-dots-bg">
          <div className="container-compact">
            <Col md={24} className="text-center">
              <h1 className="text-madison"
                  data-aos='fade-up'>{intl.getHTML('about.section4.heading')}</h1>
              <Row>
                <Col md={8} data-aos='fade-up'>
                  <div className="p-20 misc-about-product-list-item">
                    <AnimatedIcon type="secure"/>
                    <h4 className="text-madison">{intl.getHTML('about.benefit1.heading')}</h4>
                    <p>{intl.getHTML('about.benefit1.subheading')}</p>
                  </div>
                </Col>
                <Col md={8} data-aos='fade-up' data-aos-delay="300">
                  <div className="p-20 misc-about-product-list-item">
                    <AnimatedIcon type="transparency"/>
                    <h4 className="text-madison">{intl.getHTML('about.benefit2.heading')}</h4>
                    <p>{intl.getHTML('about.benefit2.subheading')}</p>
                  </div>
                </Col>
                <Col md={8} data-aos='fade-up' data-aos-delay="600">
                  <div className="p-20 misc-about-product-list-item">
                    <AnimatedIcon type="professional"/>
                    <h4 className="text-madison">{intl.getHTML('about.benefit3.heading')}</h4>
                    <p>{intl.getHTML('about.benefit3.subheading')}</p>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={12} data-aos='fade-up' data-aos-delay="300">
                  <div className="p-20 misc-about-product-list-item">
                    <AnimatedIcon type="easy"/>
                    <h4 className="text-madison">{intl.getHTML('about.benefit4.heading')}</h4>
                    <p>{intl.getHTML('about.benefit4.subheading')}</p>
                  </div>
                </Col>
                <Col md={12} data-aos='fade-up' data-aos-delay="600">
                  <div className="p-20 misc-about-product-list-item">
                    <AnimatedIcon type="money-up"/>
                    <h4 className="text-madison">{intl.getHTML('about.benefit5.heading')}</h4>
                    <p>{intl.getHTML('about.benefit5.subheading')}</p>
                  </div>
                </Col>
              </Row>
            </Col>
          </div>
        </Row>

        <Row type="flex" justify="center" align="middle"
             style={{'backgroundImage': `url('${Slicing30}')`, minHeight: 300}}
             className="text-center bg-center">
          <Col>
            <h1 className="text-primary"
                data-aos='fade-up'>{intl.getHTML('about.section5.heading')}</h1>
            <h4 className="text-white m-l-15 m-r-15" data-aos='fade-up'
                data-aos-delay="300">{intl.getHTML('about.section5.subheading')}</h4>
          </Col>
        </Row>

        <Row className=" misc-about-vision-section">
          <div className="container-compact">
            <Col md={8} data-aos='fade-up' className="m-b-15">
              <ul className="custom-list-style custom-list-style--heading">
                <li><h2 className="text-madison">{intl.getHTML('about.vision1.heading')}</h2></li>
              </ul>
              <h5
                className="border-l-primary p-l-20 p-r-20">{intl.getHTML('about.vision1.subheading')}</h5>
            </Col>
            <Col md={8} data-aos='fade-up' data-aos-delay="300" className="m-b-15">
              <ul className="custom-list-style custom-list-style--heading">
                <li>
                  <h2 className="text-madison">{intl.getHTML('about.vision2.heading')}</h2>
                </li>
              </ul>
              <h5
                className="border-l-primary p-l-20 p-r-20">{intl.getHTML('about.vision2.subheading')}</h5>
            </Col>
            <Col md={8} data-aos='fade-up' data-aos-delay="600" className="m-b-15">
              <ul className="custom-list-style custom-list-style--heading">
                <li>
                  <h2 className="text-madison">{intl.getHTML('about.vision3.heading')}</h2>
                </li>
              </ul>
              <h5
                className="border-l-primary p-l-20 p-r-20">{intl.getHTML('about.vision3.subheading')}</h5>
            </Col>
          </div>
        </Row>
      </div>
    );
  }
}

export default About;
