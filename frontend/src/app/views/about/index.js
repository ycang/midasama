// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../redux/modules/views';
import * as systemSettingActions      from '../../redux/modules/systemSetting';
import About                  from './About';


const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    systemSetting:  state.systemSetting,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...systemSettingActions,
    },
    dispatch
  );
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(About);
