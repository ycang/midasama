// @flow weak

import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import * as viewsActions      from '../../redux/modules/views';
import * as educationActions      from '../../redux/modules/education';
import Education                   from './Education';

const mapStateToProps = (state) => {
  return {
    // views
    currentView:  state.views.currentView,
    education: state.education,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // views
      ...viewsActions,
      ...educationActions
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Education);
