// @flow weak

import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
// import classnames     from 'classnames/bind';
import intl from 'react-intl-universal';
import {Link} from 'react-router-dom';
import {HeroBanner} from '../../components/';
import {Row, Col, Button} from 'antd';
import {isEnUsLocale} from "../../services/utils";
import Slicing57 from '../../images/Slicing-57.jpg';

// IMPORTANT: we need to bind classnames to CSSModule generated classes:
// const cx = classnames.bind(styles);

class Education extends PureComponent {
  static propTypes = {
    // react-router 4:
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    // views:
    currentView: PropTypes.string.isRequired,
    enterHome: PropTypes.func.isRequired,
    leaveHome: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.state = {
      primary_video: '',
      isLoading: true
    }

  }

  componentDidMount() {
    const {fetchEducationList} = this.props;
    fetchEducationList({isPublished:true}).then((res) => {
      const current = res.payload.data.data;
      if(current.length > 0 ){
        this.setState({
          primary_video : current[0]
        })
      }
      this.setState({loading: false})
    }).catch((res) => this.setState({loading: false}));
  }

  componentWillUnmount() {
    const {leaveHome} = this.props;
    leaveHome();
  }

  render() {
    const {} = this.props;
    return (
      <div>
        <HeroBanner title={intl.getHTML('education.heroBanner.heading')}
                    description=""
                    imageUrl={Slicing57}/>
        <Col md={24}>
          <div className="misc-education-dots-bg">
          </div>
          <div className="container-compact container-vertical">
            <Row>
              <Col>
                <div className="misc-education-video-container">
                  <iframe className="misc-education-video" height='490' width='100%' src={this.state.primary_video.video_link} frameBorder='0' allowFullScreen isAutoPlay={true}></iframe>
                  <p className="text-center">{ (isEnUsLocale()) ? this.state.primary_video.title_en : this.state.primary_video.title_cn }</p>
                </div>
              </Col>
            </Row>
            <Row className="p-t-50" gutter={18}>
              {
                this.props.education ? this.props.education.list.map((education, i) => {
                  return (
                    <Col xs={24} sm={12} md={8} xl={6} key={i} data-aos='fade-up'
                         data-aos-delay={(i <= 3) ? i * 300 : 600} className={`video-${i}`}>
                      <div style={{position: 'relative'}}>
                        <div style={{ position: 'absolute' , width: '100%', height: '100%', backgroundColor: 'transparent'}}  onClick={()=> this.setAsPrimaryVideo(education)}></div>
                        <iframe className="misc-education-small-video" height='200' width='100%' src={education.video_link} frameBorder='0' allowFullScreen></iframe>
                        <p>{ (isEnUsLocale()) ? education.title_en : education.title_cn }</p>
                      </div>
                    </Col>
                  )
                }) : ''
              }
            </Row>
          </div>
        </Col>
      </div>
    );
  }

  setAsPrimaryVideo(primary_video){
    this.setState({
      primary_video
    });

    document.body.scrollTop = 300; // For Safari
    document.documentElement.scrollTop = 300; // For Chrome, Firefox, IE and Opera
  }
}

export default Education;
