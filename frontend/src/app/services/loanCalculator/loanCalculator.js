export const pmtCalculation = (rate, nper, pv, fv, type) => {
  if (!fv) {
    fv = 0;
  }
  if (!type) {
    type = 0;
  }

  if (rate == 0) {
    return -(pv + fv) / nper;
  }

  const pvif = Math.pow(1 + rate, nper);
  let pmt = rate / (pvif - 1) * -(pv * pvif + fv);

  if (type == 1) {
    pmt = pmt / (1 + rate);
  }
  return pmt;
};

export const finalMonthlyInstallment = (loanTermInYear, propertyFinalPrice, downPaymentPercent, interestRate) => {
  let monthlyInstallment = 0;
  const loanTerm = loanTermInYear * 12;

  if (interestRate && propertyFinalPrice && loanTermInYear > 0) {
    let downPaymentAmount = 0;
    if(downPaymentPercent > 0) {
      downPaymentAmount = (downPaymentPercent / 100) * propertyFinalPrice;
    }

    const loanAmount = (propertyFinalPrice - downPaymentAmount);
    monthlyInstallment = -pmtCalculation((interestRate / 100) / 12, loanTerm, loanAmount);
  }
  return monthlyInstallment;
};
