import {GlobalVars} from '../../config/global_vars';
import auth from '../../services/auth';
import intl from 'react-intl-universal';

export const uploadProps = {
  name: 'file',
  listType: 'picture',
  action: `${GlobalVars.apiServer}/asset`
}

/**
 * only support one file upload
 * @param values
 * @param attribute
 * @returns {boolean}
 */
export const getUploadedFileId = (values,attribute) => {
  if (values[attribute].fileList) {
    return values[attribute].fileList[0].response.data.id;
  } else {
    if (values[attribute][0].uid) {
      return values[attribute][0].uid;
    }
  }
  return false
}

/**
 * only support multiple upload
 * @param values
 * @param attribute
 * @returns {boolean}
 */
export const getUploadedFileIds = (values,attribute) => {
  if (values[attribute].fileList) {
    let fileIdArr = [];
    values[attribute].fileList.map((res) => {
      fileIdArr.push(res.response.data.id);
    })
    return fileIdArr;
  } else {
    if (values[attribute][0].uid) {
      return values[attribute][0].uid;
    }
  }
  return false
}

/**
 * check is en-US locale
 * @returns {boolean}
 */
export const isEnUsLocale = () => {
  return (intl.getInitOptions().currentLocale == 'en-US') ? true : false ;
}
