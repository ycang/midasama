// @flow weak

import React            from 'react';
import {
  Route,
  Switch
 }                      from 'react-router';
import Home             from '../views/home';

import AdminLogin       from '../views/admin/login';
import AdminDashboard   from '../views/admin/dashboard';
import AdminBanner   from '../views/admin/banner';
import AdminBannerEdit   from '../views/admin/banner/edit';
import AdminNews   from '../views/admin/news';
import AdminNewsEdit   from '../views/admin/news/edit';
import AdminGallery   from '../views/admin/gallery';
import AdminGalleryEdit   from '../views/admin/gallery/edit';
import AdminProduct   from '../views/admin/product';
import AdminProductEdit   from '../views/admin/product/edit';
import AdminEducation   from '../views/admin/education';
import AdminEducationEdit   from '../views/admin/education/edit';
import AdminPartnership   from '../views/admin/partnership';
import AdminPartnershipEdit   from '../views/admin/partnership/edit';
import AdminCertificate   from '../views/admin/certificate';
import AdminCertificateEdit   from '../views/admin/certificate/edit';
import AdminAcademy   from '../views/admin/academy';
import AdminAcademyEdit   from '../views/admin/academy/edit';
import AdminSettingEdit   from '../views/admin/setting/edit';

import About            from '../views/about';
import Products            from '../views/products';
import News            from '../views/news';
import Platform            from '../views/platform';
import Partnerships            from '../views/partnerships';
import Education            from '../views/education';
import Academy            from '../views/academy';

import PrivateRoute     from '../components/privateRoute/PrivateRoute';
import Protected        from '../views/protected';

// layout
import MainLayout       from '../layout/MainLayout/';
import AdminLayout       from '../layout/AdminLayout/';
import BlankLayout       from '../layout/BlankLayout/';

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props => (
    <Layout>
      <Component {...props} />
    </Layout>
  )} />
);

const MainRoutes = () => {
  return (
    <Switch>
      <AppRoute exact path="/" layout={MainLayout} component={Home} />
      <AppRoute exact path="/about" layout={MainLayout} component={About} />
      <AppRoute exact path="/products" layout={MainLayout} component={Products} />
      <AppRoute exact path="/latest-news" layout={MainLayout} component={News} />
      <AppRoute exact path="/platform" layout={MainLayout} component={Platform} />
      <AppRoute exact path="/partnerships" layout={MainLayout} component={Partnerships} />
      <AppRoute exact path="/education" layout={MainLayout} component={Education} />
      <AppRoute exact path="/academy" layout={MainLayout} component={Academy} />

      <PrivateRoute path="/admin/academy/edit/:id" layout={AdminLayout}  component={AdminAcademyEdit} />
      <PrivateRoute path="/admin/academy" layout={AdminLayout}  component={AdminAcademy} />

      <PrivateRoute path="/admin/product/edit/:id" layout={AdminLayout}  component={AdminProductEdit} />
      <PrivateRoute path="/admin/product/new" layout={AdminLayout}  component={AdminProductEdit} />
      <PrivateRoute path="/admin/product" layout={AdminLayout}  component={AdminProduct} />

      <PrivateRoute path="/admin/certificate/edit/:id" layout={AdminLayout}  component={AdminCertificateEdit} />
      <PrivateRoute path="/admin/certificate/:id" layout={AdminLayout}  component={AdminCertificate} />

      <PrivateRoute path="/admin/gallery/edit/:id" layout={AdminLayout}  component={AdminGalleryEdit} />
      <PrivateRoute path="/admin/gallery/:id" layout={AdminLayout}  component={AdminGallery} />

      <PrivateRoute path="/admin/partnership/edit/:id" layout={AdminLayout}  component={AdminPartnershipEdit} />
      <PrivateRoute path="/admin/partnership/new" layout={AdminLayout}  component={AdminPartnershipEdit} />
      <PrivateRoute path="/admin/partnership" layout={AdminLayout}  component={AdminPartnership} />

      <PrivateRoute path="/admin/education/edit/:id" layout={AdminLayout}  component={AdminEducationEdit} />
      <PrivateRoute path="/admin/education/new" layout={AdminLayout}  component={AdminEducationEdit} />
      <PrivateRoute path="/admin/education" layout={AdminLayout}  component={AdminEducation} />

      <PrivateRoute path="/admin/banner/edit/:id" layout={AdminLayout}  component={AdminBannerEdit} />
      <PrivateRoute path="/admin/banner/new" layout={AdminLayout}  component={AdminBannerEdit} />
      <PrivateRoute path="/admin/banner" layout={AdminLayout}  component={AdminBanner} />

      <PrivateRoute path="/admin/news/edit/:id" layout={AdminLayout}  component={AdminNewsEdit} />
      <PrivateRoute path="/admin/news/new" layout={AdminLayout}  component={AdminNewsEdit} />
      <PrivateRoute path="/admin/news" layout={AdminLayout}  component={AdminNews} />

      <PrivateRoute path="/admin/product" layout={AdminLayout}  component={AdminProduct} />
      <PrivateRoute path="/admin/partnership" layout={AdminLayout}  component={AdminPartnership} />
      <PrivateRoute path="/admin/dashboard" layout={AdminLayout}  component={AdminDashboard} />
      <PrivateRoute path="/admin/systemSetting" layout={AdminLayout}  component={AdminSettingEdit} />

      <AppRoute path="/admin" layout={BlankLayout}  component={AdminLogin} />

      {/* private views: need user to be authenticated */}
      <PrivateRoute path="/protected" component={Protected} />
    </Switch>
  );
};

export default MainRoutes;
